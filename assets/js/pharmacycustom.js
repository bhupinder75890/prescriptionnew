jQuery(document).ready(function () {

var base_url = $('.base_url').val();

// alert(new Date().getFullYear() + 5);
   $( function() {
    $('.datepicker').datepicker({
       dateFormat: 'dd-mm-yy',
       minDate: 0,
       changeMonth: true,
       changeYear: true,
       // yearRange:new Date().getFullYear()+'":"'+new Date().getFullYear() + 5,
       // yearRange: "2020:2025",
        yearRange: new Date().getFullYear() + ':+5',
    });
   });

   $(".startdate").datepicker({
          dateFormat: "dd-mm-yy",
          onSelect: function (date) {
              var date2 = $('.startdate').datepicker('getDate');
              date2.setDate(date2.getDate() + 1);
              $('.enddate').datepicker('setDate', date2);
              $('.enddate').datepicker('option', 'minDate', date2);
          }
      });
      $('.enddate').datepicker({
          dateFormat: "dd-mm-yy",
          onClose: function () {
              var dt1 = $('.startdate').datepicker('getDate');

              var dt2 = $('.enddate').datepicker('getDate');
              if (dt2 <= dt1) {
                  var minDate = $('.endate').datepicker('option', 'minDate');
                  $('.enddate').datepicker('setDate', minDate);
              }
          }
      });

   //datepicker
   jQuery.validator.addMethod("regex", function(value, element, param){
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
         }, "Enter valid email address");

// Capital
 $('.capital').keyup(function(){
   $(this).val($(this).val().toUpperCase());
 });
 //capital

 // ckeditor
  jQuery.validator.addMethod("ckrequired", function (value, element) {
             var idname = $(element).attr('id');
             var editor = CKEDITOR.instances[idname];
             var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
             if (ckValue.length === 0) {
  //if empty or trimmed value then remove extra spacing to current control
                 $(element).val(ckValue);
             } else {
  //If not empty then leave the value as it is
                 $(element).val(editor.getData());
             }
             return $(element).val().length > 0;
         }, "This field is required");

  function GetTextFromHtml(html) {
             var dv = document.createElement("DIV");
             dv.innerHTML = html;
             return dv.textContent || dv.innerText || "";
         }
         // ckedior

    // login
    $("#loginform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
       regex: "",
     },
     password: {
       required: true
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
     password: {
       required: "Please enter password",

     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // login
//merchant
 $("#merchantUpdate").validate({
rules:
{
  merchant: {
    required: true,
  },
  secret: {
    required: true,
  },
},
messages:
{
  merchant: {
    required: "This is required",
  },
  secret: {
    required: "This is required",
  },
},
submitHandler: function (form)
{
  formSubmit(form);
}
});
//merchant

    // forgotpassword
    $("#forgotpasswordform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
        regex: "",
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // forgotpassword

// remove space
$('.drugStrength').keyup(function()
{
  $(this).val($(this).val().replace(/ +?/g, ''));
});
$('.drugDose').keyup(function()
{
  $(this).val($(this).val().replace(/ +?/g, ''));
});
// remove space


    // Newpassword
    $("#newpasswordform").validate({
   rules:
   {
     password: {
       required: true
     },
   },
   messages:
   {
     password: {
       required: "Please enter password",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // Newpassword
  // back
 $('.cancel').click(function(){
		parent.history.back();
		return false;
	});



  // back

  // add blog
  $("#profile").validate({
  rules:
  {
   name: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   address: {
     required: true
   },
  },
  messages:
  {
   name: {
     required: "Please enter name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   address: {
     required: "Please enter address",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);
  }
  });


  $("#addprescription").validate({
  rules:
  {
   patientName: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   // ic: {
   //   required: true
   // },
   pharmacyId: {
     required: true
   },
  },
  messages:
  {
   patientName: {
     required: "Please enter patient name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   // ic: {
   //   required: "Please enter ic",
   // },
   pharmacyId: {
     required: "Please select pharmacy",
   },
  },
  submitHandler: function (form)
  {
     // formSubmit(form);
     $('#confirmprescription').modal('show');
  }
  });

  $("#profile1").validate({
  rules:
  {
   name: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   license: {
     required: true
   },
   image: {
     required: true
   },
  },
  messages:
  {
   patientName: {
     required: "Please enter  name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   license: {
     required: "Please enter license number",
   },
   image: {
     required: "Profile image is required",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);
  }
  });

  // update profile

  // drug add
  // add blog
  $("#adddrug").validate({
  rules:
  {
   drugName: {
     required: true
   },
   // 'drugDose[]': {
   //   required: true
   // },
   drugType: {
     required: true,
   },
   drugSalePrice: {
     required: true
   },
   drugStrength: {
     required: true
   },
  },
  messages:
  {
   drugName: {
     required: "Please enter name ",
   },
   // 'drugDose[]': {
   //   required: "Please enter dose ",
   // },
   drugType: {
     required: "Please select type",
   },
   drugSalePrice: {
     required: "Please enter sale price",
   },
   drugStrength: {
     required: "Please enter strength ",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);
  }
  });


  // drug add

  // pagesAdd
$("#addpages").validate({
     ignore: [],
    rules:
    {

      pagesTitle:{
        required: true,
      },
      pagesDescription:{
        ckrequired: true,
      },
      pagesStatus:{
        required: true,
      },
    },
    messages:
    {

      pagesTitle: {
        required: "Please enter title",
      },
      pagesDescription: {
        ckrequired: "Please enter description",
      },
      pagesStatus: {
        required: "Please select status",
      },

    },
    submitHandler: function (form)
    {
       var a = CKEDITOR.instances.pagesDescription.getData();
     var input2 = $("<input>").attr("type", "hidden").attr("name", "pagesDescription").val(a);
       $(form).append(input2);
       formSubmit(form);

     }
  });

  // pages add

  // change pages status

 $('body').on('click','.pagesStatus',function(){

    var id =  $(this).attr('data-id');
    var status =  $(this).attr('data-status');

    if(status == 1)
    {
    var conf = confirm("Are you sure to change the status to Active? ");
   }
   else
   {
     var conf = confirm("Are you sure to change the status to Inactive?");
   }
 if(conf == true)
 {
   $.ajax({
     dataType:'json',
     url : base_url+'pharmacy1/pagesStatus',
     type : 'post',
     data : {
       pagesId:id,pagesStatus:status
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

       $.toast().reset('all');
       var delayTime = 3000;

       if (response.success)
       {
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });

       if(status == 1)
       {
         $(".pagesStatus"+id).removeClass("btn-primary");
         $(".pagesStatus"+id).addClass("btn-success");
         $(".pagesStatus"+id).attr("data-status",'0')
         $(".pagesStatus"+id).text('Active');
       }
       else
       {
         $(".pagesStatus"+id).removeClass("btn-success");
         $(".pagesStatus"+id).addClass("btn-primary");
         $(".pagesStatus"+id).attr('data-status','1')
         $(".pagesStatus"+id).text('Inactive');
        }
       }
     }
   });
 }
});

 // change pages status

 // pages delete

     $('body').on('click','.pagesdelete',function(){

       var id = $(this).attr('data-id');
       var conf = confirm("Are you sure to delete this record");
       if(conf == true)
       {

       $.ajax({
         dataType:'json',
         url :base_url + 'pharmacy1/pagesDelete',
         type :'post',
         data : {
           id:id
         },
         success: function(response){

           $.toast().reset('all');
         var delayTime = 3000;
         if(response.delayTime)
         delayTime = response.delayTime;
         if (response.success)
         {

           $.toast({
             heading             : 'Success',
             text                : response.success_message,
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'success',
             hideAfter           : delayTime,
             position            : 'top-right'
           });

             setTimeout(function() {  location.reload(true);},1000);
         }
         else
         {
           if( response.formErrors)
           {
             $.toast({
               heading             : 'Error',
               text                : response.errors,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
           else
           {
             jQuery('#InputEmail').val('');
             $.toast({
               heading             : 'Error',
               text                : response.error_message,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
         }

         }
       });
     }

     });

 // pages delete




$('body').on('click','.drugdelete',function(){

  var id = $(this).attr('data-id');
  var conf = confirm("Are you sure to delete this record");
  if(conf == true)
  {

  $.ajax({
    dataType:'json',
    url :base_url + 'pharmacy1/drugDelete',
    type :'post',
    data : {
      id:id
    },
    success: function(response){

      $.toast().reset('all');
    var delayTime = 3000;
    if(response.delayTime)
    delayTime = response.delayTime;
    if (response.success)
    {
      // $(".data"+id).hide();

      $.toast({
        heading             : 'Success',
        text                : response.success_message,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'success',
        hideAfter           : delayTime,
        position            : 'top-right'
      });

        setTimeout(function() {  location.reload(true);},1000);
    }
    else
    {
      if( response.formErrors)
      {
        $.toast({
          heading             : 'Error',
          text                : response.errors,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {
        jQuery('#InputEmail').val('');
        $.toast({
          heading             : 'Error',
          text                : response.error_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
    }

    }
  });
}

});





$("#passwordUpdate").validate({
rules:
{
 pass: {
   required: true,
   minlength: 6
 },
 cpass: {
   required: true,
   equalTo: "#pass",
  minlength: 6
 },
},
messages:
{

 pass: {
   required: "Please enter password",
   minlength: "Your password must be at least 6 characters long",

},

 cpass: {
   required: "Please enter confirm password",
   minlength: "Your password must be at least 6 characters long",
   equalTo:   "Confirm password is not matched."
},
},
submitHandler: function (form)
{
 formSubmit(form);
}
});

jQuery.validator.addMethod("ckrequired", function (value, element) {
           var idname = $(element).attr('id');
           var editor = CKEDITOR.instances[idname];
           var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
           if (ckValue.length === 0) {
//if empty or trimmed value then remove extra spacing to current control
               $(element).val(ckValue);
           } else {
//If not empty then leave the value as it is
               $(element).val(editor.getData());
           }
           return $(element).val().length > 0;
       }, "This field is required");



  ///change status country




  // prescription perpage
  $('body').on('change','.prescriptionperpage',function(){

        var perpage = $(this).val();
        var searchtext = $('.searchtext').val();

        $.ajax({
          dataType:'json',
          url :base_url + 'pharmacy1/getprescriptionperpage',
          type :'post',
          data : {
            perpage:perpage,searchtext:searchtext
          },
          beforeSend  : function () {
            $(".loader_panel").css('display','block');
          },
          complete: function () {
            $(".loader_panel").css('display','none');
          },
          success: function(response){

            prescriptionrowmanage(response.result);

          }
        });

      });
  // prescription perpage

  // prescription search
  $('body').on('keyup','.searchprescription',function(){

        var searchtext = $(this).val();

        $.ajax({
          dataType:'json',
          url :base_url + 'pharmacy1/getprescriptionperpage',
          type :'post',
          data : {
            searchtext:searchtext
          },
          beforeSend  : function () {
            $(".loader_panel").css('display','block');
          },
          complete: function () {
            $(".loader_panel").css('display','none');
          },
          success: function(response){

            prescriptionrowmanage(response.result);

          }
        });


      });

  // prescription search
  // prescription pagination
  $('body').on('click','.prescriptionpagination a',function(e){
     e.preventDefault();
     var pageno = $(this).attr('data-ci-pagination-page');
     var searchtext = $('.searchtext').val();
     var perpage = $('.prescriptionperpage').val();

     if(!pageno)
     {
       pageno = 1;
     }
     $.ajax({
     dataType:'json',
       url :base_url + 'pharmacy1/getprescriptionperpage/'+pageno,
       type :'post',
       data : {
         perpage:perpage,page:pageno,searchtext:searchtext
       },
       beforeSend  : function () {
         $(".loader_panel").css('display','block');
       },
       complete: function () {
         $(".loader_panel").css('display','none');
       },
       success: function(response){

       prescriptionrowmanage(response.result);

       }
     });
   });

  // perpage prescription

// search prescription
$('body').on('keyup','.searchprescription',function(){

      var searchtext = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'pharmacy1/getprescriptionperpage',
        type :'post',
        data : {
          searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          prescriptionrowmanage(response.result);

        }
      });


    });

// search prescription

// view prescription
$('body').on('click','.viewprescription',function(){

   var id = $(this).attr('data-id');
   $.ajax({
     dataType:'json',
     url :base_url + 'pharmacy1/prescriptionView',
     type :'post',
     data : {
       id:id
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){
     if (response.success)
     {
       var gg = response.result;
       var medicine = response.medicine;
       var rows = '';

       var dates = new Date(Date.parse(gg.date));
       var month = dates.getMonth();
       var day = dates.getDate();
       var year = dates.getFullYear();
        var date = ''+day+'-'+month+'-'+year;

     rows += '<p><b>Order NO</b> : '+gg.orderNo+' </p>';
     rows += '<p><b>Patient s Name</b> : '+gg.patientName+' </p>';
     rows += '<p><b>Email</b> : '+gg.email+' </p>';
     rows += '<p><b>Phone Number </b> : '+gg.phone+' </p>';
     rows += '<p><b>IC </b> : '+gg.ic+' </p>';
     rows += '<p><b>Doctor s Name </b> : '+gg.doctor+' </p>';
     rows += '<p><b>Date  </b> : '+date+' </p>';
     rows += '<p><b>Total Amount  </b> : '+gg.totalamount+' </p>';
     rows += '<p><b>Amount paid by insurance  </b> : '+gg.copay+' </p>';
     rows += '<p><b>Delivery Charges  </b> : '+gg.deliveryCharges+' </p>';
     rows += '<p><b>Amount paid by patient  </b> : '+gg.payableAmount+' </p>';
     var row ='';
      if(medicine)
       {
         $.each( medicine, function( key, value ) {
           var s = key + 1;
           row += '<tr> ';
           row += '<td> '+s+'</td>';
           row += '<td> '+value.drugName+'</td>';
           row += '<td> '+value.strength+'</td>';
           row += '<td> '+value.drugDose+'</td>';
           if(value.duration == 1)
           {
           row += '<td>Day</td>';
           }
           else if(value.duration == 2)
           {
           row += '<td>Week</td>';
           }
           else if(value.duration == 3)
           {
           row += '<td>Month</td>';
           }

           if(value.frequency == 1)
           {
           row += '<td>Daily</td>';
           }
           else if(value.frequency == 2)
           {
             row += '<td>Bd</td>';
           }
           else if(value.frequency == 3)
           {
             row += '<td>Tds</td>';
           }
           else if(value.frequency == 4)
           {
             row += '<td>Qds</td>';
           }
           else if(value.frequency == 5)
           {
             row += '<td>Om</td>';
           }
           else if(value.frequency == 6)
           {
             row += '<td>On</td>';
           }
           else if(value.frequency == 7)
           {
             row += '<td>5 day</td>';
           }
           else if(value.frequency == 8)
           {
             row += '<td>6 day</td>';
           }
           else if(value.frequency == 9)
           {
             row += '<td>Alternative</td>';
           }
           else if(value.frequency == 10)
           {
             row += '<td>Monthly</td>';
           }
           else if(value.frequency == 10)
           {
             row += '<td>Weekly</td>';
           }
           else if(value.frequency == 10)
           {
             row += '<td>5 day/week</td>';
           }

           row += '<td> '+value.no+'</td>';
           row += '<td> '+value.quantity+'</td>';
           row += '<td> '+value.price+'</td>';
           row += '<td> '+value.subtotal+'</td>';
           row += '<td> '+value.instruction+'</td>';
           row += '</tr>';

       });
       }

     $('.bodydata').html(rows);
     $('.tableresrow').html(row);
     $('#prescriptionmodal').modal('show');
     }

   }
 });
});

// view prescription

// add more
$('.addmore').click(function(){
  var rows = '';
var l =  $('.maindose').length;
l = l + 1;
rows += '<div class="row maindose maindose'+l+'">';
rows +='<div class="col-sm-6">';
rows +='<div class="form-group">';
rows +='<label>Dose<span class="error">*</span></label>';
rows +='<input type="text" placeholder="Please enter dose" class="form-control drugDose" name="drugDose[]"   id="drugDose">';
rows +='</div>';
rows +='</div>';
rows +='<div class="col-sm-2">';
rows +='<a data-id="'+l+'" class="removedose"><i class="fa fa-trash" aria-hidden="true"></i></a>';
rows +='</div>';
rows +='</div>';
$('.adddose').append(rows);
});
// add more

// remove drug
$('body').on('click','.removedose',function(){
 var    id = $(this).attr('data-id');
  $('.maindose'+id).remove();
});
// remove drug

// drug search
$('body').on('change','.drugperpage',function(){

      var perpage = $(this).val();
      var searchtext = $('.searchdrug').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'pharmacy1/getdrugperpage',
        type :'post',
        data : {
          perpage:perpage,searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          drugrowmanage(response.result);

        }
      });

    });
// drug perpage
// drug pagination
$('body').on('click','.drugpagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');
   var searchtext = $('.searchtext').val();
   var perpage = $('.drugperpage').val();

   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'pharmacy1/getdrugperpage/'+pageno,
     type :'post',
     data : {
       perpage:perpage,page:pageno,searchtext:searchtext
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     drugrowmanage(response.result);

     }
   });
 });

// perpage drug

// search drug
$('body').on('keyup','.searchdrug',function(){

    var searchtext = $(this).val();

    $.ajax({
      dataType:'json',
      url :base_url + 'pharmacy1/getdrugperpage',
      type :'post',
      data : {
        searchtext:searchtext
      },
      beforeSend  : function () {
        $(".loader_panel").css('display','block');
      },
      complete: function () {
        $(".loader_panel").css('display','none');
      },
      success: function(response){

        drugrowmanage(response.result);

      }
    });


  });

// drug search

// report search
$('body').on('change','.reportperpage',function(){

      var perpage = $(this).val();
      var searchtext = $('.reportstartdate').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'pharmacy1/getreportperpage',
        type :'post',
        data : {
          perpage:perpage,searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          reportrowmanage(response.result);

        }
      });

    });
// drug perpage
// drug pagination
$('body').on('click','.reportpagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');
   var startdate = $('.startdate').val();
   var enddate = $('.enddate').val();
   var perpage = $('.reportperpage').val();

   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'pharmacy1/getreportperpage/'+pageno,
     type :'post',
     data : {
       perpage:perpage,page:pageno,startdate:startdate,enddate:enddate
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     reportrowmanage(response.result);

     }
   });
 });

// perpage drug

// status change
$('body').on('change','.prescriptionStatus',function(){

     var id = $(this).find(':selected').data('id');
     var status = $(this).val();
      $('.confirmprescriptionId').val(id);
      $('.confirmprescriptionStatus').val(status);
       if(status != '')
       {
         if(status == 1)
         {
           $('.approved1').show();
           $('.approved2').hide();
         }
         else if(status == 2)
         {
           $('.approved2').show();
           $('.approved1').hide();
         }
       $('#confirmprescription').modal('show');
       }
    });

    $('body').on('click','#confirmed',function()
    {
      var id = $('.confirmprescriptionId').val();
      var status = $('.confirmprescriptionStatus').val();
      $.ajax({
      dataType:'json',
        url :base_url + 'pharmacy1/prescriptionStatus',
        type :'post',
        data : {
          prescriptionId:id,status:status
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
       success: function (response) {
         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
         $.toast().reset('all');
         var delayTime = 3000;
         if(response.delayTime)
         delayTime = response.delayTime;
         if (response.success)
         {
           $('#confirmprescription').modal('hide');
           
             setTimeout(function() {  location.reload(true);},1000);
              
           $.toast({
             heading             : 'Success',
             text                : response.success_message,
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'success',
             hideAfter           : delayTime,
             position            : 'top-right'
           });
         }
         else
         {

           $(".button-disabled").removeAttr("disabled");
           if( response.formErrors)
           {
             $.toast({
               heading             : 'Error',
               text                : response.errors,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
           else
           {
             $.toast({
               heading             : 'Error',
               text                : response.error_message,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
         }
         if(response.ajaxPageCallBack)
         {
           response.formid = form.id;
           ajaxPageCallBack(response);
         }
         if(response.resetform)
           {
           $('.reset')[0].reset();
            }
         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
       },
       error:function(response){
           $.toast({
             heading             : 'Error',
             text                : "Server Error",
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'error',
             hideAfter           : 4000,
             position            : 'top-right'
           });

       }
     });
    });
    // subscription

    // cost
    $('body').on('change','.deliveryCost',function()
    {
      var cost = $(this).val();
      var id =  $(this).find("option:selected").data('id');

      console.log(id);
      $.ajax({
      dataType:'json',
        url :base_url + 'pharmacy1/prescriptionCost/',
        type :'post',
        data : {
          prescriptionId:id,deliveryCharges:cost
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
       success: function (response) {
         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
         $.toast().reset('all');
         var delayTime = 3000;
         if(response.delayTime)
         delayTime = response.delayTime;
         if (response.success)
         {
           $('#confirmprescription').modal('hide');
           $(".deliveryCost"+id).attr('disabled','true');
             setTimeout(function() {  location.reload(true);},1000);

           $.toast({
             heading             : 'Success',
             text                : response.success_message,
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'success',
             hideAfter           : delayTime,
             position            : 'top-right'
           });
         }
         else
         {

           $(".button-disabled").removeAttr("disabled");
           if( response.formErrors)
           {
             $.toast({
               heading             : 'Error',
               text                : response.errors,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
           else
           {
             $.toast({
               heading             : 'Error',
               text                : response.error_message,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
         }
         if(response.ajaxPageCallBack)
         {
           response.formid = form.id;
           ajaxPageCallBack(response);
         }
         if(response.resetform)
           {
           $('.reset')[0].reset();
            }
         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
       },
       error:function(response){
           $.toast({
             heading             : 'Error',
             text                : "Server Error",
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'error',
             hideAfter           : 4000,
             position            : 'top-right'
           });

       }
     });
    });
    // cost

// status change


// search drug
$('body').on('click','.searchreport',function(){

    var startdate = $('.startdate').val();
    var enddate = $('.enddate').val();

    $.ajax({
      dataType:'json',
      url :base_url + 'pharmacy1/getreportperpage',
      type :'post',
      data : {
        startdate:startdate,enddate:enddate
      },
      beforeSend  : function () {
        $(".loader_panel").css('display','block');
      },
      complete: function () {
        $(".loader_panel").css('display','none');
      },
      success: function(response){

        reportrowmanage(response.result);

      }
    });


  });

// Report search


// pages pagination
$('body').on('click','.pagespagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');

   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'pharmacy1/getpagesperpage/'+pageno,
     type :'post',
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     pagesrowmanage(response.result);

     }
   });
 });

// pages pagination


// number only
//decimal
$(function () {
$(".numberdecimalonly").keydown(function (event) {


  if (event.shiftKey == true) {
    event.preventDefault();
  }

  if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

  } else {
    event.preventDefault();
  }

  if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
  event.preventDefault();

});
});
//decimal
// number only

$('body').on('change', '.image', function()
  {
     readURL(this);
  });

$('body').on('change', '.simage', function()
  {
     sreadURL(this);
  });

});

 // *************** image read*************
 function readURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.showimage').show();
            $('.showimage').attr('src', e.target.result);
            $('.showimage').attr('height','100');
            $('.showimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }

 function sreadURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.signatureimage').show();
            $('.signatureimage').attr('src', e.target.result);
            $('.signatureimage').attr('height','100');
            $('.signatureimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }
 // *************** image read*************
 // image read

 // prescription mamnage row
  function prescriptionrowmanage(data)
    {
      var base_url = $('.base_url').val();

     var rows = '';
     var a = data.start + 1;

     if(data.result.length != 0)
     {
    $.each( data.result, function( key, value ) {
      var dates = new Date(Date.parse(value.date));
      var month = dates.getMonth() + 1;
      var day = dates.getDate();
      var year = dates.getFullYear();
       var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
      rows += '<td>'+a+'</td>';
      rows += '<td>'+date+'</td>';
      rows += '<td>'+value.orderNo+'</td>';
      rows += '<td>'+value.name+'</td>';
      rows += '<td>'+value.patientName+'</td>';
      // rows += '<td>'+value.email+'</td>';
      rows += '<td>'+value.phone+'</td>';
      rows += '<td>'+value.payableAmount+'</td>';
      rows += '<td>'+value.copay+'</td>';
      rows += '<td>'+value.totalamount+'</td>';
      rows += '<td><a data-id="'+value.prescriptionId+'" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a></td>';

      if(value.status == 0)
      {
      rows += '<td><select  class="prescriptionStatus" name="status">';
      }
      else
      {
      rows += '<td><select disabled class="prescriptionStatus" name="status">';
      }
      rows +='<option  value="">Select Status</option>';
      if(value.status == 1 )
      {
      rows +='<option selected  data-id="'+value.prescriptionId+'" value="1">Accept Order</option>';
      }
      else
      {
        rows +='<option   data-id="'+value.prescriptionId+'" value="1">Accept Order</option>';
      }
      if(value.status == 2 )
      {
      rows +='<option selected data-id="'+value.prescriptionId+'" value="2">Cancel</option>';
      }
      else
      {
        rows +='<option data-id="'+value.prescriptionId+'" value="2">Cancel</option>';
      }
      rows +='</select></td>';
      rows +='<td>';
      if(value.delivery == 1)
      {
      rows +="Delivery";
      }
      else
      {
        rows +="Self Pick Up";
      }
      if(value.delivery == 1)
      {
        if(value.deliveryCharges && value.deliveryCharges != '')
        {
       rows +='<select disabled  class="deliveryCost deliveryCost'+value.prescriptionId+'"  >';
       }
       else
       {
      rows +='<select  class="deliveryCost"  >';
       }
      rows +='<option value="">Select Cost</option>';
      if(value.deliveryCharges == 10)
      {
      rows +='<option selected data-id="'+value.prescriptionId+'"  value="10">RM10</option>';
      }
      else
      {
        rows +='<option data-id="'+value.prescriptionId+'"  value="10">RM10</option>';
      }
      if(value.deliveryCharges == 20)
      {
      rows +='<option selected data-id="'+value.prescriptionId+'"  value="20">RM20</option>';
      }
      else
      {
      rows +='<option  data-id="'+value.prescriptionId+'"  value="20">RM20</option>';
      }
      if(value.deliveryCharges == 25)
      {
      rows +='<option selected data-id="'+value.prescriptionId+'"  value="25">RM25</option>';
      }
      else
      {
      rows +='<option  data-id="'+value.prescriptionId+'"  value="25">RM25</option>';
      }
      if(value.deliveryCharges == 30)
      {
      rows +='<option selected data-id="'+value.prescriptionId+'"  value="30">RM30</option>';
      }
      else
      {
        rows +='<option data-id="'+value.prescriptionId+'"  value="30">RM30</option>';
      }
      if(value.deliveryCharges == 40)
      {
      rows +='<option selected data-id="'+value.prescriptionId+'"  value="40">RM40</option>';
      }
      else
      {
      rows +='<option  data-id="'+value.prescriptionId+'"  value="40">RM40</option>';
      }
      rows +='</select>';
     }
      rows +='</td>';
      if(value.paymentStatus == 0)
      {
      rows +='<td>Payment Due</td>';
      }
      else if(value.paymentStatus == 1)
      {
      rows +='<td>Payment Paid</td>';
      }
      else if(value.paymentStatus == 2)
      {
       rows +='<td>Payment Failed</td>';
      }
      rows += '</tr>';
     a++;
  });
  }
  else
  {
     rows += '<tr>';
     rows += '<td colspan="9">No Record Found</td>';
     rows += '</tr>';
  }
  $(".prescriptiondata").html(rows);
  $(".prescriptionpagination").html(data.links);

 }
 // prescription mamnage row

 // drug manage rows
 function drugrowmanage(data)
   {
     var base_url = $('.base_url').val();

    var rows = '';
    var a = data.start + 1;

    if(data.result.length != 0)
    {
    $.each( data.result, function( key, value ) {
     var dates = new Date(Date.parse(value.drugExpiryDate));
     var month = dates.getMonth() + 1;
     var day = dates.getDate();
     var year = dates.getFullYear();
      var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
     rows += '<td>'+a+'</td>';
     rows += '<td>'+value.drugName+'</td>';
     var zz = [];
     $.each(value.dose, function(k,v)
     {
        zz.push(v.drugDose);
     });
     zz = zz.join(", ");
     if(value.drugType == 1)
     {
     rows += '<td>Unit</td>';
     }
     else if(value.drugType == 2)
     {
       rows += '<td>Tablet</td>';
     }
     else if(value.drugType == 3)
     {
       rows += '<td>Bottle</td>';
     }
     else if(value.drugType == 4)
     {
       rows += '<td>Injection</td>';
     }
     else if(value.drugType == 5)
     {
       rows += '<td>Syrup</td>';
     }
     rows += '<td>'+value.drugStrength+'</td>';

     rows += '<td>'+zz+'</td>';


     rows += '<td>'+value.drugSalePrice+'</td>';
     rows += '<td>'+value.drugBuyPrice+'</td>';
     rows += '<td>'+date+'</td>';
     rows += '<td><a href="'+base_url+'pharmacy1/drug/edit/'+value.drugId+'"><i class="fa fa-edit"></i></a><a class="drugdelete" data-id="'+value.drugId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
     rows += '</tr>';
    a++;
 });
 }
 else
 {
    rows += '<tr>';
    rows += '<td colspan="6">No Record Found</td>';
    rows += '</tr>';
 }
 $(".drugdata").html(rows);
 $(".drugpagination").html(data.links);

 }
 // drug manage rows


 // prescription mamnage row
  function reportrowmanage(data)
    {
      var base_url = $('.base_url').val();

     var rows = '';
     var a = data.start + 1;

     if(data.result.length != 0)
     {
    $.each( data.result, function( key, value ) {
      var dates = new Date(Date.parse(value.date));
      var month = dates.getMonth() + 1;
      var day = dates.getDate();
      var year = dates.getFullYear();
       var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
      rows += '<td>'+a+'</td>';
      rows += '<td>'+date+'</td>';
      rows += '<td>'+value.orderNo+'</td>';
      rows += '<td>'+value.name+'</td>';
      rows += '<td>'+value.patientName+'</td>';
      rows += '<td>'+value.email+'</td>';
      rows += '<td>'+value.phone+'</td>';
      rows += '<td>'+value.payableAmount+'</td>';
      rows += '<td>'+value.copay+'</td>';
      rows += '<td>'+value.totalamount+'</td>';
      if(value.delivery == 1)
      {
      rows +='<td>Delivery</td>';
      }
      else
      {
        rows +='<td>Self Pick Up</td>';
      }
      rows += '<td><a data-id="'+value.prescriptionId+'" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a></td>';
      rows += '</tr>';
     a++;
  });
  }
  else
  {
     rows += '<tr>';
     rows += '<td colspan="8">No Record Found</td>';
     rows += '</tr>';
  }
  $(".reportdata").html(rows);
  $(".reportpagination").html(data.links);

 }
 // Report mamnage row

 // pages manage row
 // pages manage row
 function pagesrowmanage(data)
   {
     var base_url = $('.base_url').val();

    var rows = '';
    var a = data.start + 1;

    if(data.result.length != 0)
    {
   $.each( data.result, function( key, value ) {

    rows += '<tr>';
     rows += '<td>'+a+'</td>';
     rows += '<td>'+value.pagesTitle+'</td>';
     if(value.pagesStatus == 1)
     {
     rows += '<td><a data-id="'+value.pagesId+'" data-status="0" class="btn btn-success pagesStatus pagesStatus'+value.pagesId+'">Active</a></td>';
     }
     else
     {
       rows += '<td><a data-id="'+value.pagesId+'" data-status="1" class="btn btn-primary pagesStatus pagesStatus'+value.pagesId+'">InActive</a></td>';
     }

     rows += '<td><a href="'+base_url+'pharmacy1/pages/edit/'+value.categoriesId+'"><i class="fa fa-edit"></i></a><a class="pagesdelete" data-id="'+value.pagesId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
     rows += '</tr>';
    a++;
 });
 }
 else
 {
    rows += '<tr>';
    rows += '<td colspan="5">No Record Found</td>';
    rows += '</tr>';
 }
 $(".pagesdata").html(rows);
 $(".pagespagination").html(data.links);

}
 // pages manage row

 // *************** submit function******************


 function formSubmit(form)
{
  $.ajax({
    url         : form.action,
    type        : form.method,
    data        : new FormData(form),
    enctype : 'multipart/form-data',
    contentType : false,
    cache       : false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    processData : false,
    dataType    : "json",
    beforeSend  : function () {
      $(".button-disabled").attr("disabled", "disabled");
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
        $(".button-disabled").attr("disabled",false);
    },
    success: function (response) {

      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      $.toast().reset('all');
      var delayTime = 3000;
      if(response.delayTime)
      delayTime = response.delayTime;
      if (response.success)
      {
        $.toast({
          heading             : 'Success',
          text                : response.success_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'success',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {

        $(".button-disabled").removeAttr("disabled");
        if( response.formErrors)
        {
          $.toast({
            heading             : 'Error',
            text                : response.errors,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
        else
        {
          $.toast({
            heading             : 'Error',
            text                : response.error_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
        {
        $('.reset')[0].reset();
         }
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
    },
    error:function(response){
        $.toast({
          heading             : 'Error',
          text                : "Server Error",
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : 4000,
          position            : 'top-right'
        });

    }
  });
}





function emailform(form)
{
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
       $('.reset')[0].reset();
       $("#employeeEmail").modal('hide');
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}
 // *************** submit function******************
