jQuery(document).ready(function () {

var base_url = $('.base_url').val();


   // datepicker
   // $( function() {
   //  $('.datepicker').datepicker({
   //     dateFormat: 'mm-dd-yy',
   //     changeMonth: true,
   //     changeYear: true,
   //     // yearRange: "1950:2020"
   //     yearRange: "1950:"+new Date().getFullYear(),
   //  });
   // });
   $(".drug").attr('autocomplete', 'off');

   //datepicker
   jQuery.validator.addMethod("regex", function(value, element, param){
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
         }, "Enter valid email address");

// Capital
 $('.capital').keyup(function(){
   $(this).val($(this).val().toUpperCase());
 });
 //capital

    // login
    $("#loginform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
       regex: "",
     },
     password: {
       required: true
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
     password: {
       required: "Please enter password",

     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // login

    // forgotpassword
    $("#forgotpasswordform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
        regex: "",
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // forgotpassword



    // Newpassword
    $("#newpasswordform").validate({
   rules:
   {
     password: {
       required: true
     },
   },
   messages:
   {
     password: {
       required: "Please enter password",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // Newpassword
  // back
 $('.cancel').click(function(){
		parent.history.back();
		return false;
	});



  // back

  // add blog
  $("#profile").validate({
  rules:
  {
   name: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   license: {
     required: true
   },
  },
  messages:
  {
   name: {
     required: "Please enter name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   license: {
     required: "Please enter license number",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);
  }
  });


  $("#addprescription").validate({
  rules:
  {
   patientName: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   // ic: {
   //   required: true
   // },
   pharmacyId: {
     required: true
   },
   delivery: {
     required: true
   },
  },
  messages:
  {
   patientName: {
     required: "Please enter patient name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   // ic: {
   //   required: "Please enter ic",
   // },
   pharmacyId: {
     required: "Please select pharmacy",
   },
   delivery: {
     required: "Please select delivery",
   },
  },
  submitHandler: function (form)
  {
     // formSubmit(form);
     $('#confirmprescription').modal('show');
  }
  });

  $("#profile1").validate({
  rules:
  {
   name: {
     required: true
   },
   email: {
     required: true,
     email:true,
      regex: "",
   },
   phone: {
     required: true
   },
   license: {
     required: true
   },
   image: {
     required: true
   },
  },
  messages:
  {
   patientName: {
     required: "Please enter  name ",
   },
   email: {
     required: "email is required",
   },
   phone: {
     required: "Please enter phone number",
   },
   license: {
     required: "Please enter license number",
   },
   image: {
     required: "Profile image is required",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);
  }
  });

  // update profile


  // delete blog
$('body').on('click','.prescriptiondelete',function(){

  var id = $(this).attr('data-id');
  var conf = confirm("Are you sure to delete this record");
  if(conf == true)
  {

  $.ajax({
    dataType:'json',
    url :base_url + 'doctor/prescriptionDelete',
    type :'post',
    data : {
      id:id
    },
    success: function(response){

      $.toast().reset('all');
    var delayTime = 3000;
    if(response.delayTime)
    delayTime = response.delayTime;
    if (response.success)
    {
      // $(".data"+id).hide();

      $.toast({
        heading             : 'Success',
        text                : response.success_message,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'success',
        hideAfter           : delayTime,
        position            : 'top-right'
      });

        setTimeout(function() {  location.reload(true);},1000);
    }
    else
    {
      if( response.formErrors)
      {
        $.toast({
          heading             : 'Error',
          text                : response.errors,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {
        jQuery('#InputEmail').val('');
        $.toast({
          heading             : 'Error',
          text                : response.error_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
    }

    }
  });
}

});
// delete blog




$("#passwordUpdate").validate({
rules:
{
 pass: {
   required: true,
   minlength: 6
 },
 cpass: {
   required: true,
   equalTo: "#pass",
  minlength: 6
 },
},
messages:
{

 pass: {
   required: "Please enter password",
   minlength: "Your password must be at least 6 characters long",

},

 cpass: {
   required: "Please enter confirm password",
   minlength: "Your password must be at least 6 characters long",
   equalTo:   "Confirm password is not matched."
},
},
submitHandler: function (form)
{
 formSubmit(form);
}
});

jQuery.validator.addMethod("ckrequired", function (value, element) {
           var idname = $(element).attr('id');
           var editor = CKEDITOR.instances[idname];
           var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
           if (ckValue.length === 0) {
//if empty or trimmed value then remove extra spacing to current control
               $(element).val(ckValue);
           } else {
//If not empty then leave the value as it is
               $(element).val(editor.getData());
           }
           return $(element).val().length > 0;
       }, "This field is required");



  ///change status country

  // perpage prescription

  // prescription perpage
  $('body').on('change','.prescriptionperpage',function(){

        var perpage = $(this).val();
        var searchtext = $('.searchtext').val();

        $.ajax({
          dataType:'json',
          url :base_url + 'doctor/getprescriptionperpage',
          type :'post',
          data : {
            perpage:perpage,searchtext:searchtext
          },
          beforeSend  : function () {
            $(".loader_panel").css('display','block');
          },
          complete: function () {
            $(".loader_panel").css('display','none');
          },
          success: function(response){

            prescriptionrowmanage(response.result);

          }
        });

      });
  // prescription perpage
  // prescription pagination
  $('body').on('click','.prescriptionpagination a',function(e){
     e.preventDefault();
     var pageno = $(this).attr('data-ci-pagination-page');
     var searchtext = $('.searchtext').val();
     var perpage = $('.prescriptionperpage').val();

     if(!pageno)
     {
       pageno = 1;
     }
     $.ajax({
     dataType:'json',
       url :base_url + 'doctor/getprescriptionperpage/'+pageno,
       type :'post',
       data : {
         perpage:perpage,page:pageno,searchtext:searchtext
       },
       beforeSend  : function () {
         $(".loader_panel").css('display','block');
       },
       complete: function () {
         $(".loader_panel").css('display','none');
       },
       success: function(response){

       prescriptionrowmanage(response.result);

       }
     });
   });

  // perpage prescription

// search prescription
$('body').on('keyup','.searchprescription',function(){

      var searchtext = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'doctor/getprescriptionperpage',
        type :'post',
        data : {
          searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          prescriptionrowmanage(response.result);

        }
      });


    });

// search prescription

// view prescription
$('body').on('click','.viewprescription',function(){

   var id = $(this).attr('data-id');
   $.ajax({
     dataType:'json',
     url :base_url + 'doctor/prescriptionView',
     type :'post',
     data : {
       id:id
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){
     if (response.success)
     {
       var gg = response.result;
       var medicine = response.medicine;
       var rows = '';

       var dates = new Date(Date.parse(gg.date));
       var month = dates.getMonth();
       var day = dates.getDate();
       var year = dates.getFullYear();
        var date = ''+day+'-'+month+'-'+year;

     rows += '<p><b>Order No</b> : '+gg.orderNo+' </p>';
     rows += '<p><b>Patient s Name</b> : '+gg.patientName+' </p>';
     rows += '<p><b>Email</b> : '+gg.email+' </p>';
     rows += '<p><b>Phone Number </b> : '+gg.phone+' </p>';
     rows += '<p><b>IC </b> : '+gg.ic+' </p>';
     rows += '<p><b>Pharmacy  </b> : '+gg.pharmacy+' </p>';
     rows += '<p><b>Date  </b> : '+date+' </p>';
     rows += '<p><b>Total order  </b> : '+gg.totalamount+' </p>';
     rows += '<p><b>Amount paid by insurance  </b> : '+gg.copay+' </p>';
     rows += '<p><b>Delivery Charges  </b> : '+gg.deliveryCharges+' </p>';
     rows += '<p><b>Amount paid by patient  </b> : '+gg.payableAmount+' </p>';
     var row ='';
      if(medicine)
       {
         $.each( medicine, function( key, value ) {
           var s = key + 1;
        row += '<tr> ';
        row += '<td> '+s+'</td>';
        row += '<td> '+value.drugName+'</td>';
        row += '<td> '+value.strength+'</td>';
        row += '<td> '+value.drugDose+'</td>';
        if(value.duration == 1)
        {
        row += '<td>Day</td>';
        }
        else if(value.duration == 2)
        {
        row += '<td>Week</td>';
        }
        else if(value.duration == 3)
        {
        row += '<td>Month</td>';
        }

        if(value.frequency == 1)
        {
        row += '<td>Daily</td>';
        }
        else if(value.frequency == 2)
        {
          row += '<td>Bd</td>';
        }
        else if(value.frequency == 3)
        {
          row += '<td>Tds</td>';
        }
        else if(value.frequency == 4)
        {
          row += '<td>Qds</td>';
        }
        else if(value.frequency == 5)
        {
          row += '<td>Om</td>';
        }
        else if(value.frequency == 6)
        {
          row += '<td>On</td>';
        }
        else if(value.frequency == 7)
        {
          row += '<td>5 day</td>';
        }
        else if(value.frequency == 8)
        {
          row += '<td>6 day</td>';
        }
        else if(value.frequency == 9)
        {
          row += '<td>Alternative</td>';
        }
        else if(value.frequency == 10)
        {
          row += '<td>Monthly</td>';
        }
        else if(value.frequency == 10)
        {
          row += '<td>Weekly</td>';
        }
        else if(value.frequency == 10)
        {
          row += '<td>5 day/week</td>';
        }

        row += '<td> '+value.no+'</td>';
        row += '<td> '+value.quantity+'</td>';
        row += '<td> '+value.price+'</td>';
        row += '<td> '+value.subtotal+'</td>';
        row += '<td> '+value.instruction+'</td>';
        row += '</tr>';

       });
       }

     $('.bodydata').html(rows);
     $('.tableresrow').html(row);
     $('#prescriptionmodal').modal('show');
     }

   }
 });
});

// view prescription

// add more
$('.addmore').click(function(){
  var rows = '';
var l =  $('.main').length;
l = l + 1;
rows +='<tr class="main main'+l+'">';
rows += '<td>'+l+'</td>';
rows += '<td>';
rows += '<input type="text" placeholder="search drug name" data-id="'+l+'" required  class="form-control medicines drug drug'+l+'" name="drug[]"  >';
rows += '<input type="hidden"  class="drugId'+l+'" name="drugId[]" value="">';
rows += '<ul class="resultshow'+l+'" style="display:none;"></ul></td>';
rows += '<td>'
rows +='<input readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength'+l+'">';
rows += '</td>';
rows += '<td>';
rows += '<select required type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose'+l+'">';
rows += '</select><input type="hidden" value="" class="doses'+l+'">';
rows += '</td>';
rows += '<td>';
rows += '<select data-id="'+l+'" required type="text" name="frequency[]"  class="form-control medicines frequency frequency'+l+'">';
rows +=  '     <option  value="">Select Frequency</option>';
rows +=   '    <option data-id="'+l+'" value="1">Daily</option>';
rows +=   '    <option data-id="'+l+'" value="2">Bd</option>';
rows +=    '   <option data-id="'+l+'" value="3">Tds</option>';
rows +=     '  <option data-id="'+l+'" value="4">qds</option>';
rows +=      ' <option data-id="'+l+'" value="5">om</option>';
rows +=       '<option data-id="'+l+'" value="6">on</option>';
rows +=       '<option data-id="'+l+'" value="7">5*/day</option>';
rows +=       '<option data-id="'+l+'" value="8">6*/day</option>';
rows +=       '<option data-id="'+l+'" value="9">Alternative day</option>';
rows +=       '<option data-id="'+l+'" value="10">Monthly </option>';
rows +=       '<option data-id="'+l+'" value="11">Weekly </option>';
rows +=       '<option data-id="'+l+'" value="12">5 day/week</option>';
rows +=     '</select>';
rows += '</td>';
rows += '<td>'
rows += '    <select  required type="text" name="duration[]"  class="form-control medicines duration duration'+l+'">';
rows += '      <option  value="">Select Duration</option>';
rows += '      <option data-id="'+l+'" value="1">Day</option>';
rows += '      <option data-id="'+l+'" value="2">Week</option>';
rows += '      <option data-id="'+l+'" value="3">Month</option>';
rows += '</select>';
rows += '  </td>';
rows += '  <td><input  required data-id="'+l+'" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no'+l+'"></td>';
rows += '  <td><input  required data-id="'+l+'" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty'+l+'"></td>';
rows += '  <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price'+l+'"></td>';
rows += '  <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal'+l+'"></td>';
rows += '  <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>';
rows += '<td><a class="prescriptionremove" data-id="'+l+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
rows += '</tr>';
$('.maint').append(rows);
});
// add more

// change pharmacy

// change pharmacy
$('body').on('keyup','.drug',function(){
      var s = this;
      var ph = $(this).val();
      var id = $('#pharmacyId').val();
      var no = $(this).attr('data-id');
     if(ph)
     {
      $.ajax({
        dataType:'json',
        url :base_url + 'doctor/getmedicine',
        type :'post',
        data : {
          userId:id,text:ph
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){
          if(response.success == "true")
          {
            var  list ='';
           $.each(response.result,function(key, value){
             var id = response.result[key].drugId;
             var name = response.result[key].drugName;
             var sale = response.result[key].drugSalePrice;
             var dose = response.result[key].drugDose;
             var strength = response.result[key].drugStrength;

             list +='<li class="medicineresult" data-st="'+strength+'"  data-no="'+no+'" data-dose="'+dose+'" data-text="'+name+'" data-sale="'+sale+'" data-id="'+id+'">'+name+'</li>';
           });
           $(s).parent().find("ul").show();
           $(s).parent().find('ul').html(list);
          }
        }
      });
    }

    });


    $('body').on('click','.medicineresult',function()
    {
      var id = $(this).attr('data-id');
      var text = $(this).attr('data-text');
      var sale = $(this).attr('data-sale');
      var dose = $(this).attr('data-dose');
      var no = $(this).attr('data-no');
      var sale = $(this).attr('data-sale');
      var st = $(this).attr('data-st');

      $.ajax({
        dataType:'json',
        url :base_url + 'doctor/getdose',
        type :'post',
        data : {
          id:id
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){
          if(response.success == "true")
          {
            var  list ='';
           $.each(response.result,function(key, value){
             var doseid = response.result[key].drugDoseId;
             var name = response.result[key].drugDose;

             list +='<option data-no="'+no+'" data-text="'+name+'" value="'+doseid+'">'+name+'</option>';
           });

           $('.drug'+no).val(text);
           $('.drugId'+no).val(id);
           $('.dose'+no).html(list);
           $('.doses'+no).val(response.result[0].drugDose);
           $('.price'+no).val(sale);
           $('.strength'+no).val(st);
           $('.resultshow'+no).hide();
          }
          else
          {

            $('.drug'+no).val(text);
            $('.drugId'+no).val(id);
            $('.dose'+no).html('');
            $('.doses'+no).val('');
            $('.price'+no).val(sale);
            $('.strength'+no).val(st);
            // $('.frequency'+no).attr("disabled", true);
            // $('.duration'+no).attr("disabled", true);
            $('.qty'+no).val('1');
            var tttotal = 1 * parseFloat(sale);
            $('.subtotal'+no).val(tttotal);

            $('.paybypatient').html(ttotal);
            $('.resultshow'+no).hide();
            var ttotal = 0;
            var t = 0;
            $( ".subtotal" ).each(function( index ) {
              var t = $(this).val();
               if(t)
               {
                ttotal = parseFloat(t) + parseFloat(ttotal);
              }
            });
              $('.totalamount').val(ttotal);

            // var ttotal = 1 * parseInt(sale);
          }
        }
      });


    });

    $('body').on('change','.dose',function()
    {

      var  dose = this.options[this.selectedIndex].innerHTML;
      var  n = $(this).find("option:selected").data('no');
      // console.log(n);
      $('.doses'+n).val(dose);
      var frequency = $('.frequency'+n).val();
      // var n = $(this).find("option:selected").data('id');
      var strength = $('.strength'+n).val();
        var dose = dose.split("mg");
        var strength = strength.split("mg");
        var no = $('.no'+n).val();
        var ddd = (parseInt(dose[0]) / parseInt(strength[0]));
        if(!ddd)
        {
          ddd = 1;
        }

      var price = $('.price'+n).val();
      var duration = $('.duration'+n).val();

      if(frequency == 1)
      {
        var frequency1 = 1;
      }
      else if(frequency == 2)
      {
        var frequency1 = 2;
      }
      else if(frequency == 3)
      {
        var frequency1 = 3;

      }
      else if(frequency == 4)
      {
        var frequency1 = 4;
      }
      else if(frequency == 5)
      {
        var frequency1 = 1;

      }
      else if(frequency == 6)
      {
        var  frequency1 = 1;

      }
      else if(frequency == 7)
      {

        var frequency1 = 5;

      }
      else if(frequency == 8)
      {
        var frequency1 = 6;

      }
      else if(frequency == 9)
      {
        var frequency1 = 15;

      }
      else if(frequency == 10)
      {
        var frequency1 = 30
      }
      else if(frequency == 11)
      {
        var frequency1 = 7
      }
      else if(frequency == 12)
      {
        var  frequency1 = 5
      }
      if(duration == 1)
      {
        var duration1 = 1;
      }
      else if(duration  == 2)
      {
        var duration1 = 7;
      }
      else if(duration  == 3)
      {
        var duration1 = 30;
      }
      // if(size == 1)
      // {
      //   size1 = 1;
      // }
      // else if(size == 2)
      // {
      //   size1 = 0.50;
      // }
      // else if(size == 3)
      // {
      //   size1  = 0.25;
      // }

      var subtotal = parseInt(duration1) * parseInt(frequency1) * parseFloat(ddd) * parseInt(no);
      $('.qty'+n).val(subtotal);
      var subtotal1 = parseFloat(subtotal) * parseFloat(price);
      subtotal1 = subtotal1.toFixed(2);

     $('.subtotal'+n).val(subtotal1);
      var multiz = 0;
     $('.subtotal').each(function(index) {

       var $this = $(this);

       var valu = $this.val();
       if(valu)
       {
       multiz = parseFloat(multiz) + parseFloat(valu);
       }
   });
   multiz = multiz.toFixed(2);

      $('.totalamount').val(multiz);
      $('.paybypatient').html(multiz);
    });

// duration
    $('body').on('change','.duration',function()
    {

      var duration = $(this).val();
      var  n = $(this).find("option:selected").data('id');
      var price = $('.price'+n).val();
      var frequency = $('.frequency'+n).val();
      var strength = $('.strength'+n).val();
      var dose = $('.doses'+n ).val();
      var dose = dose.split("mg");
      var strength = strength.split("mg");
      var no = $('.no'+n).val();

      var ddd = (parseInt(dose[0]) / parseInt(strength[0]));
      if(!ddd)
      {
        ddd = 1;
      }

      // var size = $('.size'+n).val();
      // console.log(duration);
      // console.log(price);
      // console.log(frequency);
      // console.log(size);

      if(frequency == 1)
      {
        var frequency1 = 1;
      }
      else if(frequency == 2)
      {
        var frequency1 = 2;
      }
      else if(frequency == 3)
      {
        var frequency1 = 3;

      }
      else if(frequency == 4)
      {
        var frequency1 = 4;
      }
      else if(frequency == 5)
      {
        var frequency1 = 1;

      }
      else if(frequency == 6)
      {
        var  frequency1 = 1;

      }
      else if(frequency == 7)
      {

        var frequency1 = 5;

      }
      else if(frequency == 8)
      {
        var frequency1 = 6;

      }
      else if(frequency == 9)
      {
        var frequency1 = 15;

      }
      else if(frequency == 10)
      {
        var frequency1 = 30
      }
      else if(frequency == 11)
      {
        var frequency1 = 7
      }
      else if(frequency == 12)
      {
        var  frequency1 = 5
      }
      if(duration == 1)
      {
        var duration1 = 1;
      }
      else if(duration  == 2)
      {
        var duration1 = 7;
      }
      else if(duration  == 3)
      {
        var duration1 = 30;
      }
      // if(size == 1)
      // {
      //   size1 = 1;
      // }
      // else if(size == 2)
      // {
      //   size1 = 0.50;
      // }
      // else if(size == 3)
      // {
      //   size1  = 0.25;
      // }
      //  console.log(frequency1);
      //  console.log(size1);
      //  console.log(duration1);
      // console.log(price);
      var dosecal = (parseInt(dose[0]) / parseInt(strength[0]));
      if(!dosecal)
      {
        var subtotal = 1;
      }
      else
      {
        var subtotal = parseInt(duration1) * parseInt(frequency1) * parseFloat(ddd) * parseInt(no);
      }
      $('.qty'+n).val(subtotal);

      var subtotal1 = parseFloat(subtotal) * parseFloat(price);
      subtotal1 = subtotal1.toFixed(2);

     $('.subtotal'+n).val(subtotal1);
      var multiz = 0;
     $('.subtotal').each(function(index) {

       var $this = $(this);

       var valu = $this.val();
       if(valu)
       {
       multiz = parseFloat(multiz) + parseFloat(valu);
       }
   });
   multiz = multiz.toFixed(2);

      $('.totalamount').val(multiz);
      $('.paybypatient').html(multiz);
    });

    // duration



    // frequency
    $('body').on('change','.frequency',function()
    {
      var frequency = $(this).val();
      // var n = $(this).attr('data-id');
      var n = $(this).find("option:selected").attr('data-id');
      var no = $('.no'+n).val();

      // var size = $('.size'+n).val();
      var strength = $('.strength'+n).val();
        var dose = $('.doses'+n ).val();
        var dose = dose.split("mg");
        var strength = strength.split("mg");
        var ddd = (parseInt(dose[0]) / parseInt(strength[0]));

         if(!ddd)
         {
          var ddd = 1;
         }

      var price = $('.price'+n).val();
      var duration = $('.duration'+n).val();

      if(frequency == 1)
      {
        var frequency1 = 1;
      }
      else if(frequency == 2)
      {
        var frequency1 = 2;
      }
      else if(frequency == 3)
      {
        var frequency1 = 3;

      }
      else if(frequency == 4)
      {
        var frequency1 = 4;
      }
      else if(frequency == 5)
      {
        var frequency1 = 1;

      }
      else if(frequency == 6)
      {
        var  frequency1 = 1;

      }
      else if(frequency == 7)
      {

        var frequency1 = 5;

      }
      else if(frequency == 8)
      {
        var frequency1 = 6;

      }
      else if(frequency == 9)
      {
        var frequency1 = 15;

      }
      else if(frequency == 10)
      {
        var frequency1 = 30
      }
      else if(frequency == 11)
      {
        var frequency1 = 7
      }
      else if(frequency == 12)
      {
        var  frequency1 = 5
      }
      if(duration == 1)
      {
        var duration1 = 1;
      }
      else if(duration  == 2)
      {
        var duration1 = 7;
      }
      else if(duration  == 3)
      {
        var duration1 = 30;
      }
      // if(size == 1)
      // {
      //   size1 = 1;
      // }
      // else if(size == 2)
      // {
      //   size1 = 0.50;
      // }
      // else if(size == 3)
      // {
      //   size1  = 0.25;
      // }
      var dosecal = (parseInt(dose[0]) / parseInt(strength[0]));
      if(!dosecal)
      {
        var subtotal = 1;
      }
      else
      {
        var subtotal = parseInt(duration1) * parseInt(frequency1) * parseFloat(ddd) * parseInt(no);
      }
      $('.qty'+n).val(subtotal);

      var subtotal1 = parseFloat(subtotal) * parseFloat(price);
      subtotal1 = subtotal1.toFixed(2);
     $('.subtotal'+n).val(subtotal1);
      var multiz = 0;
     $('.subtotal').each(function(index) {

       var $this = $(this);

       var valu = $this.val();
       if(valu)
       {
       multiz = parseFloat(multiz) + parseFloat(valu);
       }
   });
   multiz = multiz.toFixed(2);

      $('.totalamount').val(multiz);
      $('.paybypatient').html(multiz);
    });
    // frequency

    // no
    $('body').on('keyup','.no',function()
    {
      var no = $(this).val();
      var n = $(this).data('id');
      // var size = $('.size'+n).val();
      var price = $('.price'+n).val();
      var duration = $('.duration'+n).val();
      var frequency = $('.frequency'+n).val();
      var strength = $('.strength'+n).val();
      var dose = $('.doses'+n ).val();

      var dose = dose.split("mg");
      var strength = strength.split("mg");

     var ddd = (parseInt(dose[0]) / parseInt(strength[0]));
     if(!ddd)
     {
      var ddd = 1;
     }


      if(frequency == 1)
      {
        var frequency1 = 1;
      }
      else if(frequency == 2)
      {
        var frequency1 = 2;
      }
      else if(frequency == 3)
      {
        var frequency1 = 3;

      }
      else if(frequency == 4)
      {
        var frequency1 = 4;
      }
      else if(frequency == 5)
      {
        var frequency1 = 1;

      }
      else if(frequency == 6)
      {
        var  frequency1 = 1;

      }
      else if(frequency == 7)
      {

        var frequency1 = 5;

      }
      else if(frequency == 8)
      {
        var frequency1 = 6;

      }
      else if(frequency == 9)
      {
        var frequency1 = 15;

      }
      else if(frequency == 10)
      {
        var frequency1 = 30
      }
      else if(frequency == 11)
      {
        var frequency1 = 7
      }
      else if(frequency == 12)
      {
        var  frequency1 = 5
      }
      if(duration == 1)
      {
        var duration1 = 1;
      }
      else if(duration  == 2)
      {
        var duration1 = 7;
      }
      else if(duration  == 3)
      {
        var duration1 = 30;
      }


      var dosecal = (parseInt(dose[0]) / parseInt(strength[0]));
      if(!dosecal)
      {
        var subtotal = 1;
      }
      else
      {
        var subtotal = parseInt(duration1) * parseInt(frequency1) * parseFloat(ddd) * parseInt(no);
      }

        $('.qty'+n).val(subtotal);
      var subtotal1 = parseFloat(subtotal) * parseFloat(price);
      subtotal1 = subtotal1.toFixed(2);
     $('.subtotal'+n).val(subtotal1);
      var multiz = 0;
     $('.subtotal').each(function(index) {

       var $this = $(this);

       var valu = $this.val();
       if(valu)
       {
       multiz = parseFloat(multiz) + parseFloat(valu);
       }
   });
   multiz = multiz.toFixed(2);

      $('.totalamount').val(multiz);
      $('.paybypatient').html(multiz);
    });
    // no

    // qty keyup
    $('body').on('keyup','.qty',function()
    {
      var qty = $(this).val();
      var n = $(this).data('id');
      var price = $('.price'+n).val();
      var subtotal1 = parseFloat(qty) * parseFloat(price);
      subtotal1 = subtotal1.toFixed(2);
      $('.subtotal'+n).val(subtotal1);
      var multiz = 0;
     $('.subtotal').each(function(index) {
       var $this = $(this);
       var valu = $this.val();
       if(valu)
       {
       multiz = parseFloat(multiz) + parseFloat(valu);
       }
   });
   multiz = multiz.toFixed(2);
      $('.totalamount').val(multiz);
      $('.paybypatient').html(multiz);
   });
    // qty keyup


    $('body').on('keyup','.copay',function()
    {
      var cop = $(this).val();
      var am = $('.totalamount').val();
     var pay = parseInt(am) - parseInt(cop);
     pay = pay.toFixed(2);

     $('.paybypatient').html(pay);

   });
// change pharmacy

// number only
$(".numberonly").keydown(function (e) {
// Allow: backspace, delete, tab, escape, enter and .
if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
// Allow: Ctrl+A, Command+A
(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
// Allow: home, end, left, right, down, up
(e.keyCode >= 35 && e.keyCode <= 40)) {
  // let it happen, don't do anything
  return;
}
// Ensure that it is a number and stop the keypress
if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
  e.preventDefault();
}
});
// number only

$('body').on('change', '.image', function()
  {
     readURL(this);
  });

$('body').on('change', '.simage', function()
  {
     sreadURL(this);
  });

  ////delete prescription
   $('body').on('click','.prescriptionremove',function()
   {
     var id = $(this).attr('data-id');
     $('.main'+id).remove();
   });
  ////delete prescription

});

 // *************** image read*************
 function readURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.showimage').show();
            $('.showimage').attr('src', e.target.result);
            $('.showimage').attr('height','100');
            $('.showimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }

 function sreadURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.signatureimage').show();
            $('.signatureimage').attr('src', e.target.result);
            $('.signatureimage').attr('height','100');
            $('.signatureimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }
 // *************** image read*************
 // image read

 // prescription mamnage row

  // doctormanage row
  function prescriptionrowmanage(data)
    {
      var base_url = $('.base_url').val();

     var rows = '';
     var a = data.start + 1;

     if(data.result.length != 0)
     {
    $.each( data.result, function( key, value ) {
      var dates = new Date(Date.parse(value.date));
      var month = dates.getMonth() + 1;
      var day = dates.getDate();
      var year = dates.getFullYear();
       var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
      rows += '<td>'+a+'</td>';
      rows += '<td>'+date+'</td>';
      rows += '<td>'+value.orderNo+'</td>';
      rows += '<td>'+value.patientName+'</td>';
      rows += '<td>'+value.email+'</td>';
      rows += '<td>'+value.phone+'</td>';
      rows += '<td>'+value.ic+'</td>';
      rows += '<td>'+value.pharmacy+'</td>';
      rows += '<td>'+value.totalamount+'</td>';
      rows += '<td><a href="'+base_url+'doctor/prescription/clone/'+value.prescriptionId+'"><i class="fa fa-clone"></i></a><a href="'+base_url+'doctor/downloadimage/'+value.prescriptionId+'" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><a data-id="'+value.prescriptionId+'" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a><a href="'+base_url+'doctor/prescription/edit/'+value.prescriptionId+'"><i class="fa fa-edit"></i></a><a class="prescriptiondelete" data-id="'+value.prescriptionId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
      rows += '</tr>';
     a++;
  });
  }
  else
  {
     rows += '<tr>';
     rows += '<td colspan="7">No Record Found</td>';
     rows += '</tr>';
  }
  $(".prescriptiondata").html(rows);
  $(".prescriptionpagination").html(data.links);

 }
  // doctormanage row
 // prescription mamnage row

 // *************** submit function******************
// subscription
$('body').on('click','#confirmed',function()
{
  var form = $('#addprescription')[0];
  var formData = new FormData(form);
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : formData,
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.resetform)
       {
       $('.reset')[0].reset();
        }
     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
});
// subscription


 function formSubmit(form)
{
  $.ajax({
    url         : form.action,
    type        : form.method,
    data        : new FormData(form),
    enctype : 'multipart/form-data',
    contentType : false,
    cache       : false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    processData : false,
    dataType    : "json",
    beforeSend  : function () {
      $(".button-disabled").attr("disabled", "disabled");
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
        $(".button-disabled").attr("disabled",false);
    },
    success: function (response) {

      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      $.toast().reset('all');
      var delayTime = 3000;
      if(response.delayTime)
      delayTime = response.delayTime;
      if (response.success)
      {
        $.toast({
          heading             : 'Success',
          text                : response.success_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'success',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {

        $(".button-disabled").removeAttr("disabled");
        if( response.formErrors)
        {
          $.toast({
            heading             : 'Error',
            text                : response.errors,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
        else
        {
          $.toast({
            heading             : 'Error',
            text                : response.error_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
        {
        $('.reset')[0].reset();
         }
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
    },
    error:function(response){
        $.toast({
          heading             : 'Error',
          text                : "Server Error",
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : 4000,
          position            : 'top-right'
        });

    }
  });
}





function emailform(form)
{
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
       $('.reset')[0].reset();
       $("#employeeEmail").modal('hide');
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}
 // *************** submit function******************
