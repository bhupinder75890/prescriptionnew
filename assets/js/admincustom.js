jQuery(document).ready(function () {

var base_url = $('.base_url').val();


   // datepicker
   // $( function() {
   //  $('.datepicker').datepicker({
   //     dateFormat: 'mm-dd-yy',
   //     changeMonth: true,
   //     changeYear: true,
   //     // yearRange: "1950:2020"
   //     yearRange: "1950:"+new Date().getFullYear(),
   //  });
   // });

   //datepicker
   jQuery.validator.addMethod("regex", function(value, element, param){
            return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
         }, "Enter valid email address");

    // login
    $("#loginform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
        regex: "",
     },
     password: {
       required: true
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
     password: {
       required: "Please enter password",

     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // login

 // password update


 $("#passwordUpdate").validate({
 rules:
 {
  pass: {
    required: true,
    minlength: 6
  },
  cpass: {
    required: true,
    equalTo: "#pass",
   minlength: 6
  },
 },
 messages:
 {

  pass: {
    required: "Please enter password",
    minlength: "Your password must be at least 6 characters long",

 },

  cpass: {
    required: "Please enter confirm password",
    minlength: "Your password must be at least 6 characters long",
    equalTo:   "Confirm password is not matched."
 },
 },
 submitHandler: function (form)
 {
  formSubmit(form);
 }
 });
 // password update

    // forgotpassword
    $("#forgotpasswordform").validate({
   rules:
   {
     email: {
       required: true,
       email:true,
       regex: "",
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // forgotpassword



    // Newpassword
    $("#newpasswordform").validate({
   rules:
   {
     password: {
       required: true
     },
   },
   messages:
   {
     password: {
       required: "Please enter password",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // Newpassword
  // back
 $('.cancel').click(function(){
		parent.history.back();
		return false;
	});



  // back

  // add pharmacy
  $("#addpharmacy").validate({
  rules:
  {
   name: {
     required: true
   },
   email: {
     required: true,
     email:true,
     regex: "",
   },
   phone: {
     required: true,
     number:true,
   },
   address: {
     required: true
   },
  },
  messages:
  {
   name: {
     required: "Please enter name ",
   },

   email: {
     required: "Please enter email",
   },
   phone: {
     required: "Please enter phone number",
   },
   address: {
     required: "Please enter address",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);

  }
  });

  // add pharmacy

  // add doctor
  $("#adddoctor").validate({
  rules:
  {
   name: {
     required: true
   },
   userEmail: {
     required: true,
     email:true,
     regex: "",
   },
   phone: {
     required: true,
     number:true,
   },
   license: {
     required: true,
   },
  },
  messages:
  {
   name: {
     required: "Please enter name ",
   },

   email: {
     required: "Please enter email",
   },
   phone: {
     required: "Please enter phone number",
   },
   license: {
     required: "Please enter license number",
   },
  },
  submitHandler: function (form)
  {
     formSubmit(form);

  }
  });

  // add doctor


  // delete pharmacy
$('body').on('click','.pharmacydelete',function(){

  var id = $(this).attr('data-id');
  var conf = confirm("Are you sure to delete this record");
  if(conf == true)
  {

  $.ajax({
    dataType:'json',
    url :base_url + 'admin/pharmacyDelete',
    type :'post',
    data : {
      id:id
    },
    success: function(response){

      $.toast().reset('all');
    var delayTime = 3000;
    if(response.delayTime)
    delayTime = response.delayTime;
    if (response.success)
    {
      // $(".data"+id).hide();

      $.toast({
        heading             : 'Success',
        text                : response.success_message,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'success',
        hideAfter           : delayTime,
        position            : 'top-right'
      });

        setTimeout(function() {  window.location.href = base_url+'admin/pharmacylist';},1000);
    }
    else
    {
      if( response.formErrors)
      {
        $.toast({
          heading             : 'Error',
          text                : response.errors,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {
        jQuery('#InputEmail').val('');
        $.toast({
          heading             : 'Error',
          text                : response.error_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
    }

    }
  });
}

});
// delete Pharmacy

// delete pharmacy
$('body').on('click','.doctordelete',function(){

var id = $(this).attr('data-id');
var conf = confirm("Are you sure to delete this record");
if(conf == true)
{

$.ajax({
  dataType:'json',
  url :base_url + 'admin/doctorDelete',
  type :'post',
  data : {
    id:id
  },
  success: function(response){

    $.toast().reset('all');
  var delayTime = 3000;
  if(response.delayTime)
  delayTime = response.delayTime;
  if (response.success)
  {
    // $(".data"+id).hide();

    $.toast({
      heading             : 'Success',
      text                : response.success_message,
      loader              : true,
      loaderBg            : '#fff',
      showHideTransition  : 'fade',
      icon                : 'success',
      hideAfter           : delayTime,
      position            : 'top-right'
    });

      setTimeout(function() {  window.location.href = base_url+'admin/doctorlist';},1000);
  }
  else
  {
    if( response.formErrors)
    {
      $.toast({
        heading             : 'Error',
        text                : response.errors,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'error',
        hideAfter           : delayTime,
        position            : 'top-right'
      });
    }
    else
    {
      jQuery('#InputEmail').val('');
      $.toast({
        heading             : 'Error',
        text                : response.error_message,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'error',
        hideAfter           : delayTime,
        position            : 'top-right'
      });
    }
  }

  }
});
}

});
// delete doctor

// doctor perpage
$('body').on('change','.doctorperpage',function(){

      var perpage = $(this).val();
      var searchtext = $('.searchtext').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'admin/getdoctorperpage',
        type :'post',
        data : {
          perpage:perpage,searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          doctorrowmanage(response.result);

        }
      });

    });
// doctor perpage
// doctor pagination
$('body').on('click','.doctorpagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');
   var searchtext = $('.searchtext').val();
   var perpage = $('.doctorperpage').val();

   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'admin/getdoctorperpage/'+pageno,
     type :'post',
     data : {
       perpage:perpage,page:pageno,searchtext:searchtext
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     doctorrowmanage(response.result);

     }
   });
 });
// doctor pagination
// doctor search
$('body').on('keyup','.searchtext',function(){

      var searchtext = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'admin/getdoctorperpage',
        type :'post',
        data : {
          searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          doctorrowmanage(response.result);

        }
      });


    });
// doctor search
// pharmacy perpage
$('body').on('change','.pharmacyperpage',function(){

      var perpage = $(this).val();
      var searchtext = $('.searchtext').val();


      $.ajax({
        dataType:'json',
        url :base_url + 'admin/getpharmacyperpage',
        type :'post',
        data : {
          perpage:perpage,searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          pharmacyrowmanage(response.result);

        }
      });

    });
// pharmacy perpage
// pharmacy perpage
// ph pagination
$('body').on('click','.pharmacypagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');
   var searchtext = $('.searchtext').val();
    var perpage = $('.pharmacyperpage').val();
   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'admin/getpharmacyperpage/'+pageno,
     type :'post',
     data : {
       perpage:perpage,page:pageno,searchtext:searchtext
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     pharmacyrowmanage(response.result);

     }
   });
 });
// doctor pagination
// pharmacy perpage
// search ph
$('body').on('keyup','.phsearchtext',function(){

      var searchtext = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'admin/getpharmacyperpage',
        type :'post',
        data : {
          searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          pharmacyrowmanage(response.result);

        }
      });


    });
// search ph

// prescription perpage
$('body').on('change','.prescriptionperpage',function(){

      var perpage = $(this).val();
      var searchtext = $('.searchtext').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'admin/getprescriptionperpage',
        type :'post',
        data : {
          perpage:perpage,searchtext:searchtext
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          prescriptionrowmanage(response.result);

        }
      });

    });
// prescription perpage
// prescription pagination
$('body').on('click','.prescriptionpagination a',function(e){
   e.preventDefault();
   var pageno = $(this).attr('data-ci-pagination-page');
   var searchtext = $('.searchtext').val();
   var perpage = $('.prescriptionperpage').val();

   if(!pageno)
   {
     pageno = 1;
   }
   $.ajax({
   dataType:'json',
     url :base_url + 'admin/getprescriptionperpage/'+pageno,
     type :'post',
     data : {
       perpage:perpage,page:pageno,searchtext:searchtext
     },
     beforeSend  : function () {
       $(".loader_panel").css('display','block');
     },
     complete: function () {
       $(".loader_panel").css('display','none');
     },
     success: function(response){

     prescriptionrowmanage(response.result);

     }
   });
 });

// perpage prescription

// search prescription
$('body').on('keyup','.searchprescription',function(){

    var searchtext = $(this).val();

    $.ajax({
      dataType:'json',
      url :base_url + 'admin/getprescriptionperpage',
      type :'post',
      data : {
        searchtext:searchtext
      },
      beforeSend  : function () {
        $(".loader_panel").css('display','block');
      },
      complete: function () {
        $(".loader_panel").css('display','none');
      },
      success: function(response){

        prescriptionrowmanage(response.result);

      }
    });


  });

// search prescription

// view prescription
$('body').on('click','.viewprescription',function(){

 var id = $(this).attr('data-id');
 $.ajax({
   dataType:'json',
   url :base_url + 'admin/prescriptionView',
   type :'post',
   data : {
     id:id
   },
   beforeSend  : function () {
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
   },
   success: function(response){
   if (response.success)
   {
     var gg = response.result;
     var medicine = response.medicine;
     var rows = '';
     var dates = new Date(Date.parse(gg.date));
     var month = dates.getMonth();
     var day = dates.getDate();
     var year = dates.getFullYear();
      var date = ''+day+'-'+month+'-'+year;
   rows += '<p><b>Order No</b> : '+gg.orderNo+' </p>';
   rows += '<p><b>Patient s Name</b> : '+gg.patientName+' </p>';
   rows += '<p><b>Email</b> : '+gg.email+' </p>';
   rows += '<p><b>Phone Number </b> : '+gg.phone+' </p>';
   rows += '<p><b>IC </b> : '+gg.ic+' </p>';
   rows += '<p><b>Pharmacy  </b> : '+gg.pharmacy+' </p>';
   rows += '<p><b>Date  </b> : '+date+' </p>';
   rows += '<p><b>Total order  </b> : '+gg.totalamount+' </p>';
   rows += '<p><b>Amount paid by insurance  </b> : '+gg.copay+' </p>';
   rows += '<p><b>Delivery Charges  </b> : '+gg.deliveryCharges+' </p>';
   rows += '<p><b>Amount paid by patient  </b> : '+gg.payableAmount+' </p>';
   var row ='';
    if(medicine)
     {
       $.each( medicine, function( key, value ) {
         var s = key + 1;
         row += '<tr> ';
         row += '<td> '+s+'</td>';
         row += '<td> '+value.drugName+'</td>';
         row += '<td> '+value.strength+'</td>';
         row += '<td> '+value.drugDose+'</td>';
         if(value.duration == 1)
         {
         row += '<td>Day</td>';
         }
         else if(value.duration == 2)
         {
         row += '<td>Week</td>';
         }
         else if(value.duration == 3)
         {
         row += '<td>Month</td>';
         }

         if(value.frequency == 1)
         {
         row += '<td>Daily</td>';
         }
         else if(value.frequency == 2)
         {
           row += '<td>Bd</td>';
         }
         else if(value.frequency == 3)
         {
           row += '<td>Tds</td>';
         }
         else if(value.frequency == 4)
         {
           row += '<td>Qds</td>';
         }
         else if(value.frequency == 5)
         {
           row += '<td>Om</td>';
         }
         else if(value.frequency == 6)
         {
           row += '<td>On</td>';
         }
         else if(value.frequency == 7)
         {
           row += '<td>5 day</td>';
         }
         else if(value.frequency == 8)
         {
           row += '<td>6 day</td>';
         }
         else if(value.frequency == 9)
         {
           row += '<td>Alternative</td>';
         }
         else if(value.frequency == 10)
         {
           row += '<td>Monthly</td>';
         }
         else if(value.frequency == 10)
         {
           row += '<td>Weekly</td>';
         }
         else if(value.frequency == 10)
         {
           row += '<td>5 day/week</td>';
         }

         row += '<td> '+value.no+'</td>';
         row += '<td> '+value.quantity+'</td>';
         row += '<td> '+value.price+'</td>';
         row += '<td> '+value.subtotal+'</td>';
         row += '<td> '+value.instruction+'</td>';
         row += '</tr>';

     });
     }

   $('.bodydata').html(rows);
   $('.tableresrow').html(row);
   $('#prescriptionmodal').modal('show');
   }

 }
});
});

// view prescription
$('body').on('click','.export',function(){
 var id = $('.pharmacyId').val();
 if(id != '')
 {
   window.location.href = base_url+'admin/Export/'+id;
 }
 else
 {
   window.location.href = base_url+'admin/Export/';
 }
});
// close jqueryal

$('body').on('change', '.file', function()
  {
     readURL(this);
  });

});


 // doctormanage row
 function doctorrowmanage(data)
   {
     var base_url = $('.base_url').val();

    var rows = '';
    var a = data.start + 1;

    if(data.result.length != 0)
    {
   $.each( data.result, function( key, value ) {

    rows += '<tr>';
     rows += '<td>'+a+'</td>';
     rows += '<td>'+value.name+'</td>';
     rows += '<td>'+value.userEmail+'</td>';
     rows += '<td>'+value.phone+'</td>';
     rows += '<td>'+value.license+'</td>';
     rows += '<td><a href="'+base_url+'admin/doctor/edit/'+value.userId+'"><i class="fa fa-edit"></i></a><a class="doctordelete" data-id="'+value.userId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
     rows += '</tr>';
    a++;
 });
 }
 else
 {
    rows += '<tr>';
    rows += '<td colspan="5">No Record Found</td>';
    rows += '</tr>';
 }
 $(".doctordata").html(rows);
 $(".doctorpagination").html(data.links);

}
 // doctormanage row

 // pharmacy row
 function pharmacyrowmanage(data)
   {
     var base_url = $('.base_url').val();

    var rows = '';
    var a = data.start + 1;

    if(data.result.length != 0)
    {
   $.each( data.result, function( key, value ) {

    rows += '<tr>';
     rows += '<td>'+a+'</td>';
     rows += '<td>'+value.name+'</td>';
     rows += '<td>'+value.email+'</td>';
     rows += '<td>'+value.phone+'</td>';
     rows += '<td><a href="'+base_url+'admin/pharmacy/edit/'+value.pharmacyId+'"><i class="fa fa-edit"></i></a><a class="pharmacydelete" data-id="'+value.pharmacyId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
     rows += '</tr>';
    a++;
 });
 }
 else
 {
    rows += '<tr>';
    rows += '<td colspan="5">No Record Found</td>';
    rows += '</tr>';
 }
 $(".pharmacydata").html(rows);
 $(".pharmacypagination").html(data.links);

}
 // pharmacy row

 // prescription rows
 function prescriptionrowmanage(data)
   {
     var base_url = $('.base_url').val();

    var rows = '';
    var a = data.start + 1;

    if(data.result.length != 0)
    {
   $.each( data.result, function( key, value ) {
   var dates = new Date(Date.parse(value.date));
   var month = dates.getMonth();
   var day = dates.getDate();
   var year = dates.getFullYear();
    var date = ''+day+'-'+month+'-'+year;
    rows += '<tr>';
     rows += '<td>'+a+'</td>';
     rows += '<td>'+date+'</td>';
     rows += '<td>'+value.orderNo+'</td>';
     rows += '<td>'+value.doctor+'</td>';
     rows += '<td>'+value.patientName+'</td>';
     rows += '<td>'+value.email+'</td>';
     rows += '<td>'+value.phone+'</td>';
     rows += '<td>'+value.pharmacy+'</td>';
     rows += '<td>'+value.payableAmount+'</td>';
     rows += '<td>'+value.copay+'</td>';
     rows += '<td>'+value.totalamount+'</td>';
     if(value.delivery == 1)
     {
     rows +='<td>Delivery</td>';
     }
     else
     {
       rows +='<td>Self Pick Up</td>';
     }
     rows += '<td><a href="'+base_url+'admin/convertimage/'+value.prescriptionId+'" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a><a data-id="'+value.prescriptionId+'" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a></td>';
     rows += '</tr>';
    a++;
 });
 }
 else
 {
    rows += '<tr>';
    rows += '<td colspan="7">No Record Found</td>';
    rows += '</tr>';
 }
 $(".prescriptiondata").html(rows);
 $(".prescriptionpagination").html(data.links);

}
 // prescription rows
 // *************** image read*************
 function readURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.showimage').attr('src', e.target.result);
            $('.showimage').attr('height','100');
            $('.showimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }
 // *************** image read*************
 // image read

 // *************** submit function******************

 function formSubmit(form)
{
  $.ajax({
    url         : form.action,
    type        : form.method,
    data        : new FormData(form),
    enctype : 'multipart/form-data',
    contentType : false,
    cache       : false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    processData : false,
    dataType    : "json",
    beforeSend  : function () {
      $(".button-disabled").attr("disabled", "disabled");
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
        $(".button-disabled").attr("disabled",false);
    },
    success: function (response) {

      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      $.toast().reset('all');
      var delayTime = 3000;
      if(response.delayTime)
      delayTime = response.delayTime;
      if (response.success)
      {
        $.toast({
          heading             : 'Success',
          text                : response.success_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'success',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {

        $(".button-disabled").removeAttr("disabled");
        if( response.formErrors)
        {
          $.toast({
            heading             : 'Error',
            text                : response.errors,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
        else
        {
          $.toast({
            heading             : 'Error',
            text                : response.error_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
        {
        $('.reset')[0].reset();
         }
      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
    },
    error:function(response){
        $.toast({
          heading             : 'Error',
          text                : "Server Error",
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : 4000,
          position            : 'top-right'
        });

    }
  });
}



function emailform(form)
{
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
       $('.reset')[0].reset();
       $("#employeeEmail").modal('hide');
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}
 // *************** submit function******************
