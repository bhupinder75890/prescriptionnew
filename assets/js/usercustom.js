jQuery(document).ready(function () {

var base_url = $('.base_url').val();


    // login
    $("#loginform").validate({
   rules:
   {
     email: {
       required: true
     },
     password: {
       required: true
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
     password: {
       required: "Please enter password",

     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });
 // login


 // register
 $("#registerform").validate({
rules:
{
  email: {
    required: true
  },
  password: {
    required: true,
    minlength: 6
  },
  confirmpassword: {
    required: true,
    equalTo: "#password",
    minlength: 6
  },
  name: {
    required: true
  },
  phone: {
    required: true,
    number:true
  },
},
messages:
{
  email: {
    required: "Please enter email address",
  },
  password: {
    required: "Please enter password",
  },
  confirmpassword: {
    required: "Please enter confirm password",
    equalTo:"Confirm password is not matched"
  },
  name: {
    required: "Please enter name",
  },
  phone: {
    required: "Please enter phone",
  },
},
submitHandler: function (form)
{
  formSubmit(form);
}
});
 // register

    // verify
    $("#verifyform").validate({
   rules:
   {
     otp: {
       required: true
     },
   },
   messages:
   {
     otp: {
       required: "Please enter otp",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });
 // verify


    // forgotpassword
    $("#forgotpasswordform").validate({
   rules:
   {
     email: {
       required: true
     },
   },
   messages:
   {
     email: {
       required: "Please enter email address",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // forgotpassword



    // Newpassword
    $("#newpasswordform").validate({
   rules:
   {
     password: {
       required: true
     },
   },
   messages:
   {
     password: {
       required: "Please enter password",
     },
   },
   submitHandler: function (form)
   {
     formSubmit(form);
   }
 });

 // Newpassword
  // back
 $('.cancel').click(function(){
		parent.history.back();
		return false;
	});



  // back

//brocker
$("#brockerUpdate").validate({
rules:
{
 stub: {
   required: true
 },
 description: {
   required: true
 },
 exchange: {
   required: true
 },
},
messages:
{
 stub: {
   required: "Please enter stub",
 },
 description: {
   required: "Please enter description",
 },
 exchange: {
   required: "Please enter exchange",
 },
},
submitHandler: function (form)
{
 brockerform(form);
}
});

$("#brockerUse").validate({
rules:
{
 stubId: {
   required: true
 },
},
messages:
{
 stubId: {
   required: "Please select stub",
 },
},
submitHandler: function (form)
{
 formSubmit(form);
}
});

//brocker
// Live trading
//brocker
$("#livetradingUpdate").validate({
rules:
{
 app: {
   required: true
 },
 type: {
   required: true
 },
 apiKey: {
   required: true
 },
 publicKey: {
   required: true
 },
},
messages:
{
 app: {
   required: "Please enter app name",
 },
 type: {
   required: "Please enter type",
 },
 apiKey: {
   required: "Please enter api key",
 },
 publicKey: {
   required: "Please enter public key",
 },
},
submitHandler: function (form)
{
  formSubmit(form);

}
});
// Live trading
// pass



$("#passwordUpdate").validate({
rules:
{
 pass: {
   required: true,
   minlength: 6
 },
 cpass: {
   required: true,
   equalTo: "#pass",
  minlength: 6
 },
},
messages:
{

 pass: {
   required: "Please enter password",
   minlength: "Your password must be at least 6 characters long",

},

 cpass: {
   required: "Please enter confirm password",
   minlength: "Your password must be at least 6 characters long",
   equalTo:   "Confirm password is not matched."
},
},
submitHandler: function (form)
{
 formSubmit(form);
}
});

jQuery.validator.addMethod("ckrequired", function (value, element) {
           var idname = $(element).attr('id');
           var editor = CKEDITOR.instances[idname];
           var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
           if (ckValue.length === 0) {
//if empty or trimmed value then remove extra spacing to current control
               $(element).val(ckValue);
           } else {
//If not empty then leave the value as it is
               $(element).val(editor.getData());
           }
           return $(element).val().length > 0;
       }, "This field is required");



// countryAdd
$("#addcountry").validate({
     ignore: [],
    rules:
    {

      name:{
        required: true,
      },
    },
    messages:
    {

      name: {
        required: "Please enter country",
      },
    },
    submitHandler: function (form)
    {
      formSubmit(form);
    }
  });

  // country add


  // setting save
  $("#settingupdate").validate({

      rules:
      {

        username:{
          required: true,
        },
        password:{
          required: true,
        },
        ip:{
          required: true,
        },
        link:{
          required: true,
        },
      },
      messages:
      {

        username: {
          required: "Please enter username",
        },
        password: {
          required: "Please enter password",
        },
        ip: {
          required: "Please enter ip",
        },
        link: {
          required: "Please enter link",
        },

      },
      submitHandler: function (form)
      {
        formSubmit(form);
      }
    });

    // contactsave

    //resendotp

    $('body').on('click','.resendotp',function(){

      var reference = $(this).attr('data-id');

      $.ajax({
        dataType:'json',
        url :base_url + 'resendotp',
        type :'post',
        data : {
          reference:reference
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          $.toast().reset('all');
        var delayTime = 3000;
        if(response.delayTime)
        delayTime = response.delayTime;
        if (response.success)
        {

          $.toast({
            heading             : 'Success',
            text                : response.success_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'success',
            hideAfter           : delayTime,
            position            : 'top-right'
          });

        }
        else
        {
          if( response.formErrors)
          {
            $.toast({
              heading             : 'Error',
              text                : response.errors,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'error',
              hideAfter           : delayTime,
              position            : 'top-right'
            });
          }
          else
          {
            jQuery('#InputEmail').val('');
            $.toast({
              heading             : 'Error',
              text                : response.error_message,
              loader              : true,
              loaderBg            : '#fff',
              showHideTransition  : 'fade',
              icon                : 'error',
              hideAfter           : delayTime,
              position            : 'top-right'
            });
          }
        }

        }
      });


    });

    // resendotp

    // linkedorderperpage
    $('body').on('change','.linkedperpage',function(){

      var perpage = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'user/linkedorderperpage',
        type :'post',
        data : {
          perpage:perpage
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          linkedordermanageRow(response.result);

        }
      });

    });
    // linkedorderperpage

    // refresh linked order
     $('body').on('click','.linkedrefesh',function(){

      var perpage = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'user/linkedorderperpage',
        type :'post',
        data : {
          perpage:perpage
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response)
        {
          linkedordermanageRow(response.result);
        }
      });

    });
    // refresh linked order


    // linkedorder pagination
    $('body').on('click','.linkedorderpagination a',function(e){
    // $('.linkedorderpagination').on('click','a',function(e){
      e.preventDefault();
      var pageno = $(this).attr('data-ci-pagination-page');
      var perpage = $('.linkedperpage').val();
      var exchange = $('.exchange').val();
      var position = $('.position').val();
      var date = $('.date').val();
      if(!pageno)
      {
        pageno = 1;
      }
      $.ajax({
      dataType:'json',
        url :base_url + 'user/linkedorderperpage/'+pageno,
        type :'post',
        data : {
          perpage:perpage,page:pageno,exchange:exchange,position:position,date:date
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

        linkedordermanageRow(response.result);

        }
      });
    });
    // linkedorder pagination
    // linkedorder search
    $('body').on('click','.searchlinked',function(){

      var perpage = $('.linkedperpage').val();
      var exchange = $('.exchange').val();
      var position = $('.position').val();
      var date = $('.date').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'user/linkedorderperpage',
        type :'post',
        data : {
          perpage:perpage,exchange:exchange,position:position,date:date
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          linkedordermanageRow(response.result);

        }
      });


    });
    // linkedorder search



    // history
    // historyperpage
    $('body').on('change','.historyperpage',function(){

      var perpage = $(this).val();

      $.ajax({
        dataType:'json',
        url :base_url + 'user/historyperpage',
        type :'post',
        data : {
          perpage:perpage
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          historymanageRow(response.result);

        }
      });


    });

    // historyperpage

    // history pagination
    $('body').on('click','.historypagination a',function(e){
    // $('.linkedorderpagination').on('click','a',function(e){
      e.preventDefault();
      var pageno = $(this).attr('data-ci-pagination-page');
      var perpage = $('.linkedperpage').val();
      var exchange = $('.exchange').val();
      var position = $('.position').val();
      var date = $('.date').val();
      if(!pageno)
      {
        pageno = 1;
      }
      $.ajax({
      dataType:'json',
        url :base_url + 'user/historyperpage/'+pageno,
        type :'post',
        data : {
          perpage:perpage,page:pageno,exchange:exchange,position:position,date:date
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

        historymanageRow(response.result);

        }
      });
    });
    // history pagination
    // history search
    $('body').on('click','.searchhistory',function(){

      var perpage = $('.linkedperpage').val();
      var exchange = $('.exchange').val();
      var position = $('.position').val();
      var date = $('.date').val();

      $.ajax({
        dataType:'json',
        url :base_url + 'user/historyperpage',
        type :'post',
        data : {
          perpage:perpage,exchange:exchange,position:position,date:date
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          historymanageRow(response.result);

        }
      });


    });
    // history search
    // history

    // submit linked order limit
    function formSubmitorderLimit(form)
    {
      var base_url = $('.base_url').val();
     $.ajax({
       url         : form.action,
       type        : form.method,
       data        : new FormData(form),
       enctype : 'multipart/form-data',
       contentType : false,
       cache       : false,
       headers     : {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       },
       processData : false,
       dataType    : "json",
       beforeSend  : function () {
         $(".button-disabled").attr("disabled", "disabled");
         $(".loader_panel").css('display','block');
       },
       complete: function () {
         $(".loader_panel").css('display','none');
           $(".button-disabled").attr("disabled",false);
       },
       success: function (response) {

         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
         $.toast().reset('all');
         var delayTime = 3000;
         if(response.delayTime)
         delayTime = response.delayTime;
         if (response.success)
         {
            $("#mytrade").modal('hide');

             getlinkedorder();

           $.toast({
             heading             : 'Success',
             text                : response.success_message,
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'success',
             hideAfter           : delayTime,
             position            : 'top-right'
           });
         }
         else
         {

           $(".button-disabled").removeAttr("disabled");
           if( response.formErrors)
           {
             $.toast({
               heading             : 'Error',
               text                : response.errors,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
           else
           {
             $.toast({
               heading             : 'Error',
               text                : response.error_message,
               loader              : true,
               loaderBg            : '#fff',
               showHideTransition  : 'fade',
               icon                : 'error',
               hideAfter           : delayTime,
               position            : 'top-right'
             });
           }
         }
         if(response.ajaxPageCallBack)
         {
           response.formid = form.id;
           ajaxPageCallBack(response);
         }
         if(response.resetform)
           {

           $('.reset')[0].reset();
            }


            if(response.hide)
            {
              // console.log('fff');
              $('.hidepopup').modal('hide');
            }

         if(response.url)
         {
           if(response.delayTime)
           setTimeout(function() { window.location.href=response.url;}, response.delayTime);
           else
           window.location.href=response.url;
         }
       },
       error:function(response){
           $.toast({
             heading             : 'Error',
             text                : "Server Error",
             loader              : true,
             loaderBg            : '#fff',
             showHideTransition  : 'fade',
             icon                : 'error',
             hideAfter           : 4000,
             position            : 'top-right'
           });

       }
     });
    }

    // submit linked order limit

    // getlinkedorder
    function getlinkedorder()
    {
      var base_url = $('.base_url').val();

      var perpage = 10;

      $.ajax({
        dataType:'json',
        url :base_url + 'user/linkedorderperpage',
        type :'post',
        data : {
          perpage:perpage
        },
        beforeSend  : function () {
          $(".loader_panel").css('display','block');
        },
        complete: function () {
          $(".loader_panel").css('display','none');
        },
        success: function(response){

          linkedordermanageRow(response.result);

        }
      });
    }
    // getlinkedorder


    // linked order data manage
    function linkedordermanageRow(data)
    {
     var rows = '';
     var a = data.start + 1;

     if(data.result.length != 0)
     {
    $.each( data.result, function( key, value ) {
 // console.log(value.live_price);
    var dates = new Date(Date.parse(value.date));
    var dates1 = new Date(Date.parse(value.time));
     var minutes = dates1.getMinutes();
     if(minutes > 10)
     {
       var time = '' + parseInt(dates1.getHours()+5) + ':0' + parseInt(dates1.getMinutes() + 30);
     }
     else
     {
       var time = '' + parseInt(dates1.getHours()+5) + ':'+ parseInt(dates1.getMinutes() + 30);
     }
     var month = dates.getMonth();
     var day = dates.getDate();
     var year = dates.getFullYear()
    var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
      rows += '<td>'+a+'</td>';
      rows += '<td>'+value.exchange+'</td>';
      rows += '<td>'+value.price+'</td>';
      if(value.live_price)
      {
      rows += '<td>'+value.live_price+'</td>';
      }
      else
      {
        rows += '<td>0.00</td>';
      }
      rows += '<td>'+value.position+'</td>';
      if(value.Qty)
      {
      rows += '<td>'+value.Qty+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }

      if(value.profit)
      {
      rows += '<td>'+value.profit+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      rows += '<td>'+value.old+'</td>';
      if(value.TP)
      {
      rows += '<td>'+value.TP+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      if(value.SL)
      {
      rows += '<td>'+value.SL+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      rows += '<td>'+value.strategy+'</td>';
      rows += '<td>'+date+'</td>';
      rows += '<td>'+time+'</td>';
      rows += '<td><a class="closetrade btn btn-rounded" data-id="'+value.linkedorderId+'">Close Trade</a></td>';
      rows += '<td><a class="limittrade btn btn-rounded" data-id="'+value.linkedorderId+'">Trade Limit</a></td>';
      rows += '</tr>';
     a++;
  });
  }
  else
  {
     rows += '<tr>';
     rows += '<td colspan="5">No Record Found</td>';
     rows += '</tr>';
  }



  $(".linkedorderrows").html(rows);
  $(".linkedorderpagination").html(data.links);

}
    // linked order data manage

    // history rows
    function historymanageRow(data)
    {
     var rows = '';
     var a = data.start + 1;

     if(data.result.length != 0)
     {
    $.each( data.result, function( key, value ) {
 // console.log(value.live_price);
    var dates = new Date(Date.parse(value.date));
    var dates1 = new Date(Date.parse(value.time));
     var minutes = dates1.getMinutes();
     if(minutes > 10)
     {
       var time = '' + parseInt(dates1.getHours()+5) + ':0' + parseInt(dates1.getMinutes() + 30);
     }
     else
     {
       var time = '' + parseInt(dates1.getHours()+5) + ':'+ parseInt(dates1.getMinutes() + 30);
     }
     var month = dates.getMonth();
     var day = dates.getDate();
     var year = dates.getFullYear()
    var date = ''+day+'-'+month+'-'+year;
     rows += '<tr>';
      rows += '<td>'+a+'</td>';
      rows += '<td>'+value.exchange+'</td>';
      rows += '<td>'+value.price+'</td>';
      if(value.live_price)
      {
      rows += '<td>'+value.live_price+'</td>';
      }
      else
      {
        rows += '<td>0.00</td>';
      }
      rows += '<td>'+value.position+'</td>';
      if(value.Qty)
      {
      rows += '<td>'+value.Qty+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }

      if(value.profit)
      {
      rows += '<td>'+value.profit+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      rows += '<td>'+value.old+'</td>';
      if(value.TP)
      {
      rows += '<td>'+value.TP+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      if(value.SL)
      {
      rows += '<td>'+value.SL+'</td>';
      }
      else
      {
        rows += '<td></td>';
      }
      rows += '<td>'+value.strategy+'</td>';
      rows += '<td>'+date+'</td>';
      rows += '<td>'+time+'</td>';
      rows += '</tr>';
     a++;
  });
  }
  else
  {
     rows += '<tr>';
     rows += '<td colspan="5">No Record Found</td>';
     rows += '</tr>';
  }



  $(".linkedorderrows").html(rows);
  $(".historypagination").html(data.links);

}
    // history rows

    // profile
    $("#profileUpdate").validate({

        rules:
        {

          name:{
            required: true,
          },
          phone:{
            required: true,
            number:true,
          },
          email:{
            required: true,
            email:true,
          },
        },
        messages:
        {
          name: {
            required: "Please enter name",
          },
          phone: {
            required: "Please enter phone",
          },
          email: {
            required: "Please enter email",
          },
        },
        submitHandler: function (form)
        {
          formSubmit(form);
        }
      });
    // profile

    //stock setting
    $("#stocksettingUpdate").validate({

        rules:
        {

          timeIn:{
            required: true,
          },
          timeOut:{
            required: true,
          },

        },
        messages:
        {
          timeIn: {
            required: "Please select timein",
          },
          timeOut: {
            required: "Please select timeout",
          },
        },
        submitHandler: function (form)
        {
          formSubmit(form);
        }
      });
    //stock setting

 // portfoilo
 $("#portfolioadd").validate({
   rules:
   {
     name: {
       required: true
     },
   },
   messages:
   {
     name: {
       required: "Please enter name",
     },
   },
   submitHandler: function (form)
   {
     formSubmitModal(form);
   }
 });

 // portfoilo

 // delete portfolio
$('body').on('click','.portfoliodelete',function(){

 var id = $(this).attr('data-id');
 var conf = confirm("Are you sure to delete this record");
 if(conf == true)
 {

 $.ajax({
   dataType:'json',
   url :base_url + 'user/portfolioDelete',
   type :'post',
   data : {
     id:id
   },
   success: function(response){

     $.toast().reset('all');
   var delayTime = 3000;
   if(response.delayTime)
   delayTime = response.delayTime;
   if (response.success)
   {
     // $(".data"+id).hide();
     portfoliogetdata();

     $.toast({
       heading             : 'Success',
       text                : response.success_message,
       loader              : true,
       loaderBg            : '#fff',
       showHideTransition  : 'fade',
       icon                : 'success',
       hideAfter           : delayTime,
       position            : 'top-right'
     });

       // setTimeout(function() {  location.reload(true);},1000);
   }
   else
   {
     if( response.formErrors)
     {
       $.toast({
         heading             : 'Error',
         text                : response.errors,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {
       jQuery('#InputEmail').val('');
       $.toast({
         heading             : 'Error',
         text                : response.error_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
   }

   }
 });
}

});
// delete portfolio


// edit portfoilo
$('body').on('click','.editportfolio',function(){

 var id = $(this).attr('data-id');

 $.ajax({
   dataType:'json',
   url :base_url + 'user/portfolioEdit',
   type :'post',
   data : {
     id:id
   },
   success: function(response){
   if (response.success)
   {
    $('#myModal').modal('show');
    $('.name').val(response.result.name);
    $(".portfoiloadd").attr('action',''+base_url+'user/portfolioUpdate/'+response.result.portfolioId+'');


   }
   }
 });


});
// edit portfoilo

// portfoilo pagination
$('body').on('click','.portfliopagination a',function(e){
// $('.linkedorderpagination').on('click','a',function(e){
  e.preventDefault();
  var pageno = $(this).attr('data-ci-pagination-page');
  if(!pageno)
  {
    pageno = 1;
  }
  $.ajax({
  dataType:'json',
    url :base_url + 'user/portfolioperpage/'+pageno,
    type :'post',
    beforeSend  : function () {
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
    },
    success: function(response){

    portfoilomanageRow(response.result);

    }
  });
});
// portfoilo pagination

//stcok
$("#stockadd").validate({
  rules:
  {
    tradeType: {
      required: true
    },
    sector: {
      required: true
    },
    buyPrice: {
      required: true
    },
    qty: {
      required: true
    },
    investement: {
      required: true
    },
  },
  messages:
  {
    tradeType: {
      required: "Please enter trade type",
    },
    sector: {
      required: "Please enter sector",
    },
    buyPrice: {
      required: "Please enter buy price",
    },
    qty: {
      required: "Please enter qty",
    },
    investement: {
      required: "Please enter investement",
    },
  },
  submitHandler: function (form)
  {
    formSubmitStock(form);
  }
});

$('body').on('keyup', '.buyPrice', function()
  {
    var inve = 0;
   var price = $('.buyPrice').val();
   var qty = $('.qty').val();
    inve = price * qty;
  $('.investement').val(inve);
  });

$('body').on('keyup', '.qty', function()
  {
    var inve = 0;
   var price = $('.buyPrice').val();
   var qty = $('.qty').val();
    inve = price * qty;
  $('.investement').val(inve);
  });

//stcok

// stock delete
$('body').on('click','.stockdelete',function(){

 var id = $(this).attr('data-id');
 var conf = confirm("Are you sure to delete this record");
 if(conf == true)
 {

 $.ajax({
   dataType:'json',
   url :base_url + 'user/stockDelete',
   type :'post',
   data : {
     id:id
   },
   success: function(response){

     $.toast().reset('all');
   var delayTime = 3000;
   if(response.delayTime)
   delayTime = response.delayTime;
   if (response.success)
   {
     stockgetdata();
     $.toast({
       heading             : 'Success',
       text                : response.success_message,
       loader              : true,
       loaderBg            : '#fff',
       showHideTransition  : 'fade',
       icon                : 'success',
       hideAfter           : delayTime,
       position            : 'top-right'
     });

   }
   else
   {
     if( response.formErrors)
     {
       $.toast({
         heading             : 'Error',
         text                : response.errors,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {
       jQuery('#InputEmail').val('');
       $.toast({
         heading             : 'Error',
         text                : response.error_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
   }

   }
 });
}

});
// delete portfolio

//edit stock

$('body').on('click','.editstock',function(){

 var id = $(this).attr('data-id');


 $.ajax({
   dataType:'json',
   url :base_url + 'user/stockEdit',
   type :'post',
   data : {
     id:id
   },
   success: function(response){
   if (response.success)
   {
    $('#myModal').modal('show');
    $('.modal-title').html('Edit Stock');
    $('.name').val(response.result.name);
    $('.sector').val(response.result.sector);
    $('.searchsymbol').val(response.result.symbol);
    $('.company').val(response.result.company);
    $('.SymboldataId').val(response.result.SymbolDataId);
    $('.buyPrice').val(response.result.buyPrice);
    $('.qty').val(response.result.qty);
    $('.investement').val(response.result.investement);
    $('.portfolioId').val(response.result.portfolioId);
    $(".stockadd").attr('action',''+base_url+'user/stockUpdate/'+response.result.stockId+'');


   }
   }
 });

});

//edit stock

// keyup search symbol
$('body').on('keyup','.searchsymbol',function(){

		var req = $(this).val();
		$.ajax({
			url:base_url + 'user/getsymbol',
			dataType: 'json',
			type: 'POST',
			data : {
				value : req
			},
			success:function(data){
				//console.log(data);
				var list = [];
				if(data.success == "true"){
					$.each(data.result, function(){
						var id = this.SymbolDataId;
						var value = this.Symbol;
						var company = this.Company;
						list.push('<li class="symbolname" data-name='+company+' data-id='+id+'>'+value+'</li>');
					});
				}
				else
        {

					list.push('<li class="symbolname" data-id="0">Not Found</li>');
				}
				$('.resultCat').html(list);
				$('.resultCat').css('display','block');
			},
		});
	});

	$('body').on('click','.symbolname',function(){
		var name = $(this).text();
		$(".searchsymbol").val(name);
		$('.resultCat').css('display','none');
		var symbolId = $(this).attr('data-id');
		var company = $(this).attr('data-name');
		$(".SymboldataId").val(symbolId);
		$(".company").val(company);

	});
// keyup search symbol

// close trade
$('body').on('click','.closetrade',function(){
  var id = $(this).attr('data-id');
  $('.linkedorderId').val(id);
  $('#myModal').modal('show');
});

// limit trade
$('body').on('click','.limittrade',function(){
  var id = $(this).attr('data-id');
  $('.limitlinkedorderId').val(id);
  $('#mytrade').modal('show');
});

$('body').on('click','.closedtrade',function(){
  var id = $('.linkedorderId').val();

  $.ajax({
    dataType:'json',
    url :base_url + 'user/linkedorderClose',
    type :'post',
    data : {
      id:id
    },
    success: function(response){

      $.toast().reset('all');
    var delayTime = 3000;
    if(response.delayTime)
    delayTime = response.delayTime;
    if (response.success)
    {
        $('#myModal').modal('hide');
        getlinkedorder();

      $.toast({
        heading             : 'Success',
        text                : response.success_message,
        loader              : true,
        loaderBg            : '#fff',
        showHideTransition  : 'fade',
        icon                : 'success',
        hideAfter           : delayTime,
        position            : 'top-right'
      });

    }
    else
    {
      if( response.formErrors)
      {
        $.toast({
          heading             : 'Error',
          text                : response.errors,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {
        $.toast({
          heading             : 'Error',
          text                : response.error_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
    }

    }
  });


});
// close trade



// limit form validation

$("#linkedorderlimitform").validate({
  rules:
  {
    tp: {
      required: true
    },
    sl: {
      required: true
    },
  },
  messages:
  {
    tp: {
      required: "Please enter tp",
    },
    sl: {
      required: "Please enter sl",
    },
  },
  submitHandler: function (form)
  {
    formSubmitorderLimit(form);
  }
});

// limit form validation

// contact form
// register
$("#contactform").validate({
rules:
{
 firstname: {
   required: true
 },
 lastname: {
   required: true,
 },
 email: {
   required: true,
   email:true,
 },
 subject: {
   required: true,
 },
 message: {
   required: true,
 },
},
messages:
{
 firstname: {
   required: "Please enter first name",
 },
 lastname: {
   required: "Please enter last name",
 },
 email:{
   required:"Please enter email address",
 },
 subject:{
   required:"Please enter subject",
 },
 message:{
   required:"Please enter message",
 },
},
submitHandler: function (form)
{
 formSubmit(form);
}
});
// register

// contact form


// close jquery

$('body').on('change', '.file', function()
  {
     readURL(this);
  });

});

function linkedrefesh()
{
  var perpage = $(this).val();

  $.ajax({
    dataType:'json',
    url :base_url + 'user/linkedorderperpage',
    type :'post',
    data : {
      perpage:perpage
    },
    beforeSend  : function () {
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
    },
    success: function(response){

      linkedordermanageRow2(response.result);

    }
  });

}

function linkedordermanageRow2(data)
{
 var rows = '';
 var a = data.start + 1;

 if(data.result.length != 0)
 {
$.each( data.result, function( key, value ) {
// console.log(value.live_price);
var dates = new Date(Date.parse(value.date));
var dates1 = new Date(Date.parse(value.time));
 var minutes = dates1.getMinutes();
 if(minutes > 10)
 {
   var time = '' + parseInt(dates1.getHours()+5) + ':0' + parseInt(dates1.getMinutes() + 30);
 }
 else
 {
   var time = '' + parseInt(dates1.getHours()+5) + ':'+ parseInt(dates1.getMinutes() + 30);
 }
 var month = dates.getMonth();
 var day = dates.getDate();
 var year = dates.getFullYear()
var date = ''+day+'-'+month+'-'+year;
 rows += '<tr>';
  rows += '<td>'+a+'</td>';
  rows += '<td>'+value.exchange+'</td>';
  rows += '<td>'+value.price+'</td>';
  if(value.live_price)
  {
  rows += '<td>'+value.live_price+'</td>';
  }
  else
  {
    rows += '<td>0.00</td>';
  }
  rows += '<td>'+value.position+'</td>';
  if(value.Qty)
  {
  rows += '<td>'+value.Qty+'</td>';
  }
  else
  {
    rows += '<td></td>';
  }

  if(value.profit)
  {
  rows += '<td>'+value.profit+'</td>';
  }
  else
  {
    rows += '<td></td>';
  }
  rows += '<td>'+value.old+'</td>';
  if(value.TP)
  {
  rows += '<td>'+value.TP+'</td>';
  }
  else
  {
    rows += '<td></td>';
  }
  if(value.SL)
  {
  rows += '<td>'+value.SL+'</td>';
  }
  else
  {
    rows += '<td></td>';
  }
  rows += '<td>'+value.strategy+'</td>';
  rows += '<td>'+date+'</td>';
  rows += '<td>'+time+'</td>';
  rows += '<td><a class="closetrade btn btn-rounded" data-id="'+value.linkedorderId+'">Close Trade</a></td>';
  rows += '<td><a class="limittrade btn btn-rounded" data-id="'+value.linkedorderId+'">Trade Limit</a></td>';
  rows += '</tr>';
 a++;
});
}
else
{
 rows += '<tr>';
 rows += '<td colspan="5">No Record Found</td>';
 rows += '</tr>';
}



$(".linkedorderrows").html(rows);
$(".linkedorderpagination").html(data.links);

}


 setInterval('linkedrefesh()', 2000);



// portfolio getdata
function portfoliogetdata()
{
  var base_url = $('.base_url').val();

  $.ajax({
    dataType:'json',
    url :base_url + 'user/portfolioperpage',
    type :'post',
    beforeSend  : function () {
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
    },
    success: function(response){

      portfoilomanageRow(response.result);

    }
  });

}

// linked order data manage
function portfoilomanageRow(data)
{
 var base_url = $('.base_url').val();
 var rows = '';
 var a = data.start + 1;
 if(data.result.length != 0)
 {
$.each( data.result, function( key, value ) {

 rows += '<tr>';
  rows += '<td>'+a+'</td>';
  rows += '<td>'+value.name+'</td>';
  rows += '<td><a class="editportfolio" data-id="'+value.portfolioId+'"><i class="fa fa-edit"></i></a><a class="portfoliodelete" data-id="'+value.portfolioId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
  rows += '<td><a href="'+base_url+'user/stock/'+value.portfolioId+'" class="btn btn-success" >Detail</a></td>';
  rows += '</tr>';
 a++;
});
}
else
{
  rows += '<tr>';
   rows += '<td colspan="2">No Record Found</td>';
   rows += '</tr>';
}


$(".portfoliorows").html(rows);
$(".portfliopagination").html(data.links);

}
// portfolio data manage


////stock

function stockgetdata()
{
  var base_url = $('.base_url').val();
  var id = $('.portfolioId').val();

  $.ajax({
    dataType:'json',
    url :base_url + 'user/stockperpage/'+id,
    type :'post',
    beforeSend  : function () {
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
    },
    success: function(response){

      stockmanageRow(response.result);

    }
  });
}

function stockmanageRow(data)
{
 var base_url = $('.base_url').val();
 var rows = '';
 var a = data.start + 1;
 if(data.result.length != 0)
 {
$.each( data.result, function( key, value ) {

 rows += '<tr>';
  rows += '<td>'+a+'</td>';
  if(value.tradeType == 1)
  {
  rows += '<td>Buy</td>';
  }
  else
  {
    rows += '<td>Sell</td>';
  }
    rows += '<td>'+value.sector+'</td>';
    rows += '<td>'+value.Company+'</td>';
    rows += '<td>'+value.buyPrice+'</td>';
    rows += '<td>'+value.qty+'</td>';
    rows += '<td>'+value.investement+'</td>';
  rows += '<td><a class="editstock" data-id="'+value.stockId+'"><i class="fa fa-edit"></i></a><a class="stockdelete" data-id="'+value.stockId+'"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
  rows += '</tr>';
 a++;
});
}
  else
  {
    rows += '<tr>';
     rows += '<td colspan="2">No Record Found</td>';
     rows += '</tr>';

}


$(".stockrows").html(rows);
$(".stockpagination").html(data.links);

}
////stock




 // *************** image read*************
 function readURL(input)
 {
    if (input.files && input.files[0])
     {
        var reader = new FileReader();
        reader.onload = function (e)
         {
            $('.showimage').attr('src', e.target.result);
            $('.showimage').attr('height','100');
            $('.showimage').attr('width','100');
         }
        reader.readAsDataURL(input.files[0]);
     }
 }
 // *************** image read*************
 // image read

 // *************** submit function******************

 function formSubmit(form)
{
  $.ajax({
    url         : form.action,
    type        : form.method,
    data        : new FormData(form),
    enctype : 'multipart/form-data',
    contentType : false,
    cache       : false,
    headers     : {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    processData : false,
    dataType    : "json",
    beforeSend  : function () {
      $(".button-disabled").attr("disabled", "disabled");
      $(".loader_panel").css('display','block');
    },
    complete: function () {
      $(".loader_panel").css('display','none');
        $(".button-disabled").attr("disabled",false);
    },
    success: function (response) {

      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
      $.toast().reset('all');
      var delayTime = 3000;
      if(response.delayTime)
      delayTime = response.delayTime;
      if (response.success)
      {
        $.toast({
          heading             : 'Success',
          text                : response.success_message,
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'success',
          hideAfter           : delayTime,
          position            : 'top-right'
        });
      }
      else
      {

        $(".button-disabled").removeAttr("disabled");
        if( response.formErrors)
        {
          $.toast({
            heading             : 'Error',
            text                : response.errors,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
        else
        {
          $.toast({
            heading             : 'Error',
            text                : response.error_message,
            loader              : true,
            loaderBg            : '#fff',
            showHideTransition  : 'fade',
            icon                : 'error',
            hideAfter           : delayTime,
            position            : 'top-right'
          });
        }
      }
      if(response.ajaxPageCallBack)
      {
        response.formid = form.id;
        ajaxPageCallBack(response);
      }
      if(response.resetform)
        {
        $('.reset')[0].reset();
         }

      if(response.url)
      {
        if(response.delayTime)
        setTimeout(function() { window.location.href=response.url;}, response.delayTime);
        else
        window.location.href=response.url;
      }
    },
    error:function(response){
        $.toast({
          heading             : 'Error',
          text                : "Server Error",
          loader              : true,
          loaderBg            : '#fff',
          showHideTransition  : 'fade',
          icon                : 'error',
          hideAfter           : 4000,
          position            : 'top-right'
        });

    }
  });
}




function formSubmitModal(form)
{
  var base_url = $('.base_url').val();
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
         $(".portfoiloadd").attr('action',''+base_url+'user/portfolioSave');

       portfoliogetdata();
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.resetform)
       {
       $('.reset')[0].reset();
        }


        if(response.hide)
        {
          // console.log('fff');
          $('.hidepopup').modal('hide');
        }

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}

function formSubmitStock(form)
{
  var base_url = $('.base_url').val();
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
         $(".portfoiloadd").attr('action',''+base_url+'user/portfolioSave');

       stockgetdata();
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.resetform)
       {

       $('.reset')[0].reset();
        }


        if(response.hide)
        {
          // console.log('fff');
          $('.hidepopup').modal('hide');
        }

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}



function brockerform(form)
{
 $.ajax({
   url         : form.action,
   type        : form.method,
   data        : new FormData(form),
   enctype : 'multipart/form-data',
   contentType : false,
   cache       : false,
   headers     : {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   },
   processData : false,
   dataType    : "json",
   beforeSend  : function () {
     $(".button-disabled").attr("disabled", "disabled");
     $(".loader_panel").css('display','block');
   },
   complete: function () {
     $(".loader_panel").css('display','none');
       $(".button-disabled").attr("disabled",false);
   },
   success: function (response) {

     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
     $.toast().reset('all');
     var delayTime = 3000;
     if(response.delayTime)
     delayTime = response.delayTime;
     if (response.success)
     {
       $("#apiKey").val(response.result.apiKey);
       $("#secret").val(response.result.secret);
       $.toast({
         heading             : 'Success',
         text                : response.success_message,
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'success',
         hideAfter           : delayTime,
         position            : 'top-right'
       });
     }
     else
     {

       $(".button-disabled").removeAttr("disabled");
       if( response.formErrors)
       {
         $.toast({
           heading             : 'Error',
           text                : response.errors,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
       else
       {
         $.toast({
           heading             : 'Error',
           text                : response.error_message,
           loader              : true,
           loaderBg            : '#fff',
           showHideTransition  : 'fade',
           icon                : 'error',
           hideAfter           : delayTime,
           position            : 'top-right'
         });
       }
     }
     if(response.ajaxPageCallBack)
     {
       response.formid = form.id;
       ajaxPageCallBack(response);
     }
     if(response.url)
     {
       if(response.delayTime)
       setTimeout(function() { window.location.href=response.url;}, response.delayTime);
       else
       window.location.href=response.url;
     }
   },
   error:function(response){
       $.toast({
         heading             : 'Error',
         text                : "Server Error",
         loader              : true,
         loaderBg            : '#fff',
         showHideTransition  : 'fade',
         icon                : 'error',
         hideAfter           : 4000,
         position            : 'top-right'
       });

   }
 });
}
 // *************** submit function******************
