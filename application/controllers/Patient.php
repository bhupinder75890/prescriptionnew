<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient extends CI_Controller {

  public function __construct() {
    parent::__construct();


    $this->load->model('common');
    $this->load->library("pagination");
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->helper(array('form', 'url'));

  }


  public function payment($id)
  {
    $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
    $date =  $nowUtc->format('d M Y');
    $data['date'] = $date;
    $result = $this->common->getrow('prescription',array("prescriptionId"=>$id));
    $merchant = $this->common->getrow('merchant',array("userId"=>$result->pharmacyId));
    $ord_totalamt = $result->payableAmount;
    $merchant_secret = $merchant->secret;
    $ord_mercID = $merchant->merchant;
    $ord_mercref = $result->orderNo;

    $amount = str_replace('.', '', $ord_totalamt);
    $hash =  sha1($merchant_secret . $ord_mercID . $ord_mercref . $amount);
    $data['result'] = $result;
    $data['hash'] = $hash;
    $data['merchant'] = $merchant;
    $this->load->view('patient/index',$data);
  }

  public function success()
  {
      if(!empty($_POST) && $_POST['returncode'] == 100)
      {
         $this->common->insert('payment',array("orderNo"=>$_POST['ord_mercref'],"returnCode"=>$_POST['returncode'],"amount"=>$_POST['ord_totalamt'],"patientEmail"=>$_POST['ord_email']));
         $this->common->update(array("orderNo"=>$_POST['ord_mercref']),array("paymentStatus"=>1),"prescription");
         $pres = $this->common->getrow('prescription',array("orderNo"=>$_POST['ord_mercref']));
         $logo = $this->common->getrow('pharmacy',array("userId"=>$pres->pharmacyId));
         $user = $this->common->getrow('users',array("userId"=>$pres->pharmacyId));
         $detail['postdata'] = $_POST;
         $message = $this->load->view('email/payment',$detail,true);
 		     $mail = $this->mailsend('payment','email2mkashif@gmail.com',$message);
 		     $aa = $this->mailsend('payment',$user->userEmail,$message);
         if($aa)
         {
           $data['logo'] = $logo;
          $this->load->view('patient/success',$data);
         }
       }
      else
      {
         $this->common->update(array("orderNo"=>$_POST['ord_mercref']),array("paymentStatus"=>2),"prescription");
         $this->load->view('patient/failed');
      }
  }



  public function mailsend($sub,$to,$msg)
  {
    $ci = & get_instance();
    $ci->email_var = array(
      'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
    );

    $ci->config_email = Array (
       'protocol' => "smpt",
       'smtp_host' => "business17.web-hosting.com",
       'smtp_port' => '587',
       'smtp_user' => 'admin@doctortouchrx.com',
      'smtp_pass' => '@Admin1978',
      'mailtype' => "html",
      'wordwrap' => TRUE,
      'crlf' => '\r\n',
      'charset' => "utf-8"
    );

    $ci->email->initialize($ci->config_email);
    $ci->email->set_newline("\r\n");
    $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
       $ci->email->to($to);
    $ci->email->subject($sub);
    $ci->email->message($msg);
    if ($ci->email->send()) {
      $ci->email->clear(TRUE);

      return TRUE;

    }
    else
    {
      //echo $ci->email->print_debugger();
      return FALSE;
    }
  }






}
