<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Pharmacy extends CI_Controller {

  public function __construct() {
    parent::__construct();


    $this->load->model('common');
    $this->load->library("pagination");
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->helper(array('form', 'url'));

  }


  public function index($id = null)
  {

    if(!empty($_POST))
    {
      $data = array(
        'userEmail' =>$_POST['email'],
        'userPassword' => md5($_POST['password']),
        'userType' =>2,
      );
      $result = $this->common->getrow('users',$data);

      if ($result == TRUE)
      {

        if($result->userType == 1 )
        {
            $session_data = array(
          'userId' => $result->userId,
          'email' => $result->userEmail,
          'type' =>$result->userType,
        );
         $this->session->set_userdata('doctorloggedin', $session_data);
        $output['url'] = base_url().'doctor/dashboard';
        }
        else if($result->userType == 2)
        {
            $name = $this->common->getrow('pharmacy',array("userId"=>$result->userId));
         $url = str_replace(' ','-',strtolower($name->name));
         $session_data = array(
          'userId' => $result->userId,
          'email' => $result->userEmail,
          'type' =>$result->userType,
          'url' =>$url,

        );
         $this->session->set_userdata('doctorloggedin', $session_data);

          $output['url'] = base_url().'pharmacy1/dashboard/'.$url;
        }

        $output['success'] ="true";
        $output['success_message'] ="Login Successfully";
        $output['delayTime'] ='3000';

      }
      else
      {
        $output['formErrors'] = "true";
        $output['errors'] = 'Invalid Username or Password.';
      }

      echo json_encode($output);
      exit;
    }
    else
    {
      $data['pages'] = $this->common->get('pages',array("userId"=>$id,"pagesStatus"=>1));
      $data['logo'] = $this->common->getrow('pharmacy',array("userId"=>$id));
      $data['userId'] = $id;
      $this->load->view('pharmacy/login',$data);
    }
  }

  public function securite()
  {
    if(!isset($this->session->userdata['doctorloggedin']['userId']) && $this->session->userdata['doctorloggedin']['type'] != 2 )
    {
      redirect('pharmacy');
    }
  }


  public function logout($id)
  {
    $this->session->sess_destroy();
    redirect('pharmacy/'.$id);
  }

  public function dashboard()
  {
    $this->securite();
    $this->load->view('pharmacy/header');
    $this->load->view('pharmacy/dashboard');
    $this->load->view('pharmacy/footer');

  }



  public function password()
  {
    $this->securite();
    $this->load->view('doctor/header');
    $this->load->view('doctor/password');
    $this->load->view('doctor/footer');

  }

  public function passwordUpdate()
  {
    $insert = $this->common->update(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),array("userPassword"=>md5($_POST['pass']),"userPass"=> $_POST['pass']),'users');
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Password Changed Successfully";
      $output['delayTime'] ='3000';
      $output['resetform'] ='true';
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Password Is Not Change";
    }

    echo json_encode($output);
    exit;
  }



  public function profile()
  {
    $this->securite();
    $data['result'] = $this->common->getpharmacyprofile(array("u.userId"=>$this->session->userdata['doctorloggedin']['userId']));
    $this->load->view('pharmacy/header');
    $this->load->view('pharmacy/profile',$data);
    $this->load->view('pharmacy/footer');
  }

  public function profileUpdate()
  {
    $postdata = $_POST;
    unset($postdata['email']);
    if(!empty($_FILES["image"]["name"]))
    {
      $name = str_replace(" ","_",$_FILES['image']['name']);
      $tmp_name = str_replace(" ","_",$_FILES['image']['tmp_name']);
      $error=$_FILES['image']['error'];
      $file_ext = pathinfo($name, PATHINFO_EXTENSION);
      $img = time().$name;
      $path='assets/profile/'.$img;

      move_uploaded_file($tmp_name,$path);
      $postdata['logo'] = $img;
    }
    $insert = $this->common->update(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$postdata,'pharmacy');
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Profile Updated Successfully";
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Profile Is Not Update";
    }

    echo json_encode($output);
    exit;
  }

    ////drug
      public function drug()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_results('drug',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config = array();
        $config["base_url"] = base_url() . 'doctor/druglist/';
        $config["total_rows"] = $this->common->count_all_results("drug",array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $result = $this->common->getbypaginationWhere('drug',array("userId"=>$this->session->userdata['doctorloggedin']['userId']),'drugId',$start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
        if(!empty($result))
        {
          foreach($result as $k=>$r)
          {
            $result[$k]->dose = $this->common->get('drugDose',array("drugId"=>$r->drugId));
          }
        }
        $data['result'] = $result;

        $this->load->view('pharmacy/header');
        $this->load->view('pharmacy/drug',$data);
        $this->load->view('pharmacy/footer');

      }

      public function drugDelete()
      {
        $query = $this->common->delete('drug',array("drugId"=>$_POST['id']));
        if($query)
        {
          $msg['success']="true";
          $msg['success_message'] ="Drug Deleted Successfully";
        }
        else
        {
          $msg['errors'] ="false";
          $msg['errors'] ="Drug  is Not Delete";

        }
        echo json_encode($msg);
        exit;
      }

      public function getdrugperpage()
      {
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }


      $config = array();
      if(!empty($_POST['searchtext']))
      {
        $data['pcount'] =   $data['pcount'] = $this->common->count_searchdrug(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
        $config["total_rows"] =   $data['pcount'] = $this->common->count_searchdrug(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
      }
      else
      {
      $data['pcount'] =   $data['pcount'] = $this->common->count_all_results('drug',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
      $config["total_rows"] =   $data['pcount'] = $this->common->count_all_results('drug',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
      }

      $config["base_url"] = base_url() . 'pharmacy/getdrugperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if(!empty($_POST['searchtext']))
      {
      $result = $this->common->searchdrug(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext'],$start,$config["per_page"]);
      }
      else
      {
        $result = $this->common->getbypaginationWhere('drug',array("userId"=>$this->session->userdata['doctorloggedin']['userId']),'drugId',$start,$config["per_page"]);
      }
      if(!empty($result))
      {
        foreach($result as $k=>$r)
        {
          $result[$k]->dose = $this->common->get('drugDose',array("drugId"=>$r->drugId));
        }
      }
      $data['result'] = $result;

      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }

      public function drug_add()
      {
        $this->securite();
        $this->load->view('pharmacy/header');
        $this->load->view('pharmacy/adddrug');
        $this->load->view('pharmacy/footer');
      }

      public function drugSave()
      {
        $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $_POST['userId'] = $this->session->userdata['doctorloggedin']['userId'];
        $_POST['date'] = $date;
        if(!empty($_POST['drugExpiryDate']))
        {
        $_POST['drugExpiryDate'] = date("Y-m-d", strtotime($_POST['drugExpiryDate']));
        }
        else
        {
          $_POST['drugExpiryDate'] = Null;
        }
        $dose = $_POST['drugDose'];
        unset($_POST['drugDose']);
        $insert = $this->common->insert('drug',$_POST);
        if($insert)
        {
          if(!empty($dose))
          {
            foreach($dose as $d)
            {
              if(!empty($d))
              {
              $a['drugId'] = $insert[1];
              $a['drugDose'] = $d;
              $inserted = $this->common->insert('drugDose',$a);
              }
            }
          }

        }
        if($insert)
        {
            $output['success'] ="true";
            $output['success_message'] ="Drug  Added Successfully";
            $output['url'] = base_url().'pharmacy1/druglist/'.$this->session->userdata['doctorloggedin']['url'];
            $output['delayTime'] ='3000';
            $output['resetform'] ='true';
        }
        else
        {
            $output['formErrors'] ="true";
            $output['errors'] ="Drug  is Not Added";
        }
          echo json_encode($output);
          exit;
      }


      public function drugEdit($id)
      {
        $this->securite();
        $data['result'] = $this->common->getrow('drug',array("drugId"=>$id));
        $data['dose']  = $this->common->get('drugDose',array("drugId"=>$id));
        if($data['result']->drugExpiryDate != '')
        {
        $data['result']->drugExpiryDate = date("d-m-Y", strtotime($data['result']->drugExpiryDate));
        }


        $this->load->view('pharmacy/header');
        $this->load->view('pharmacy/editdrug',$data);
        $this->load->view('pharmacy/footer');
      }

      public function drugUpdate($id)
      {
        if(!empty($_POST['drugExpiryDate']))
        {
        $_POST['drugExpiryDate'] = date("Y-m-d", strtotime($_POST['drugExpiryDate']));
        }
        else
        {
          $_POST['drugExpiryDate'] = Null;
        }

        if(!empty($_POST['drugDose']))
        {
        $dose = $_POST['drugDose'];
        unset($_POST['drugDose']);
       }
        $update = $this->common->update(array("drugId"=>$id),$_POST,'drug');
        if($update)
        {
          if(!empty($dose))
          {
            $this->common->delete('drugDose',array("drugId"=>$id));
            foreach($dose as $d)
            {
              if(!empty($d))
              {
              $a['drugId'] = $id;
              $a['drugDose'] = $d;
              $inserted = $this->common->insert('drugDose',$a);
              }
            }
          }

        }
        if($update)
        {
          $output['success'] ="true";
          $output['success_message'] ="Drug Updated Successfully";
          $output['url'] = base_url().'pharmacy1/druglist/'.$this->session->userdata['doctorloggedin']['url'];
          $output['delayTime'] ='3000';
        }
        else
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Drug Is Not Updated";
        }
        echo json_encode($output);
        exit;
      }

    ////drug

    ////blog
      public function prescription()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_results('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config = array();
        $config["base_url"] = base_url() . 'pharmacy/prescriptionlist/';
        $config["total_rows"] = $this->common->count_all_results("prescription",array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->allprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);


        $this->load->view('pharmacy/header');
        $this->load->view('pharmacy/prescription',$data);
        $this->load->view('pharmacy/footer');

      }

      public function getprescriptionperpage()
      {
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }
      if(!empty($_POST['date']))
      {
       $_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
      }

      $config = array();
      if(!empty($_POST['searchtext']))
      {
        $data['pcount'] =   $data['pcount'] = $this->common->count_searchprescription(array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
        $config["total_rows"] =   $data['pcount'] = $this->common->count_searchprescription(array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
      }
      else
      {
      $data['pcount'] =   $data['pcount'] = $this->common->count_all_results('prescription',array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
      $config["total_rows"] =   $data['pcount'] = $this->common->count_all_results('prescription',array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
      }

      $config["base_url"] = base_url() . 'pharmacy/getprescriptionperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if(!empty($_POST['searchtext']))
      {
      $data['result'] = $this->common->searchprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext'],$start,$config["per_page"]);
      }
      else
      {
        $data['result'] = $this->common->allprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
      }
      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }

      public function prescriptionView()
      {
        $result = $this->common->viewprescription(array("prescriptionId"=>$_POST['id']));
        $medicine = $this->common->getmedicine(array("prescriptionId"=>$_POST['id']));
        if($result)
        {
          $output['success'] ="true";
          $output['result'] = $result;
          $output['medicine'] = $medicine;
        }
        else
        {
          $output['success'] ="false";
        }
        echo json_encode($output);
        exit;
      }

      public function report()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_results('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config = array();
        $config["base_url"] = base_url() . 'pharmacy/reportlist/';
        $config["total_rows"] = $this->common->count_all_results("prescription",array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->allprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);


        $this->load->view('pharmacy/header');
        $this->load->view('pharmacy/report',$data);
        $this->load->view('pharmacy/footer');

      }

      public function getreportperpage()
      {
        $startdate ='';
        $enddate ='';
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }

      if(!empty($_POST['startdate']))
      {
       $startdate = date("Y-m-d", strtotime($_POST['startdate']));
      }

      if(!empty($_POST['enddate']))
      {
       $enddate = date("Y-m-d", strtotime($_POST['enddate']));
      }


      $config = array();
      if($startdate != '' && $enddate != '')
      {
        $data['pcount'] =   $data['pcount'] = $this->common->count_report(array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$startdate,$enddate);
        $config["total_rows"] =   $data['pcount'] = $this->common->count_report(array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$startdate,$enddate);
      }
      else
      {
      $data['pcount'] =   $data['pcount'] = $this->common->count_all_results('prescription',array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
      $config["total_rows"] =   $data['pcount'] = $this->common->count_all_results('prescription',array("pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));
      }

      $config["base_url"] = base_url() . 'pharmacy/getprescriptionperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if($startdate != '' && $enddate != '')
      {
      $data['result'] = $this->common->searchreport(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$startdate,$enddate,$start,$config["per_page"]);
      }
      else
      {
        $data['result'] = $this->common->allprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
      }
      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }



  //************************* forgotPassword********************************
    public function forgotPassword()
    {
      $this->load->view('pharmacy/forgot_password');
    }

        public function forgotcheckemail()
        {
         $mail ='';
         $res = $this->common->getrow('users',array("userEmail"=>$_POST['email']));



        if(!empty($res))
       {
            $doctor = $this->common->getrow('doctor',array("userId"=>$res->userId));
            $detail['password'] = $res->userPass;
            $detail['name'] = $doctor->name;
            // $res = $this->common->update(array("userId"=>$res->userId),array("userPassword"=>md5($random)),'users');
            $msg = $this->load->view('email/forgotPassword',$detail,true);
            $mail =  $this->mailsend('Reset Password',$_POST['email'],$msg);
           if($mail)
         {
           $output['success'] ="true";
           $output['success_message'] ="Password sent to your email. Please check your email. ";
           $output['resetform'] ='true';
           $output['url'] = base_url().'pharmacy';
           $output['delayTime'] ='3000';
      }
      else
      {
       $output['formErrors'] ="true";
        $output['errors'] ="Error in Email Sending.";
      }
      }
      else
      {
        $output['formErrors'] ="true";
        $output['errors'] ="Email does not exists.";
      }
      echo json_encode($output);
      exit;
    }

    public function prescriptionStatus()
    {
       $update = $this->common->update(array("prescriptionId"=>$_POST['prescriptionId']),array("status"=>$_POST['status']),'prescription');
       if($update)
       {
         if($_POST['status'] == 1)
         {
         $output['url'] = base_url().'pharmacy1/imagesend/'.$_POST['prescriptionId'];
         }
         $output['success'] ="true";
         $output['success_message'] ="Status Changed Successfully";
       }
       else
       {
         $output['formErrors'] ="true";
         $output['error'] ="Status Not Changed";
       }
       echo json_encode($output);
       exit;
    }

    public function prescriptionCost()
  {
    $last = $this->common->getrow('prescription',array("prescriptionId"=>$_POST['prescriptionId']));
    $t = $last->payableAmount + $_POST['deliveryCharges'];
    $update = $this->common->update(array("prescriptionId"=>$_POST['prescriptionId']),array("payableAmount"=>$t,"deliveryCharges"=>$_POST['deliveryCharges']),'prescription');
    if($update)
    {
      $output['success'] ="true";
      $output['success_message'] ="Cost Updated Successfully";
    }
    else
    {
      $output['formErrors'] ="true";
      $output['error'] ="Cost Is Not Updated";
    }
    echo json_encode($output);
    exit;

    }

  //************************* forgotPassword********************************
 public function testing()
 {
   $res = $this->common->getTable('prescription');
   if(!empty($res))
   {
     foreach($res as $r)
     {
       $this->convertimage($r->prescriptionId);
     }
   }
 }

 public function cron()
 {
    //  $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
    //  $date =  $nowUtc->format('Y-m-d H:i:s');
    //  $name= "cron";
    //  $this->common->insert('cron',array("name"=>$name,"date"=>$date));
     $res = $this->common->get('prescription',array("send"=>0,"userId"=>$this->session->userdata['doctorloggedin']['userId']));
     if(!empty($res))
     {
      $this->common->insert('cron',array("name"=>serialize($res),"date"=>$date));
       foreach($res as $r)
       {
         $this->convertimage1($r->prescriptionId);
       }
     }
    //  $fullpath[] ="http://borneogrub.com.my/eprescription/assets/admin/img/logo.png";
    //  $mail = $this->sendEmail('image','sbhupinder751@gmail.com','ssss',$fullpath);
 }

 public function cron1()
 {
     $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $name= "cron1";
        $this->common->insert('cron',array("name"=>$name,"date"=>$date));
 }

  public function convertimage($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->get('medicine',array("prescriptionId"=>$id)) ;
    $this->load->view('email/imageconvert1',$data);
  }

  public function convertimage1($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->get('medicine',array("prescriptionId"=>$id)) ;
    $this->load->view('email/imageconvert',$data);
  }

  public function imagesend($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->getmedicine(array("prescriptionId"=>$id)) ;
    $this->load->view('email/pharmacyimageconvert',$data);
  }


  // public function imageupload()
  // {
  //   define('UPLOAD_DIR', 'assets/generate/');
  //   $img = $_POST['imgBase64'];
  //   $id = $_POST['id'];
  //   $img = str_replace('data:image/png;base64,', '', $img);
  //   $img = str_replace(' ', '+', $img);
  //   $data = base64_decode($img);
  //   $file = UPLOAD_DIR . uniqid() . '.png';
  //   $success = file_put_contents($file, $data);
  //   $res = $this->common->update(array("prescriptionId"=>$id),array("image"=>$file),'prescription');
  //   print $success ? $file : 'Unable to save the file.';
  //
  //   }

    public function imageupload1()
    {
      define('UPLOAD_DIR', 'assets/generate/');
      $img = $_POST['imgBase64'];
      $id = $_POST['id'];
      $img = str_replace('data:image/png;base64,', '', $img);
      $img = str_replace(' ', '+', $img);
      $data = base64_decode($img);
      $file = UPLOAD_DIR . uniqid() . '.png';
      $success = file_put_contents($file, $data);
      $res = $this->common->update(array("prescriptionId"=>$id),array("image"=>$file),'prescription');
      // print $success ? $file : 'Unable to save the file.';
      $result = $this->common->viewprescription(array("prescriptionId"=>$id));
      if($result)
      {
          $data1['name'] = $result->patientName;
          $data1['result'] = $result;
          $data1['Id'] = $result->prescriptionId;
          $data1['result'] = $result;

          $message = $this->load->view('email/patient',$data1,true);
          $fullpath[]  = base_url().$result->image;
          $mail = $this->sendEmail('Medication List',$result->email,$message,$fullpath);
      }
        if($result)
        {
          $d = $this->common->getrow('users',array("userId"=>$result->userId));
          $data2['name'] = $result->doctor;
          $data2['result'] = $result;

          $message1 = $this->load->view('email/doctor',$data2,true);
         $fullpath1[]  = base_url().$result->image;

          $mail = $this->sendEmail('Medication List',$d->userEmail,$message1,$fullpath1);
          //$this->common->update(array("prescriptionId"=>$id),array("send"=>1),'prescription');
         }

         echo $file;
         exit;

      }



  public function sendEmail($sub,$to,$msg,$att) {
    $ci = & get_instance();
    $ci->email_var = array(
      'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
    );

    $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );
    $ci->email->initialize($ci->config_email);
    $ci->email->set_newline("\r\n");
    $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
       $ci->email->to($to);
    $ci->email->subject($sub);
    $ci->email->message($msg);
     foreach($att as $file_name)
   {
    $ci->email->attach($file_name);
    }

    if ($ci->email->send()) {
      $ci->email->clear(TRUE);
      return TRUE;
    } else {
      //echo $ci->email->print_debugger();
      return FALSE;
    }
}
  public function sendEmail1($to,$sub,$msg,$att)
{
  $ci = & get_instance();
  $ci->email_var = array(
    'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
  );

  $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );
  $ci->email->initialize($ci->config_email);
  $ci->email->set_newline("\r\n");
   $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
     $ci->email->to($to);
  $ci->email->subject($sub);
  $ci->email->message($msg);
  $ci->email->attach($att);

  if ($ci->email->send()) {
    $ci->email->clear(TRUE);
    return TRUE;
  } else {
    //echo $ci->email->print_debugger();
    return FALSE;
  }
}

public function mailsend($sub,$to,$msg)
{
  $ci = & get_instance();
  $ci->email_var = array(
    'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
  );

  $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );

  $ci->email->initialize($ci->config_email);
  $ci->email->set_newline("\r\n");
  $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
     $ci->email->to($to);
  $ci->email->subject($sub);
  $ci->email->message($msg);
  if ($ci->email->send()) {
    $ci->email->clear(TRUE);

    return TRUE;

  }
  else
  {
    //echo $ci->email->print_debugger();
    return FALSE;
  }
}


  // =============================

  public function Export()
	{
		$nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
		$date = $nowUtc->format('d-m-Y');


		// if(!empty($startdate))
		// {
		// $startdate = date("Y-m-d", strtotime($startdate));
		// $startdate1 = date("d_m_Y", strtotime($startdate));
		// }
		// else
		// {
		//  $startdate ='';
		// }
		// if(!empty($enddate))
		// {
		// $enddate = date("Y-m-d", strtotime($enddate));
		// $enddate1 = date("d_m_Y", strtotime($enddate));
		// }
		// else
		// {
		// $enddate = '';
		// }


			$fileName = 'Report-'.$date.'.xlsx';

		$this->load->library('excel');



		$results = $this->common->Exportprescription(array("p.pharmacyId"=>$this->session->userdata['doctorloggedin']['userId']));


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'S . No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Order No');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Doctor Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Patient Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Phone');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Patient Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Insurance Pay');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Total Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Mode of Order');

			// set Row
			$rowCount = 2;
			$i = 1;
			if(!empty($results))
			{
			foreach ($results as $element)
			{
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
      $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, date("d-m-Y", strtotime($element->date)));
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->orderNo);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->patientName);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->email);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->phone);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->payableAmount);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->copay);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->totalamount);
      if($element->delivery == 1)
      {
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount,"Delivery");
      }
      else
      {
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, "Self Pickup");
      }




			$rowCount++;
			$i++;

			}
		 }

			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$doc = $fileName;

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			ob_end_clean();
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.$doc.'"');
			$writer->save('php://output');
	}

  // pages
public function pages()
{
  $data['pcount'] = $this->common->count_all_results('pages',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
  $config = array();
  $config["base_url"] = base_url() . 'pharmacy/pageslist/';
  $config["total_rows"] = $this->common->count_all_results("pages",array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
  $config["per_page"] = 10;
  $config["uri_segment"] = 3;
  $this->pagination->initialize($config);
  $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
  if( $page <= 0 )
  {
    $page = 1;
  }
  $start = ($page-1) * $config["per_page"];
  $data['start'] = $start;
  $data['result'] = $this->common->getbypaginationWhere('pages',array("userId"=>$this->session->userdata['doctorloggedin']['userId']),'pagesId',$start,$config["per_page"]);
  $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);


  $this->load->view('pharmacy/header');
  $this->load->view('pharmacy/pages',$data);
  $this->load->view('pharmacy/footer');

}

public function pages_add()
{
  $this->load->view('pharmacy/header');
  $this->load->view('pharmacy/addpages');
  $this->load->view('pharmacy/footer');
}

public function pagesSave()
{
    $postdata = $_POST;
    $postdata['userid']= $this->session->userdata['doctorloggedin']['userId'];
    $insert = $this->common->insert('pages',$postdata);

    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Pages  Added Successfully";
      $output['url'] = base_url().'pharmacy/pageslist/'.$this->session->userdata['doctorloggedin']['url'];
      $output['delayTime'] ='3000';
      $output['resetform'] ='true';
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Pages  is Not Added";
    }

    echo json_encode($output);
    exit;

}

public function deletepages()
{
  $query = $this->common->delete('pages',array("pagesId"=>$_POST['id']));
  if($query)
  {
    $msg['success']="true";
    $msg['success_message'] ="Pages Deleted Successfully";
  }
  else
  {
    $msg['errors'] ="false";
    $msg['errors'] ="Pages  is Not Delete";

  }
  echo json_encode($msg);
  exit;
}




public function pages_edit($id)
{
  $data['result'] = $this->common->getrow('pages',array("pagesId"=>$id));
  $this->load->view('pharmacy/header');
  $this->load->view('pharmacy/editpages',$data);
  $this->load->view('pharmacy/footer');
}

public function pagesUpdate($id)
{
  $postdata = $_POST;

  $update = $this->common->update(array("pagesId"=>$id),$postdata,'pages');

  if($update)
  {
    $output['success'] ="true";
    $output['success_message'] ="Pages Updated Successfully";
    $output['url'] = base_url().'pharmacy1/pageslist/'.$this->session->userdata['doctorloggedin']['url'];
    $output['delayTime'] ='3000';
  }
  else
  {
    $output['formErrors'] ="true";
    $output['errors'] ="Pages Is Not Updated";
  }
  echo json_encode($output);
  exit;
}

public function pagesStatus()
{
  $postdata = $_POST;

  $update = $this->common->update(array("pagesId"=>$_POST['pagesId']),array("pagesStatus"=>$_POST['pagesStatus']),'pages');
  if($update)
  {
    $output['success'] ="true";
    $output['success_message'] ="Pages Status Changed Successfully";
  }
  else
  {
    $output['formErrors'] ="true";
    $output['errors'] ="Pages Status Is Not Changed";
  }
  echo json_encode($output);
  exit;
}

public function getpagesperpage()
{
    if(!empty($_POST['perpage']))
   {
    $perpage =$_POST['perpage'];
   }
   else
   {
    $perpage =10;
   }

   $config = array();
   $data['pcount'] = $this->common->count_all_results('pages',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
   $config["total_rows"] = $this->common->count_all_results('pages',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));


   $config["base_url"] = base_url() . 'pharmacy/getpagesperpage/';
   $config["per_page"] = $perpage;
   $config["uri_segment"] = 3;
   $this->pagination->initialize($config);
   $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
   if( $page <= 0 )
   {
    $page = 1;
   }
   $start = ($page-1) * $config["per_page"];
   $data['start'] = $start;

    $data['result'] = $this->common->getbypaginationWhere('pages',array("userId"=>$this->session->userdata['doctorloggedin']['userId']).'pagesId',$start,$config["per_page"]);

   $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
   $data['start'] = $start;
   if($data)
   {
    $output['success'] = "true";
    $output['result'] = $data;
   }
   echo json_encode($output);
   exit;
}

// pages

public function content($id)
{
  $a = explode("-",$id);
  $c = count($a);
  $ids = $a[$c-1];
  $data['content'] = $this->common->getrow('pages',array("pagesId"=>$ids));
  $data['pharmacy'] = $this->common->getrow('pharmacy',array("userId"=>$data['content']->userId));
   $this->load->view('pharmacy/content',$data);
}

public function merchant()
{
  $data['result'] = $this->common->getrow('merchant',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
  $this->load->view('pharmacy/header');
  $this->load->view('pharmacy/merchant',$data);
  $this->load->view('pharmacy/footer');
}

 public function merchantUpdate()
 {
  $res = $this->common->getrow('merchant',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
  if(!empty($res))
  {
  $insert = $this->common->update(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),array("secret"=>$_POST['secret'],"merchant"=>$_POST['merchant']),'merchant');
  }
  else
  {
    $insert = $this->common->insert('merchant',array("merchant"=>$_POST['merchant'],"secret"=>$_POST['secret'],"userId"=>$this->session->userdata['doctorloggedin']['userId']));
  }
  if($insert)
  {
    $output['success'] ="true";
    $output['success_message'] ="Merchant Updated Successfully";
  }
  else
  {
    $output['formErrors'] ="true";
    $output['errors'] ="Merchant Is Not Update";
  }

  echo json_encode($output);
  exit;
 }




}
