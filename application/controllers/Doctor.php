<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

  public function __construct() {
    parent::__construct();


    $this->load->model('common');
    $this->load->library("pagination");
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->helper(array('form', 'url'));

  }


  public function index()
  {
    if(!empty($_POST))
    {
      $data = array(
        'userEmail' =>$_POST['email'],
        'userPassword' => md5($_POST['password']),
        'userType' =>1,
      );
      $result = $this->common->getrow('users',$data);

      if ($result == TRUE)
      {




        if($result->userType == 1 )
        {
            $session_data = array(
          'userId' => $result->userId,
          'email' => $result->userEmail,
          'type' =>$result->userType,
        );
         $this->session->set_userdata('doctorloggedin', $session_data);
        $output['url'] = base_url().'doctor/dashboard';
        }
        else if($result->userType == 2)
        {
            $name = $this->common->getrow('pharmacy',array("userId"=>$result->userId));
         $url = str_replace(' ','-',strtolower($name->name));
         $session_data = array(
          'userId' => $result->userId,
          'email' => $result->userEmail,
          'type' =>$result->userType,
          'url' =>$url,

        );
         $this->session->set_userdata('doctorloggedin', $session_data);

          $output['url'] = base_url().'pharmacy/dashboard/'.$url;
        }

        $output['success'] ="true";
        $output['success_message'] ="Login Successfully";
        $output['delayTime'] ='3000';

      }
      else
      {
        $output['formErrors'] = "true";
        $output['errors'] = 'Invalid Username or Password.';
      }

      echo json_encode($output);
      exit;
    }
    else
    {
      $this->load->view('doctor/login');
    }
  }

  public function securite()
  {
    if(!isset($this->session->userdata['doctorloggedin']['userId']) && $this->session->userdata['doctorloggedin']['userId'] != 1)
    {
      redirect('doctor');
    }
  }


  public function logout()
  {
    $this->session->sess_destroy();
    redirect('doctor');
  }

  public function dashboard()
  {
    $this->securite();
    $this->load->view('doctor/header');
    $this->load->view('doctor/dashboard');
    $this->load->view('doctor/footer');

  }



  public function password()
  {
    $this->securite();
    $this->load->view('doctor/header');
    $this->load->view('doctor/password');
    $this->load->view('doctor/footer');

  }

  public function passwordUpdate()
  {
    $insert = $this->common->update(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),array("userPassword"=>md5($_POST['pass']),"userPass"=> $_POST['pass']),'users');
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Password Changed Successfully";
      $output['delayTime'] ='3000';
      $output['resetform'] ='true';
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Password Is Not Change";
    }

    echo json_encode($output);
    exit;
  }



  public function profile()
  {
    $this->securite();
    $data['result'] = $this->common->getprofile(array("u.userId"=>$this->session->userdata['doctorloggedin']['userId']));
    $this->load->view('doctor/header');
    $this->load->view('doctor/profile',$data);
    $this->load->view('doctor/footer');
  }

  public function profileUpdate()
  {
    $postdata = $_POST;
    if(!empty($_FILES["image"]["name"]))
    {
      $name = str_replace(" ","_",$_FILES['image']['name']);
      $tmp_name = str_replace(" ","_",$_FILES['image']['tmp_name']);
      $error=$_FILES['image']['error'];
      $file_ext = pathinfo($name, PATHINFO_EXTENSION);
      $img = time().$name;
      $path='assets/profile/'.$img;

      move_uploaded_file($tmp_name,$path);
      $postdata['image'] = $img;
    }
    if(!empty($_FILES["signature"]["name"]))
    {
      $name = str_replace(" ","_",$_FILES['signature']['name']);
      $tmp_name = str_replace(" ","_",$_FILES['signature']['tmp_name']);
      $error=$_FILES['signature']['error'];
      $file_ext = pathinfo($name, PATHINFO_EXTENSION);
      $img = time().$name;
      $path='assets/signature/'.$img;

      move_uploaded_file($tmp_name,$path);
      $postdata['signature'] = $img;
    }
     unset($postdata['email']);
    $insert = $this->common->update(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$postdata,'doctor');
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Profile Updated Successfully";
      $output['url'] = base_url().'doctor/profile';
      $output['delayTime'] ='3000';

    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Profile Is Not Update";
    }

    echo json_encode($output);
    exit;
  }

    ////services

    ////blog
      public function prescription()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_results('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config = array();
        $config["base_url"] = base_url() . 'doctor/prescriptionlist/';
        $config["total_rows"] = $this->common->count_all_results("prescription",array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->allprescription(array("p.userId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
        // $last = $this->common->getRecentOne('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']),'prescriptionId','desc');
        // if(!empty($last))
        // {
        //  $a =  $this->convertimage($last->prescriptionId);
        //  $a = '';
        // }

        $this->load->view('doctor/header');
        $this->load->view('doctor/prescription',$data);
        $this->load->view('doctor/footer');

      }

      public function prescription_add()
      {
        $this->securite();

        $data['pharmacy'] = $this->common->getTable('pharmacy');
        $this->load->view('doctor/header');
        $this->load->view('doctor/addprescription',$data);
        $this->load->view('doctor/footer');
      }

      public function prescriptionSave()
      {
        $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $_POST['date'] = $date;
        $_POST['userId'] = $this->session->userdata['doctorloggedin']['userId'];
        $drug = $_POST['drug'];
        $drugId = $_POST['drugId'];
        $dosage = [];
        if(!empty($_POST['dosage']))
        {
        $dosage = $_POST['dosage'];
        }
        $frequency = $_POST['frequency'];
        $price = $_POST['price'];
        $subtotal = $_POST['subtotal'];
        $instruction = $_POST['instruction'];
        $no = $_POST['no'];
        $qty = $_POST['qty'];
        $duration = $_POST['duration'];
        $strength = $_POST['strength'];

        // $_POST['orderNo'] = strtotime($date) . rand(0,99999);
        $_POST['orderNo'] = rand(10,100000);
        unset($_POST['drug']);
        unset($_POST['dosage']);
        unset($_POST['strength']);
        unset($_POST['frequency']);
        unset($_POST['duration']);
        unset($_POST['instruction']);
        unset($_POST['drugId']);
        unset($_POST['price']);
        unset($_POST['subtotal']);
        unset($_POST['qty']);
        unset($_POST['no']);
        unset($_POST['duration']);
        if(!empty($_POST['copay']))
        {
          $_POST['payableAmount'] = $_POST['totalamount'] - $_POST['copay'];
        }
        else
        {
          $_POST['payableAmount'] = $_POST['totalamount'];
        }

        $insert = $this->common->insert('prescription',$_POST);
        if($insert)
        {
          if(!empty($drug))
          {
            foreach($drug as $k=>$d)
            {
              if(!empty($drug[$k]))
              {
              $a['prescriptionId'] = $insert[1];
              $a['drugId'] = $drugId[$k];
              if(!empty($dosage))
              {
                if(!empty($dosage[$k]))
                {
                $a['drugDoseId'] = $dosage[$k];
                }
              }
              $a['frequency'] = $frequency[$k];
              $a['price'] = $price[$k];
              $a['subtotal'] = $subtotal[$k];
              $a['instruction'] = $instruction[$k];
              $a['no'] = $no[$k];
              $a['quantity'] = $qty[$k];
              $a['duration'] = $duration[$k];
              $a['strength'] = $strength[$k];
              $m = $this->common->insert('medicine',$a);
              }
            }
          }
        }

        // if($insert)
        // {
        //   $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$insert[1])) ;
        //   $data['medicines'] = $this->common->get('medicine',array("prescriptionId"=>$insert[1])) ;
        //   $pharmacy = $this->common->getrow('pharmacy',array("pharmacyId"=>$_POST['pharmacyId']));
        //   $data['name'] = $pharmacy->name;
        //   $message = $this->load->view('email/pharmacy',$data,true);
 		    //  $mail = $this->mailsend('Prescription',$pharmacy->email,$message);
        // }

        // if($insert)
        // {
        //     $this->convertimage1($insert[1]);
        // }


        // if($insert)
        // {
        //   $data['name'] = $_POST['patientName'];
        //   $message1 = $this->load->view('email/patient',$data,true);
        //   $mail = $this->mailsend('Prescription',$_POST['email'],$message1);
        // }
        //  if($insert)
        // {
        //   $data['name'] = "Admin";
        //   $message1 = $this->load->view('email/patient',$data,true);
        //   $mail = $this->mailsend('Prescription',$this->session->userdata['doctorloggedin']['email'],$message1);
        // }

        // if($insert)
        // {
        // }

          if($insert)
          {
            $output['success'] ="true";
            $output['success_message'] ="Prescription  Added Successfully";
            $output['url'] = base_url().'doctor/convertimage/'.$insert[1];
            $output['delayTime'] ='3000';
            $output['resetform'] ='true';
          }
          else
          {
            $output['formErrors'] ="true";
            $output['errors'] ="Prescription  is Not Added";
          }

          echo json_encode($output);
          exit;

      }

      public function prescriptionDelete()
      {
        $query = $this->common->delete('prescription',array("prescriptionId"=>$_POST['id']));
        if($query)
        {
          $msg['success']="true";
          $msg['success_message'] ="Prescription Deleted Successfully";
        }
        else
        {
          $msg['errors'] ="false";
          $msg['errors'] ="Prescription  is Not Delete";

        }
        echo json_encode($msg);
        exit;
      }



      public function prescription_edit($id)
      {
        $this->securite();
        $data['result'] = $this->common->getrow('prescription',array("prescriptionId"=>$id));
        $data['pharmacy'] = $this->common->getTable('pharmacy');
        $medicine = $this->common->getmedicine(array("prescriptionId"=>$id));
        if(!empty($medicine))
        {
          foreach($medicine as $key=>$m)
          {
            $medicine[$key]->alldoses =$this->common->get('drugDose',array("drugId"=>$m->drugId));
          }
        }

        $data['medicine'] = $medicine;
        $this->load->view('doctor/header');
        $this->load->view('doctor/editprescription',$data);
        $this->load->view('doctor/footer');
      }

      public function prescription_clone($id)
      {
        $this->securite();
        $result = $this->common->getrow('prescription',array("prescriptionId"=>$id));
        $data['pharmacy'] = $this->common->getTable('pharmacy');
        $result->copay = '';
        $result->payableAmount = $result->totalamount;
        $data['result'] = $result;
        $medicine = $this->common->getmedicine(array("prescriptionId"=>$id));
        if(!empty($medicine))
        {
          foreach($medicine as $key=>$m)
          {
            $medicine[$key]->alldoses =$this->common->get('drugDose',array("drugId"=>$m->drugId));
          }
        }
          $data['medicine'] = $medicine;
        $this->load->view('doctor/header');
        $this->load->view('doctor/cloneprescription',$data);
        $this->load->view('doctor/footer');
      }

      public function prescriptionUpdate($id)
      {
        $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $_POST['date'] = $date;
        $_POST['userId'] = $this->session->userdata['doctorloggedin']['userId'];
        $drug = $_POST['drug'];
        $drugId = $_POST['drugId'];
        $dosage = [];
        if(!empty($_POST['dosage']))
        {
        $dosage = $_POST['dosage'];
        }
        $frequency = $_POST['frequency'];
        $price = $_POST['price'];
        $subtotal = $_POST['subtotal'];
        $instruction = $_POST['instruction'];
        $qty = $_POST['qty'];
        $no = $_POST['no'];
        $duration = $_POST['duration'];
        $strength = $_POST['strength'];

       unset($_POST['qty']);
       unset($_POST['no']);
       unset($_POST['duration']);
       unset($_POST['strength']);
        unset($_POST['drug']);
        unset($_POST['dosage']);
        unset($_POST['frequency']);
        unset($_POST['duration']);
        unset($_POST['instruction']);
        unset($_POST['drugId']);
        unset($_POST['price']);
        unset($_POST['subtotal']);
        if(!empty($_POST['copay']))
        {
          $_POST['payableAmount'] = $_POST['totalamount'] - $_POST['copay'];
        }
        else
        {
          $_POST['payableAmount'] = $_POST['totalamount'];
        }


        $update = $this->common->update(array("prescriptionId"=>$id),$_POST,'prescription');
        if($update)
       {
         if(!empty($drug))
         {
           $this->common->delete('medicine',array("prescriptionId"=>$id));
           foreach($drug as $k=>$d)
           {
             if(!empty($drug[$k]))
             {
               $a['prescriptionId'] = $id;
               $a['drugId'] = $drugId[$k];
               if(!empty($dosage))
               {
                 if(!empty($dosage[$k]))
                 {
                 $a['drugDoseId'] = $dosage[$k];
                 }
               }
               $a['frequency'] = $frequency[$k];
               $a['price'] = $price[$k];
               $a['subtotal'] = $subtotal[$k];
               $a['instruction'] = $instruction[$k];
               $a['no'] = $no[$k];
               $a['quantity'] = $qty[$k];
               $a['duration'] = $duration[$k];
               $a['strength'] = $strength[$k];

             $m = $this->common->insert('medicine',$a);
             }
           }
         }
       }
        if($update)
        {
          $output['success'] ="true";
          $output['success_message'] ="Prescription Updated Successfully";
          $output['url'] = base_url().'doctor/prescriptionlist';
          $output['delayTime'] ='3000';
        }
        else
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Prescription Is Not Updated";
        }
        echo json_encode($output);
        exit;
      }


      public function getprescriptionperpage()
      {
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }
      if(!empty($_POST['date']))
      {
       $_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
      }

      $config = array();
      if(!empty($_POST['searchtext']))
      {
        $data['pcount'] =   $data['pcount'] = $this->common->count_searchprescription(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
        $config["total_rows"] =   $data['pcount'] = $this->common->count_searchprescription(array("userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext']);
      }
      else
      {
      $data['pcount'] =   $data['pcount'] = $this->common->count_all_results('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
      $config["total_rows"] =   $data['pcount'] = $this->common->count_all_results('prescription',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
      }

      $config["base_url"] = base_url() . 'doctor/getprescriptionperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if(!empty($_POST['searchtext']))
      {
      $data['result'] = $this->common->searchprescription(array("p.userId"=>$this->session->userdata['doctorloggedin']['userId']),$_POST['searchtext'],$start,$config["per_page"]);
      }
      else
      {
        $data['result'] = $this->common->allprescription(array("p.userId"=>$this->session->userdata['doctorloggedin']['userId']),$start,$config["per_page"]);
      }
      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }

      public function prescriptionView()
      {
        $result = $this->common->viewprescription(array("prescriptionId"=>$_POST['id']));
        $medicine = $this->common->getmedicine(array("prescriptionId"=>$_POST['id']));
        if($result)
        {
          $output['success'] ="true";
          $output['result'] = $result;
          $output['medicine'] = $medicine;
        }
        else
        {
          $output['success'] ="false";
        }
        echo json_encode($output);
        exit;
      }



  //************************* forgotPassword********************************
    public function forgotPassword()
    {
      $this->load->view('doctor/forgot_password');
    }

        public function forgotcheckemail()
        {
         $mail ='';
         $res = $this->common->getrow('users',array("userEmail"=>$_POST['email']));



        if(!empty($res))
       {

            // $length           = 8;
            // $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // $charactersLength = strlen($characters);
            // $random           = rand();
            // for ($i = 0; $i < $length; $i++)
            // {
            // $random .= $characters[rand(0, $charactersLength - 1)];
            // }
            $doctor = $this->common->getrow('doctor',array("userId"=>$res->userId));
            $detail['password'] = $res->userPass;
            $detail['name'] = $doctor->name;
            // $res = $this->common->update(array("userId"=>$res->userId),array("userPassword"=>md5($random)),'users');
            $msg = $this->load->view('email/forgotPassword',$detail,true);
            $mail =  $this->mailsend('Reset Password',$_POST['email'],$msg);
           if($mail)
         {
           $output['success'] ="true";
           $output['success_message'] ="Password sent to your email. Please check your email. ";
           $output['resetform'] ='true';
           $output['url'] = base_url().'doctor';
           $output['delayTime'] ='3000';
      }
      else
      {
       $output['formErrors'] ="true";
        $output['errors'] ="Error in Email Sending.";
      }
      }
      else
      {
        $output['formErrors'] ="true";
        $output['errors'] ="Email does not exists.";
      }
      echo json_encode($output);
    }
  //************************* forgotPassword********************************
 public function testing()
 {
   $res = $this->common->getTable('prescription');
   if(!empty($res))
   {
     foreach($res as $r)
     {
       $this->convertimage($r->prescriptionId);
     }
   }
 }



  public function convertimage($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->getmedicine(array("prescriptionId"=>$id));
    $this->load->view('email/imageconvert1',$data);
  }

  public function convertimage1($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->getmedicine(array("prescriptionId"=>$id));
    $this->load->view('email/imageconvert',$data);
  }

  public function imageupload()
  {
    define('UPLOAD_DIR', 'assets/generate/');
    $img = $_POST['imgBase64'];
    $id = $_POST['id'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = UPLOAD_DIR . uniqid() . '.png';
    $success = file_put_contents($file, $data);
    $res = $this->common->update(array("prescriptionId"=>$id),array("image"=>$file),'prescription');
    print $success ? $file : 'Unable to save the file.';

    }

    public function imageupload1()
    {
      define('UPLOAD_DIR', 'assets/generate/');
      $img = $_POST['imgBase64'];
      $id = $_POST['id'];
      $img = str_replace('data:image/png;base64,', '', $img);
      $img = str_replace(' ', '+', $img);
      $data = base64_decode($img);
      $file = UPLOAD_DIR . uniqid() . '.png';
      $success = file_put_contents($file, $data);
      $res = $this->common->update(array("prescriptionId"=>$id),array("image"=>$file),'prescription');
      // print $success ? $file : 'Unable to save the file.';
      $result = $this->common->viewprescription(array("prescriptionId"=>$id));
      // if($result)
      // {
      //     $data1['name'] = $result->patientName;
      //     $data1['result'] = $result;
      //
      //     $message = $this->load->view('email/patient',$data1,true);
      //     $fullpath[]  = base_url().$result->image;
      //     $mail = $this->sendEmail('Medication List',$result->email,$message,$fullpath);
      // }
        // if($result)
        // {
        //   $d = $this->common->getrow('users',array("userId"=>$result->userId));
        //   $data2['name'] = $result->doctor;
        //   $data2['result'] = $result;
        //
        //   $message1 = $this->load->view('email/doctor',$data2,true);
        //  $fullpath1[]  = base_url().$result->image;
        //
        //   $mail = $this->sendEmail('Medication List',$d->userEmail,$message1,$fullpath1);
        //  }
         if($result)
         {
           $data5['name'] = $result->pharmacy;
           $data5['result'] = $result;
           $message3 = $this->load->view('email/pharmacy',$data5,true);
           $fullpath5[]  = base_url().$result->image;

           $mail = $this->sendEmail('Medication List',$result->pemail,$message3,$fullpath5);
         }
         echo $file;
         exit;

      }


   public function getmedicine()
   {
     if(!empty($_POST))
     {
     $res = $this->common->getmedicineLike(array("userId"=>$_POST['userId']),$_POST['text']);

     if ($res)
     {
       $output['success'] ="true";
       $output['result'] =$res;
     }
     else
     {
       $output['success']="false";
       $output['result']=[];
     }
    }
     else
     {
       $output['success']="false";
       $output['result']=[];
     }
     echo json_encode($output);
     exit;
   }

   public function getdose()
   {
     if(!empty($_POST))
     {
      $res = $this->common->get('drugDose',array("drugId"=>$_POST['id']));

     if ($res)
     {
       $output['success'] ="true";
       $output['result'] =$res;
     }
     else
     {
       $output['success']="false";
       $output['result']=[];
     }
    }
     else
     {
       $output['success']="false";
       $output['result']=[];
     }
     echo json_encode($output);
     exit;
   }


  public function sendEmail($sub,$to,$msg,$att) {
    $ci = & get_instance();
    $ci->email_var = array(
      'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
    );

    $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );
    $ci->email->initialize($ci->config_email);
    $ci->email->set_newline("\r\n");
    $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
       $ci->email->to($to);
    $ci->email->subject($sub);
    $ci->email->message($msg);
     foreach($att as $file_name)
   {
    $ci->email->attach($file_name);
    }

    if ($ci->email->send()) {
      $ci->email->clear(TRUE);
      return TRUE;
    } else {
      //echo $ci->email->print_debugger();
      return FALSE;
    }
}
  public function sendEmail1($to,$sub,$msg,$att)
{
  $ci = & get_instance();
  $ci->email_var = array(
    'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
  );

  $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );
  $ci->email->initialize($ci->config_email);
  $ci->email->set_newline("\r\n");
   $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
     $ci->email->to($to);
  $ci->email->subject($sub);
  $ci->email->message($msg);
  $ci->email->attach($att);

  if ($ci->email->send()) {
    $ci->email->clear(TRUE);
    return TRUE;
  } else {
    //echo $ci->email->print_debugger();
    return FALSE;
  }
}

public function mailsend($sub,$to,$msg)
{
  $ci = & get_instance();
  $ci->email_var = array(
    'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
  );

  $ci->config_email = Array (
     'protocol' => "smpt",
     'smtp_host' => "business17.web-hosting.com",
     'smtp_port' => '587',
     'smtp_user' => 'admin@doctortouchrx.com',
    'smtp_pass' => '@Admin1978',
    'mailtype' => "html",
    'wordwrap' => TRUE,
    'crlf' => '\r\n',
    'charset' => "utf-8"
  );

  $ci->email->initialize($ci->config_email);
  $ci->email->set_newline("\r\n");
  $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
     $ci->email->to($to);
  $ci->email->subject($sub);
  $ci->email->message($msg);
  if ($ci->email->send()) {
    $ci->email->clear(TRUE);

    return TRUE;

  }
  else
  {
    //echo $ci->email->print_debugger();
    return FALSE;
  }
}


  // =============================





}
