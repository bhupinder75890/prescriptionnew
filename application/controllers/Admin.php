<?php
//session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct() {
    parent::__construct();


    $this->load->model('common');
    $this->load->library("pagination");
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('upload');
    $this->load->helper(array('form', 'url'));

  }


  public function index()
  {
    if(!empty($_POST))
    {
      $data = array(
        'email' =>$_POST['email'],
        'password' => md5($_POST['password']),
      );
      $result = $this->common->getrow('admin',$data);

      if ($result == TRUE)
      {

        $session_data = array(
          'id' => $result->id,
          'email' => $result->email,
          'name' =>$result->name,
          'roleId' =>$result->roleId,
        );

        $this->session->set_userdata('adminloggedin', $session_data);


        $output['url'] = base_url().'admin/dashboard';

        $output['success'] ="true";
        $output['success_message'] ="Login Successfully";
        $output['delayTime'] ='3000';

      }
      else
      {
        $output['formErrors'] = "true";
        $output['errors'] = 'Invalid Username or Password.';
      }

      echo json_encode($output);
      exit;
    }
    else
    {

      $this->load->view('admin/login');

    }
  }

  public function securite()
  {
    if(!isset($this->session->userdata['adminloggedin']['id']))
    {
      redirect('admin');
    }
  }


  public function logout()
  {
    $this->session->sess_destroy();
    redirect('admin');
  }

  public function dashboard()
  {
    $this->securite();
    $this->load->view('admin/header');
    $this->load->view('admin/dashboard');
    $this->load->view('admin/footer');

  }



  public function password()
  {
    $this->securite();
    $this->load->view('admin/header');
    $this->load->view('admin/password');
    $this->load->view('admin/footer');

  }

  public function passwordUpdate()
  {
    $insert = $this->common->update(array("id"=>$this->session->userdata['adminloggedin']['id']),array("password"=>md5($_POST['pass']),"pass"=>$_POST['pass']),'admin');
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Password Changed Successfully";
      $output['delayTime'] ='3000';
      $output['resetform'] ='true';
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Password Is Not Change";
    }

    echo json_encode($output);
    exit;
  }



  public function setting()
  {
    $this->securite();
    $data['result'] = $this->common->getone('setting');
    $this->load->view('admin/header');
    $this->load->view('admin/setting',$data);
    $this->load->view('admin/footer');
  }

  public function settingupdate()
  {
    $postdata = $_POST;
    if(!empty($_FILES["file"]["name"]))
    {
      $name = str_replace(" ","_",$_FILES['file']['name']);
      $tmp_name = str_replace(" ","_",$_FILES['file']['tmp_name']);
      $error=$_FILES['file']['error'];
      $file_ext = pathinfo($name, PATHINFO_EXTENSION);
      $img = time().$name;
      $path='assets/image/'.$img;

      move_uploaded_file($tmp_name,$path);
      $postdata['image'] = $img;
    }

    $data = $this->common->getone('setting');
    if(!empty($data))
    {
      $insert = $this->common->update(array("id"=>$data->id),$postdata,'setting');

    }
    else
    {
      $insert = $this->common->insert('setting',$postdata);

    }
    if($insert)
    {
      $output['success'] ="true";
      $output['success_message'] ="Setting Updated Successfully";
    }
    else
    {
      $output['formErrors'] ="true";
      $output['errors'] ="Setting Is Not Update";
    }

    echo json_encode($output);
    exit;
  }

    ////services

    ////blog
      public function doctor()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_table('doctor');
        $config = array();
        $config["base_url"] = base_url() . 'admin/doctorlist/';
        $config["total_rows"] = $this->common->count_all_table("doctor");
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->alldoctor($start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);


        $this->load->view('admin/header');
        $this->load->view('admin/doctor',$data);
        $this->load->view('admin/footer');

      }

      public function doctor_add()
      {
        $this->securite();
        $this->load->view('admin/header');
        $this->load->view('admin/adddoctor');
        $this->load->view('admin/footer');
      }

      public function doctorSave()
      {
        $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $length           = 8;
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $random           = '';
        for ($i = 0; $i < $length; $i++)
        {
        $random .= $characters[rand(0, $charactersLength - 1)];
        }
        $email = $_POST['userEmail'];
        unset($_POST['userEmail']);
        $check = $this->common->getrow('users',array("userEmail"=>$email));
        if(!empty($check))
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Email Address  Already Exist.";
        }
       else
       {
          $insert = $this->common->insert('users',array("userEmail"=>$email,"userPassword"=>md5($random),"userPass"=>$random,"userType"=>1));
          if($insert)
          {
            $_POST['userId'] = $insert[1];
            $doc = $this->common->insert('doctor',$_POST);
          }
          if($doc)
          {
            $detail['name'] = $_POST['name'];
 			      $detail['pass'] = $random;

 			      $detail['email'] = $email;
            $message = $this->load->view('email/userdetail',$detail,true);
 		        $mail = $this->mailsend('User login Detail',$email,$message);
          }

          if($doc)
          {
            $output['success'] ="true";
            $output['success_message'] ="Doctor  Added Successfully";
            $output['url'] = base_url().'admin/doctorlist';
            $output['delayTime'] ='3000';
            $output['resetform'] ='true';
          }
          else
          {
            $output['formErrors'] ="true";
            $output['errors'] ="Doctor  is Not Added";
          }
        }

          echo json_encode($output);
          exit;

      }

      public function doctorDelete()
      {
        $query = $this->common->delete('doctor',array("userId"=>$_POST['id']));
        if($query)
        {
          $query1 = $this->common->delete('users',array("userId"=>$_POST['id']));
        }
        if($query1)
        {
          $msg['success']="true";
          $msg['success_message'] ="Doctor Deleted Successfully";
        }
        else
        {
          $msg['errors'] ="false";
          $msg['errors'] ="Doctor  is Not Delete";
        }
        echo json_encode($msg);
        exit;
      }



      public function doctor_edit($id)
      {
        $this->securite();
        $data['result'] = $this->common->getprofile(array("u.userId"=>$id));
        $this->load->view('admin/header');
        $this->load->view('admin/editdoctor',$data);
        $this->load->view('admin/footer');
      }

      public function doctorUpdate($id)
      {
        $email = $_POST['userEmail'];
        unset($_POST['userEmail']);
        $update = $this->common->update(array("userId"=>$id),array("userEmail"=>$email),'users');
        if($update)
        {
          $doctor = $this->common->update(array("userId"=>$id),$_POST,'doctor');
        }
        if($doctor)
        {
          $output['success'] ="true";
          $output['success_message'] ="Doctor Updated Successfully";
          $output['url'] = base_url().'admin/doctorlist';
          $output['delayTime'] ='3000';
        }
        else
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Doctor Is Not Updated";
        }
        echo json_encode($output);
        exit;
      }

      public function getdoctorperpage()
      {
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }
      if(!empty($_POST['date']))
      {
       $_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
      }

      $config = array();
      if(!empty($_POST['searchtext']))
      {
        $data['pcount'] = $this->common->count_searchdoctor($_POST['searchtext']);
        $config["total_rows"] = $this->common->count_searchdoctor($_POST['searchtext']);
      }
      else
      {
      $data['pcount'] = $this->common->count_all_table('doctor');
      $config["total_rows"] = $this->common->count_all_table('doctor');
      }

      $config["base_url"] = base_url() . 'admin/getdoctorperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if(!empty($_POST['searchtext']))
      {
      $data['result'] = $this->common->searchdoctor($_POST['searchtext'],$start,$config["per_page"]);
      }
      else
      {
        $data['result'] = $this->common->alldoctor($start,$config["per_page"]);
      }
      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }

      // doctor section

      // pharmacy section

      public function pharmacy()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_table('pharmacy');
        $config = array();
        $config["base_url"] = base_url() . 'admin/pharmacylist/';
        $config["total_rows"] = $this->common->count_all_table("pharmacy");
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->getpharmacy($start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);

        $this->load->view('admin/header');
        $this->load->view('admin/pharmacy',$data);
        $this->load->view('admin/footer');

      }

      public function pharmacy_add()
      {
        $this->securite();
        $this->load->view('admin/header');
        $this->load->view('admin/addpharmacy');
        $this->load->view('admin/footer');
      }

      public function pharmacySave()
      {
        $nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
        $date =  $nowUtc->format('Y-m-d H:i:s');
        $length           = 8;
        $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $random           = '';
        for ($i = 0; $i < $length; $i++)
        {
        $random .= $characters[rand(0, $charactersLength - 1)];
        }
        $email = $_POST['email'];
        unset($_POST['email']);
        $check = $this->common->getrow('users',array("userEmail"=>$email));
        if(!empty($check))
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Email Address  Already Exist.";
        }
       else
       {
          $insert = $this->common->insert('users',array("userEmail"=>$email,"userPassword"=>md5($random),"userPass"=>$random,"userType"=>2));
          if($insert)
          {
            $_POST['userId'] = $insert[1];
            $doc = $this->common->insert('pharmacy',$_POST);
          }
          if($doc)
          {
            $detail['name'] = $_POST['name'];
            $detail['userId'] = $insert[1];
 			      $detail['pass'] = $random;

 			      $detail['email'] = $email;
            $message = $this->load->view('email/userpharmacy',$detail,true);
 		        $mail = $this->mailsend('User login Detail',$email,$message);
          }

          if($doc)
          {
            $output['success'] ="true";
            $output['success_message'] ="Pharmacy  Added Successfully";
            $output['url'] = base_url().'admin/pharmacylist';
            $output['delayTime'] ='3000';
            $output['resetform'] ='true';
          }
          else
          {
            $output['formErrors'] ="true";
            $output['errors'] ="Pharmacy  is Not Added";
          }
        }

          echo json_encode($output);
          exit;


      }

      public function pharmacyDelete()
      {
        $query = $this->common->delete('users',array("userId"=>$_POST['id']));
        if($query)
        {
        $query1 = $this->common->delete('pharmacy',array("userId"=>$_POST['id']));
        }
        if($query1)
        {
          $msg['success']="true";
          $msg['success_message'] ="Pharmacy Deleted Successfully";
        }
        else
        {
          $msg['errors'] ="false";
          $msg['errors'] ="Pharmacy  is Not Delete";

        }
        echo json_encode($msg);
        exit;
      }



      public function pharmacy_Edit($id)
      {
        $this->securite();
        $data['result'] = $this->common->getrow('pharmacy',array("userId"=>$id));
        $data['email'] = $this->common->getrow('users',array("userId"=>$id));
        $this->load->view('admin/header');
        $this->load->view('admin/editpharmacy',$data);
        $this->load->view('admin/footer');
      }

      public function pharmacyUpdate($id)
      {
         unset($_POST['email']);
        $update = $this->common->update(array("pharmacyId"=>$id),$_POST,'pharmacy');
        if($update)
        {
          $output['success'] ="true";
          $output['success_message'] ="Pharmacy Updated Successfully";
          $output['url'] = base_url().'admin/pharmacylist';
          $output['delayTime'] ='3000';
        }
        else
        {
          $output['formErrors'] ="true";
          $output['errors'] ="Pharmacy Is Not Updated";
        }
        echo json_encode($output);
        exit;
      }

      public function getpharmacyperpage()
      {
        if(!empty($_POST['perpage']))
      {
        $perpage =$_POST['perpage'];
      }
      else
      {
        $perpage =10;
      }
      if(!empty($_POST['date']))
      {
       $_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
      }

      $config = array();

      if(!empty($_POST['searchtext']))
      {
        $data['pcount'] = $this->common->count_searchpharmacy($_POST['searchtext']);
        $config["total_rows"] = $this->common->count_searchpharmacy($_POST['searchtext']);
      }
      else
      {
      $data['pcount'] = $this->common->count_all_table('pharmacy');
      $config["total_rows"] = $this->common->count_all_table('pharmacy');
      }
      $config["base_url"] = base_url() . 'admin/getdoctorperpage/';
      $config["per_page"] = $perpage;
      $config["uri_segment"] = 3;
      $this->pagination->initialize($config);
      $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
      if( $page <= 0 )
      {
        $page = 1;
      }
      $start = ($page-1) * $config["per_page"];
      $data['start'] = $start;
      if(!empty($_POST['searchtext']))
      {
      $data['result'] = $this->common->searchpharmacy($_POST['searchtext'],$start,$config["per_page"]);
      }
      else
      {
        $data['result'] = $this->common->getbypagination('pharmacy','pharmacyId',$start,$config["per_page"]);
      }
      $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
      $data['start'] = $start;
      if($data)
      {
        $output['success'] = "true";
        $output['result'] = $data;
      }
      echo json_encode($output);
      exit;
      }

      // pharmacy section



      public function prescription()
      {
        $this->securite();
        $data['pcount'] = $this->common->count_all_table('prescription');
        $config = array();
        $config["base_url"] = base_url() . 'admin/prescriptionlist/';
        $config["total_rows"] = $this->common->count_all_table("prescription");
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        if( $page <= 0 )
        {
          $page = 1;
        }
        $start = ($page-1) * $config["per_page"];
        $data['start'] = $start;
        $data['result'] = $this->common->adminprescription($start,$config["per_page"]);
        $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
        $data['pharmacy'] = $this->common->getTable('pharmacy');


        $this->load->view('admin/header');
        $this->load->view('admin/prescription',$data);
        $this->load->view('admin/footer');

      }


  // earning


  public function getprescriptionperpage()
  {
    if(!empty($_POST['perpage']))
  {
    $perpage =$_POST['perpage'];
  }
  else
  {
    $perpage =10;
  }
  if(!empty($_POST['date']))
  {
   $_POST['date'] = date("Y-m-d", strtotime($_POST['date']));
  }

  $config = array();
  if(!empty($_POST['searchtext']))
  {
    $data['pcount'] =   $data['pcount'] = $this->common->count_searchadminprescription($_POST['searchtext']);
    $config["total_rows"] =   $data['pcount'] = $this->common->count_searchadminprescription($_POST['searchtext']);
  }
  else
  {
  $data['pcount'] =   $data['pcount'] = $this->common->count_all_table('prescription');
  $config["total_rows"] =   $data['pcount'] = $this->common->count_all_table('prescription');
  }

  $config["base_url"] = base_url() . 'admin/getprescriptionperpage/';
  $config["per_page"] = $perpage;
  $config["uri_segment"] = 3;
  $this->pagination->initialize($config);
  $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
  if( $page <= 0 )
  {
    $page = 1;
  }
  $start = ($page-1) * $config["per_page"];
  $data['start'] = $start;
  if(!empty($_POST['searchtext']))
  {
  $data['result'] = $this->common->adminsearchprescription($_POST['searchtext'],$start,$config["per_page"]);
  }
  else
  {
    $data['result'] = $this->common->adminprescription($start,$config["per_page"]);
  }
  $data["links"] = getPaginationlink($config["per_page"],$config["total_rows"],$config["base_url"],$config["uri_segment"],1);
  $data['start'] = $start;
  if($data)
  {
    $output['success'] = "true";
    $output['result'] = $data;
  }
  echo json_encode($output);
  exit;
  }

  public function prescriptionView()
  {
    $result = $this->common->viewprescription(array("prescriptionId"=>$_POST['id']));
    $medicine = $this->common->getmedicine(array("prescriptionId"=>$_POST['id']));

    if($result)
    {
      $output['success'] ="true";
      $output['result'] = $result;
      $output['medicine'] = $medicine;
    }
    else
    {
      $output['success'] ="false";
    }
    echo json_encode($output);
    exit;
  }

  public function convertimage($id)
  {
    $data['id'] = $id;
    $data['result'] = $this->common->viewprescription(array("prescriptionId"=>$id)) ;
    $data['medicines'] = $this->common->getmedicine(array("prescriptionId"=>$id)) ;
    $this->load->view('email/imageconvert',$data);
  }




    //************************* forgotPassword********************************
      public function forgotPassword()
      {
        $this->load->view('admin/forgot_password');
      }

    public function forgotcheckemail()
    {
      $mail ='';
      $res = $this->common->getrow('admin',array("email"=>$_POST['email']));

     if(!empty($res))
    {
         // $length           = 8;
         // $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
         // $charactersLength = strlen($characters);
         // $random           = rand();
         // for ($i = 0; $i < $length; $i++)
         // {
         // $random .= $characters[rand(0, $charactersLength - 1)];
         // }
        // $doctor = $this->common->getrow('doctor',array("userId"=>$res->userId));
         $detail['password'] = $res->pass;
         $detail['name'] = 'admin';
         // $res = $this->common->update(array("userId"=>$res->userId),array("userPassword"=>md5($random)),'users');
         $msg = $this->load->view('email/forgotPassword',$detail,true);
         $mail =  $this->mailsend('Reset Password',$_POST['email'],$msg);
        if($mail)
      {
        $output['success'] ="true";
        $output['success_message'] ="Password sent to your email. Please check your email. ";
        $output['resetform'] ='true';
     $output['url'] = base_url().'admin';
     $output['delayTime'] ='3000';
   }
   else
   {
    $output['formErrors'] ="true";
     $output['errors'] ="Error in Email Sending.";
   }
   }
   else
   {
     $output['formErrors'] ="true";
     $output['errors'] ="Email does not exists.";
   }
   echo json_encode($output);
}
    //************************* forgotPassword********************************


    public function mailsend($sub,$to,$msg)
    {
      $ci = & get_instance();
      $ci->email_var = array(
        'site_title' => $ci->config->item('site_title'), 'site_url' => site_url()
      );

      $ci->config_email = Array (
         'protocol' => "smpt",
         'smtp_host' => "business17.web-hosting.com",
         'smtp_port' => '587',
         'smtp_user' => 'admin@doctortouchrx.com',
        'smtp_pass' => '@Admin1978',
        'mailtype' => "html",
        'wordwrap' => TRUE,
        'crlf' => '\r\n',
        'charset' => "utf-8"
      );

      $ci->email->initialize($ci->config_email);
      $ci->email->set_newline("\r\n");
      $ci->email->from('admin@doctortouchrx.com', 'Doctortouchrx');
         $ci->email->to($to);
      $ci->email->subject($sub);
      $ci->email->message($msg);
      if ($ci->email->send()) {
        $ci->email->clear(TRUE);

        return TRUE;

      }
      else
      {

        return FALSE;
      }
    }


	public function mailer($sub,$to,$msg)
      {
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';

        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);


        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");
        $this->email->from('noreply@prescription.com', 'Prescription');
        $this->email->to($to);
        $this->email->subject($sub);
        $this->email->message($msg);

        if ($this->email->send())
        {
        return true;
        }
        else
        {
          // echo $ci->email->print_debugger();
          return false;
        }
      }
  // =============================


  public function Export($id = null)
	{
		$nowUtc = new DateTime( 'now',  new DateTimeZone( 'UTC' ) );
		$date = $nowUtc->format('d-m-Y');


			$fileName = 'Report-'.$date.'.xlsx';

		$this->load->library('excel');


    if(!empty($id))
    {
		$results = $this->common->AdminExportprescription1(array("p.pharmacyId"=>$id));
    }
    else
    {
    $results = $this->common->AdminExportprescription();
    }


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		// set Header
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'S . No');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Date');
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Order No');
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Doctor Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Pharmacy Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Patient Name');
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Email');
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Phone');
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Patient Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Insurance Pay');
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Total Amount');
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Mode of Order');

			// set Row
			$rowCount = 2;
			$i = 1;
			if(!empty($results))
			{
			foreach ($results as $element)
			{
			$objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
      $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, date("d-m-Y", strtotime($element->date)));
			$objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->orderNo);
			$objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->name);
			$objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->pharmacy);
			$objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element->patientName);
			$objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element->email);
			$objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element->phone);
			$objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element->payableAmount);
			$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element->copay);
			$objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element->totalamount);
      if($element->delivery == 1)
      {
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount,"Delivery");
      }
      else
      {
			$objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, "Self Pickup");
      }



			$rowCount++;
			$i++;

			}
		 }

			$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$doc = $fileName;

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

			ob_end_clean();
			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="'.$doc.'"');
			$writer->save('php://output');
	}



}
