<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Common extends CI_Model {

    public function __construct()
   {
        parent::__construct();
    }



    public function getSingleUser($whr)
    {
      $this->db->select("us.email,us.successScore,us.parent,us.rating,us.is_active,us.type,us.date_created,um.*,(select name from countries  where id = um.country ) as country,(select name from states  where id = um.state ) as state,(select name from cities  where id = um.city ) as city,(select timeStamp from user_onlinelog where u_id = um.u_id ) as timeStamp");
      $this->db->from('user as us');
      $this->db->join('user_meta as um','um.u_id = us.id');
      $this->db->where($whr);
      $result = $this->db->get();
      $result = $result->row();
      return $result;
    }


    public function getIn($tbl,$field,$ids)
    {
      $this->db->select('*');
      $this->db->from($tbl);
      $this->db->where_in($field,$ids);
      $result = $this->db->get();
      $result = $result->result();
      return $result;
    }

    public function getRecentOne($tbl,$whr,$field,$sort)
    {
      $this->db->select('*');
      $this->db->from($tbl);
      $this->db->where($whr);
      $this->db->limit(1);
      $this->db->order_by($field,$sort);
      $result = $this->db->get();
      $result = $result->row();
      return $result;
    }

    public function getByOrder($tbl,$whr,$field,$sort)
    {
      $this->db->select('*');
      $this->db->from($tbl);
      $this->db->where($whr);
      $this->db->order_by($field,$sort);
      $result = $this->db->get();
      $result = $result->result();
      return $result;
    }

    public function getOneRow($tbl,$whr,$field,$sort)
    {
      $this->db->select('*');
      $this->db->from($tbl);
      $this->db->where($whr);
      $this->db->limit(1);
      $this->db->order_by($field,$sort);
      $result = $this->db->get();
      $result = $result->row();
      return $result;
    }

    public function getone($tbl)
    {
      $this->db->select('*');
      $this->db->from($tbl);
      $this->db->limit(1);
      $result = $this->db->get();
      $result = $result->row();
      return $result;
    }



     public function getorder($userid)
     {
       $this->db->select('*');
       $this->db->from('chdmart_order');
       $this->db->where_in('orderId',"SELECT orderId FROM chdmart_orderitem where storeuserId='$userid'", FALSE);
       $result = $this->db->get();
       $result = $result->result();
        // $result = $this->db->last_query();
       return $result;
     }



  	public function update($where,$data,$table)
  	{
  		$this->db->where($where);
  		$this->db->update($table,$data);
  		$db_error = $this->db->error();
  		if ($db_error['code'] == 0)
  			$output = array (1);
  		else
  			$output = array (0);
  			return $output;
  	}

    public  function autocomplete($field,$keyword,$tbl)
     {
        $this->db->select('*');
        $this->db->from($tbl);
        $this->db->like($field,$keyword,'both');
        $this->db->limit('10');
        $query = $this->db->get();
        return $query->result();
   }


    public function delete($table,$where)
  	{
  		$this->db->where($where);
  		$this->db->delete($table);
  		$db_error = $this->db->error();
  		if ($db_error['code'] == 0)
  			return '1';
  		else
  			return '0';
  	}

    public function deleteMultiple($table,$where,$values)
    {
      $this->db->where_in($where, $values);
      $this->db->delete($table);
      $db_error = $this->db->error();
  		if ($db_error['code'] == 0)
  			return '1';
  		else
  			return '0';
    }

  	public function insert($table,$data)
  	{
  		$this->db->insert($table,$data);
  		$insert_id = $this->db->insert_id();
  		if( $insert_id != 0)
  			$output = array (1,$insert_id);
  		else
  			$output = array (0);
  		return $output;
  	}

  	public function insert_batch($table,$data)
  	{
  		$this->db->insert_batch($table,$data);
  		$insert_id = $this->db->insert_id();
  		if( $insert_id != 0)
  			$output = array (1,$insert_id);
  		else
  			$output = array (0);
  		return $output;
  	}

  	public function checkexist($table,$where)
  	{
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $output = $this->db->get();
        $output = $output->num_rows();
		    return $output;
  	}

  public function getbylimit($tbl,$whr,$limit)
  {
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->limit($limit);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }

  public function getbylimitStart($tbl,$whr,$start,$perPage)
  {
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->limit($perPage,$start);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }


public function getbypagination($tbl,$orderby,$start,$perPage)
{
  $this->db->select('*');
  $this->db->from($tbl);
  // $this->db->where($whr);
  $this->db->limit($perPage,$start);
  $this->db->order_by($orderby,'desc');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function getbypaginationPolicy($tbl,$orderby,$start,$perPage)
{
  $this->db->select('*');
  $this->db->from($tbl);
  // $this->db->where($whr);
  $this->db->limit($perPage,$start);
  $this->db->order_by($orderby,'asc');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}


public function getbypaginationslider($tbl,$start,$perPage)
{
  $this->db->select('*');
  $this->db->from($tbl);
  // $this->db->where($whr);
  $this->db->limit($perPage,$start);
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

    public function getTableFields($value ='')
    {
      $fields = $this->db->list_fields($value);
      return $fields;
    }

  public function get($tbl,$whr)
	{
		$this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
	}




  public function get_groupby($tbl,$whr,$group_by)
	{
		$this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->group_by($group_by);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
	}


  public function getSorted($tbl,$whr,$field,$sort)
	{
		$this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->order_by($field, $sort);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
	}




  public function getRecent($tbl,$whr,$id,$howmany)
  {
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->order_by($id, "desc");
    $this->db->limit($howmany);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }




	public function getrow($tbl,$whr)
	{
		$this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->row();
    return $result;
	}



  public function count_all_results($tbl,$whr)
  {
    $this->db->where($whr);
    $this->db->from($tbl);
    $count = $this->db->count_all_results();
    return  $count;
  }

  public function count_like($tbl,$field,$keyword)
  {
    $this->db->like($field,$keyword,'both');
    $this->db->from($tbl);
    $count = $this->db->count_all_results();
    return  $count;
  }


  public function count_all_table($tbl)
  {
    $this->db->from($tbl);
    $count = $this->db->count_all_results();
    return  $count;
  }

  public function count_LatestNotification($tbl,$whr,$lastId)
  {
    $this->db->where($whr);
    $this->db->where('notificationId > ',$lastId);
    $this->db->from($tbl);
    $count = $this->db->count_all_results();
    return  $count;
  }

  public function getTable($tbl)
	{
		$this->db->select('*');
    $this->db->from($tbl);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
	}

  public function getFields($tbl,$whr,$fields)
	{
		$this->db->select(implode(',',$fields));
    $this->db->from($tbl);
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
	}

  public function getexpertcategory($whr)
  {
    $this->db->select('*');
    $this->db->from('hc_user_expert as e');
    $this->db->join('hc_category as c','c.category_id = e.category_id');
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }
  public function getexpertskill($whr)
  {
    $this->db->select('*');
    $this->db->from('hc_user_skill as e');
    $this->db->join('hc_additionalSkill as c','c.id = e.skill');
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }
  public function getexpertsubcategory($whr)
  {
    $this->db->select('*');
    $this->db->from('hc_user_subcategory as e');
    $this->db->join('hc_serviceDuration as c','c.id = e.subcategoryId');
    $this->db->where($whr);
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }

  public function getbypaginationWhere($tbl,$whr,$orderby,$start,$perPage)
  {
    $this->db->select('*');
    $this->db->from($tbl);
    $this->db->where($whr);
    $this->db->limit($perPage,$start);
    $this->db->order_by($orderby,'desc');
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }

  public function getyear($whr)
  {
    $this->db->select('YEAR(c.date) AS year');
    $this->db->from('chat as c');
    $this->db->where($whr);
    $this->db->group_by('year(date)');
    $result = $this->db->get();
    $result=$result->result();
    return $result;
  }


  public function getEarning($start,$perPage)
  {
    $this->db->select('e.*,p.patientFirstName as clientfirstName,p.patientLastName as clientlastName');
    $this->db->from('earning as e');
    $this->db->join('patient as p','p.patientId = e.patientId');
    $this->db->limit($perPage,$start);
    $this->db->order_by('earningId','desc');
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }


    public function getAllEarning()
    {
      $this->db->select('e.*,p.patientFirstName as clientfirstName,p.patientLastName as clientlastName,p.patientHours');
      $this->db->from('earning as e');
      $this->db->join('patient as p','p.patientId = e.patientId');
      $this->db->order_by('earningId','desc');
      $result = $this->db->get();
      $result = $result->result();
      return $result;
    }

public function getSearchEarning($whr,$start,$perPage)
{
  $this->db->select('e.*,p.patientFirstName as clientfirstName,p.patientLastName as clientlastName');
  $this->db->from('earning as e');
  $this->db->join('patient as p','p.patientId = e.patientId');
  $this->db->where($whr);
  $this->db->limit($perPage,$start);
  $this->db->order_by('earningId','desc');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function getemployee()
{
  $this->db->select('employeeId as id,employeeEmail as email,employeeFirstName as name');
  $this->db->from('employee');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function getclient()
{
  $this->db->select('patientId as id,patientEmail as email,patientFirstName as name');
  $this->db->from('patient');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}


public function getclientdocument()
{
  $this->db->select('patientDocumentId as id,patientDocument as document');
  $this->db->from('patientDocument');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function getemployeedocument()
{
  $this->db->select('employeeId as id,employeeDocument as document');
  $this->db->from('employeeDocument');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function gethiredocument()
{
  $this->db->select('hireId as id,hireDocument as document');
  $this->db->from('hire');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

public function getpolicydocument()
{
  $this->db->select('packetId as id,packetDocument as document');
  $this->db->from('packet');
  $result = $this->db->get();
  $result = $result->result();
  return $result;
}

function getSum($table)
 {
   $this->db->select('sum(earningAmount) as total');
   $this->db->from($table);
   $output = $this->db->get();
   $output = $output->row();
   return $output;
 }

 public function getFeaturedServices($tbl,$whr)
 {
   $this->db->select('*');
   $this->db->from($tbl);
   $this->db->where($whr);
   $this->db->limit(3);
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function getbrockerkey($whr)
 {
   $this->db->select('*');
   $this->db->from('accounts as a');
   $this->db->join('account_use as u','a.stubId = u.stubId');
   $this->db->where($whr);
   $result = $this->db->get();
   $result = $result->row();
   return $result;
 }

 public function getallexchange($whr)
 {
   $this->db->select('l.exchange');
   $this->db->from('linkedorder as l');
   $this->db->where($whr);
   $this->db->group_by('l.exchange');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function count_searchlinkedorder($whr,$postdata)
 {
   $this->db->where($whr);
   if($postdata['exchange'] !='')
   {
   $this->db->where('exchange',$postdata['exchange']);
   }
   if($postdata['position'] !='')
   {
   $this->db->where('position',$postdata['position']);
   }
   if($postdata['date'] !='')
   {
   $this->db->where('date',$postdata['date']);
   }
   $this->db->from('linkedorder');
   $count = $this->db->count_all_results();
   return  $count;
 }

 public function searchlinkedorder($whr,$postdata,$start,$perPage)
 {
   $this->db->select('*');
   $this->db->from('linkedorder');
   $this->db->where($whr);
   if($postdata['exchange'] !='')
   {
   $this->db->where('exchange',$postdata['exchange']);
   }
   if($postdata['position'] !='')
   {
   $this->db->where('position',$postdata['position']);
   }
   if($postdata['date'] !='')
   {
   $this->db->where('date',$postdata['date']);
   }
   $this->db->limit($perPage,$start);
   $this->db->order_by('linkedorderId','desc');

   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function getprofile($whr)
 {
   $this->db->select('*');
   $this->db->from('users as u');
   $this->db->join('doctor as d','d.userId = u.userId');
   $this->db->where($whr);
   $result = $this->db->get();
   $result = $result->row();
   return $result;
 }

 public function getpharmacyprofile($whr)
 {
   $this->db->select('*');
   $this->db->from('users as u');
   $this->db->join('pharmacy as d','d.userId = u.userId');
   $this->db->where($whr);
   $result = $this->db->get();
   $result = $result->row();
   return $result;
 }

 public function alldoctor($start,$perPage)
 {
   $this->db->select('*');
   $this->db->from('users as u');
   $this->db->join('doctor as d','d.userId = u.userId');
   $this->db->limit($perPage,$start);
   $this->db->order_by('u.userId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function count_searchdoctor($search)
 {
   $this->db->like('u.userEmail',$search,'both');
   $this->db->or_like('d.name',$search,'both');
   $this->db->from('users as u');
   $this->db->join('doctor as d','d.userId = u.userId');
   $count = $this->db->count_all_results();
   return  $count;
 }

 public function searchdoctor($search,$start,$perPage)
 {
   $this->db->select('*');
   $this->db->from('users as u');
   $this->db->join('doctor as d','d.userId = u.userId');
   $this->db->like('u.userEmail',$search,'both');
   $this->db->or_like('d.name',$search,'both');
   $this->db->or_like('d.phone',$search,'both');
   $this->db->limit($perPage,$start);
   $this->db->order_by('u.userId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function count_searchpharmacy($search)
 {
   $this->db->like('userEmail',$search,'both');
   $this->db->or_like('name',$search,'both');
   $this->db->from('pharmacy as p');
   $this->db->join('users as u','u.userId = p.userId');
   $count = $this->db->count_all_results();
   return  $count;
 }

 public function searchpharmacy($search,$start,$perPage)
 {
   $this->db->select('*');
   $this->db->from('pharmacy as p');
   $this->db->from('users as u','u.userId = p.userId');
   $this->db->like('userEmail',$search,'both');
   $this->db->or_like('name',$search,'both');
   $this->db->or_like('phone',$search,'both');
   $this->db->limit($perPage,$start);
   $this->db->group_by('p.pharmacyId');
   $this->db->order_by('p.pharmacyId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function allprescription($where,$start,$perPage)
 {
   $this->db->select('p.*,ph.name as pharmacy,d.*');
   $this->db->from('prescription as p');
   $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
   $this->db->join('doctor as d','d.userId = p.userId','left');
   $this->db->where($where);
   $this->db->limit($perPage,$start);
   $this->db->order_by('p.prescriptionId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function count_searchprescription($where,$search)
 {
   $this->db->like('email',$search,'both');
   $this->db->or_like('patientName',$search,'both');
   $this->db->where($where);
   $this->db->from('prescription');
   $count = $this->db->count_all_results();
   return  $count;
 }
 public function count_searchadminprescription($search)
 {
   $this->db->like('email',$search,'both');
   $this->db->or_like('patientName',$search,'both');
   $this->db->from('prescription');
   $count = $this->db->count_all_results();
   return  $count;
 }

 public function searchprescription($where,$search,$start,$perPage)
 {
   $this->db->select('p.*,ph.name as pharmacy,d.*');
   $this->db->from('prescription as p');
   $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
   $this->db->join('doctor as d','d.userId = p.userId','left');
   $this->db->where($where);
   $this->db->like('p.email',$search,'both');
   $this->db->or_like('p.patientName',$search,'both');
   $this->db->or_like('d.name',$search,'both');
   $this->db->or_like('p.orderNo',$search,'both');
   $this->db->limit($perPage,$start);
   $this->db->order_by('p.prescriptionId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function adminsearchprescription($search,$start,$perPage)
 {
   $this->db->select('p.*,ph.name as pharmacy,d.name as doctor');
   $this->db->from('prescription as p');
   $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
   $this->db->join('doctor as d','d.userId = p.userId','left');
   $this->db->like('p.email',$search,'both');
   $this->db->or_like('p.patientName',$search,'both');
   $this->db->or_like('d.name',$search,'both');
   $this->db->or_like('d.phone',$search,'both');
   $this->db->or_like('ph.name',$search,'both');
   $this->db->limit($perPage,$start);
   $this->db->order_by('p.prescriptionId','desc');
   $result = $this->db->get();
   $result = $result->result();
   return $result;
 }

 public function viewprescription($where)
 {
   $this->db->select('p.*,ph.name as pharmacy,d.name as doctor,d.phone as dphone,d.license,d.signature,ph.phone as phphone,ph.address as paddress,u.userEmail as pemail');
   $this->db->from('prescription as p');
   $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
   $this->db->join('doctor as d','d.userId = p.userId','left');
   $this->db->join('users as u','u.userId = p.pharmacyId','left');
   $this->db->where($where);
   $result = $this->db->get();
   $result = $result->row();
   return $result;
 }

  public function adminprescription($start,$perPage)
  {
    $this->db->select('p.*,ph.name as pharmacy,d.name as doctor');
    $this->db->from('prescription as p');
    $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
    $this->db->join('doctor as d','d.userId = p.userId','left');
    $this->db->limit($perPage,$start);
    $this->db->order_by('p.prescriptionId','desc');
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }


  public function getpharmacy($start,$perPage)
  {
    $this->db->select('*');
    $this->db->from('pharmacy as p');
    $this->db->join('users as u','u.userId = p.userId');
    $this->db->limit($perPage,$start);
    $this->db->order_by('pharmacyId','desc');
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }

  public function count_searchdrug($whr,$search)
  {
    $this->db->like('drugName',$search,'both');
    $this->db->where($whr);
    $this->db->from('drug');
    $count = $this->db->count_all_results();
    return  $count;
  }

  public function searchdrug($where,$search,$start,$perPage)
  {
    $this->db->select('*');
    $this->db->from('drug as p');
    $this->db->where($where);
    $this->db->like('drugName',$search,'both');
    $this->db->limit($perPage,$start);
    $this->db->order_by('drugId','desc');
    $result = $this->db->get();
    $result = $result->result();
    return $result;
  }

  public function count_report($where,$startdate,$enddate)
  {
    if($startdate != '' && $enddate != '')
    {
    $this->db->where("date >=",$startdate);
    $this->db->where("date <=",$enddate);
     }
    $this->db->where($where);
    $this->db->from('prescription');
    $count = $this->db->count_all_results();
    return  $count;
  }


   public function searchreport($where,$startdate,$enddate,$start,$perPage)
   {
     $this->db->select('p.*,ph.name as pharmacy,d.*');
     $this->db->from('prescription as p');
     $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
     $this->db->join('doctor as d','d.userId = p.userId','left');
     $this->db->where($where);
     if($startdate != '' && $enddate != '')
     {
     $this->db->where("p.date >=",$startdate);
     $this->db->where("p.date <=",$enddate);
      }
     $this->db->limit($perPage,$start);
     $this->db->order_by('p.prescriptionId','desc');
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }


   public function getmedicineLike($where,$keyword)
   {
     $this->db->select('*');
     $this->db->from('drug');
     $this->db->where($where);
     $this->db->like('drugName',$keyword,'after');
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }

   public function getmedicine($where)
   {
     $this->db->select('m.*,d.drugName,do.drugDose');
     $this->db->from('medicine as m');
     $this->db->join('drug as d','d.drugId = m.drugId');
     $this->db->join('drugDose as do','do.drugDoseId = m.drugDoseId','left');
     $this->db->where($where);
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }

   public function Exportprescription($where)
   {
     $this->db->select('p.*,ph.name as pharmacy,d.*');
     $this->db->from('prescription as p');
     $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
     $this->db->join('doctor as d','d.userId = p.userId','left');
     $this->db->where($where);
     $this->db->order_by('p.prescriptionId','desc');
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }
   public function AdminExportprescription()
   {
     $this->db->select('p.*,ph.name as pharmacy,d.*');
     $this->db->from('prescription as p');
     $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
     $this->db->join('doctor as d','d.userId = p.userId','left');
     $this->db->order_by('p.prescriptionId','desc');
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }

   public function AdminExportprescription1($whr)
   {
     $this->db->select('p.*,ph.name as pharmacy,d.*');
     $this->db->from('prescription as p');
     $this->db->join('pharmacy as ph','ph.userId = p.pharmacyId','left');
     $this->db->join('doctor as d','d.userId = p.userId','left');
     $this->db->where($whr);
     $this->db->order_by('p.prescriptionId','desc');
     $result = $this->db->get();
     $result = $result->result();
     return $result;
   }

}
?>
