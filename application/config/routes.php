<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'doctor';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


// *********************Doctor******************************
$route['doctor']                 = 'doctor/index';
$route['doctor/logout']          = 'doctor/logout';
$route['doctor/dashboard']       = 'doctor/dashboard';
$route['doctor/password']        = 'doctor/password';
$route['doctor/passwordUpdate']  = 'doctor/passwordUpdate';
$route['doctor/profile']  = 'doctor/profile';
$route['doctor/profileUpdate']  = 'doctor/profileUpdate';

$route['doctor/prescriptionlist']  = 'doctor/prescription';
$route['doctor/prescription/add']  = 'doctor/prescription_add';
$route['doctor/prescriptionSave']  = 'doctor/prescriptionSave';
$route['doctor/prescriptionDelete']  = 'doctor/prescriptionDelete';
$route['doctor/prescription/edit/(:any)']  = 'doctor/prescription_edit/$1';
$route['doctor/prescription/clone/(:any)']  = 'doctor/prescription_clone/$1';
$route['doctor/prescriptionUpdate/(:any)']  = 'doctor/prescriptionUpdate/$1';
$route["doctor/getprescriptionperpage"]  ="doctor/getprescriptionperpage";
$route["doctor/getprescriptionperpage/(:any)"]  ="doctor/getprescriptionperpage/$1";
$route["doctor/prescriptionView"]  ="doctor/prescriptionView";
$route["doctor/imageupload"]  ="doctor/imageupload";
$route["doctor/imageupload1"]  ="doctor/imageupload1";
$route["doctor/convertimage/(:any)"]  ="doctor/convertimage/$1";
$route["doctor/downloadimage/(:any)"]  ="doctor/convertimage1/$1";

$route["doctor/cron"]  ="doctor/cron";
$route["doctor/cron1"]  ="doctor/cron1";


$route['doctor/forgot-password'] = "doctor/forgotPassword";
$route['doctor/forgotcheckemail'] = "doctor/forgotcheckemail";

$route['doctor/getmedicine'] = "doctor/getmedicine";
$route['doctor/getdose'] = "doctor/getdose";

// *********************Doctor******************************

// *********************pharmacy******************************
$route['pharmacy/(:any)']       = 'pharmacy/index/$1';
$route['pharmacy1/dashboard/(:any)']       = 'pharmacy/dashboard';
$route['pharmacy1/password']        = 'pharmacy/password';
$route['pharmacy1/passwordUpdate']  = 'pharmacy/passwordUpdate';
$route['pharmacy1/profile/(:any)']  = 'pharmacy/profile';
$route['pharmacy1/profileUpdate']  = 'pharmacy/profileUpdate';

$route['pharmacy1/druglist/(:any)']  = 'pharmacy/drug';
$route['pharmacy1/druglist']  = 'pharmacy/drug';
$route['pharmacy1/drugDelete']  = 'pharmacy/drugDelete';
$route['pharmacy1/getdrugperpage']  = 'pharmacy/getdrugperpage';
$route['pharmacy1/getdrugperpage/(:any)']  = 'pharmacy/getdrugperpage/$1';
$route['pharmacy1/drug/add']  = 'pharmacy/drug_add';
$route['pharmacy1/drugSave']  = 'pharmacy/drugSave';
$route['pharmacy1/drug/edit/(:any)']  = 'pharmacy/drugEdit/$1';
$route['pharmacy1/drugUpdate/(:any)']  = 'pharmacy/drugUpdate/$1';
$route['pharmacy1/prescriptionlist']  = 'pharmacy/prescription';
$route['pharmacy1/prescriptionlist/(:any)']  = 'pharmacy/prescription';
$route['pharmacy1/getprescriptionperpage']  = 'pharmacy/getprescriptionperpage';
$route['pharmacy1/getprescriptionperpage/(:any)']  = 'pharmacy/getprescriptionperpage/$1';
$route['pharmacy1/prescriptionView'] = 'pharmacy/prescriptionView';

$route['pharmacy1/reportlist/(:any)'] = 'pharmacy/report';
$route['pharmacy1/prescriptionStatus'] = 'pharmacy/prescriptionStatus';
$route['pharmacy1/prescriptionCost'] = 'pharmacy/prescriptionCost';
$route['pharmacy1/getreportperpage'] = 'pharmacy/getreportperpage';

$route['pharmacy1/imagesend/(:any)'] = 'pharmacy/imagesend/$1';
$route['pharmacy1/imageupload1'] = 'pharmacy/imageupload1';
$route['pharmacy1/Export'] = 'pharmacy/Export';
$route['pharmacy1/logout/(:any)'] = 'pharmacy/logout/$1';
$route['pharmacy1/forgot-password'] = "pharmacy/forgotPassword";
$route['pharmacy1/forgotcheckemail'] = "pharmacy/forgotcheckemail";


// ************content*********
$route["pharmacy1/pageslist"]  = "pharmacy/pages";
$route["pharmacy1/pageslist/(:any)"]  = "pharmacy/pages";
$route["pharmacy1/pageslist/(:any)"]  = "pharmacy/pages/$1";
$route["pharmacy1/pages/add"]  = "pharmacy/pages_add/";
$route["pharmacy1/Savepages"]  = "pharmacy/pagesSave";
$route["pharmacy1/pages/edit/(:any)"]  ="pharmacy/pages_edit/$1";
$route["pharmacy1/pagesUpdate/(:any)"]  ="pharmacy/pagesUpdate/$1";
$route["pharmacy1/pagesDelete"]  ="pharmacy/deletepages";
$route["pharmacy1/pagesStatus"]  ="pharmacy/pagesStatus";
$route["pharmacy1/getpagesperpage"]  ="pharmacy/getpagesperpage";
$route["pharmacy1/getpagesperpage/(:any)"]  ="pharmacy/getpagesperpage/$1";

$route["pharmacy/p/(:any)"]  ="pharmacy/content/$1";

$route["pharmacy1/merchant"]  ="pharmacy/merchant";
$route["pharmacy1/merchant/(:any)"]  ="pharmacy/merchant";
$route["pharmacy1/merchantUpdate"]  ="pharmacy/merchantUpdate";
// ************content*********
// *********************Pharmacy******************************


// *********************Patient******************************
$route["patient/payment/(:any)"]  ="patient/payment/$1";
$route["patient/success"]  ="patient/success";

// *********************Patient******************************


/*************** massage admin route********************/
$route['admin']                 = 'admin/index';
$route['admin/logout']          = 'admin/logout';
$route['admin/dashboard']       = 'admin/dashboard';
$route['admin/password']        = 'admin/password';
$route['admin/passwordUpdate']  = 'admin/passwordUpdate';


//doctor
$route["admin/doctorlist"]  = "admin/doctor";
$route["admin/doctorlist/(:any)"]  = "admin/doctor/$1";
$route["admin/doctor/add"]  = "admin/doctor_add/";
$route["admin/doctorSave"]  = "admin/doctorSave";
$route["admin/doctor/edit/(:any)"]  ="admin/doctor_edit/$1";
$route["admin/doctorUpdate/(:any)"]  ="admin/doctorUpdate/$1";
$route["admin/doctorDelete"]  ="admin/doctorDelete";
$route["admin/getdoctorperpage"]  ="admin/getdoctorperpage";
$route["admin/getdoctorperpage/(:any)"]  ="admin/getdoctorperpage/$1";
// doctor

// pharmacy
$route["admin/pharmacylist"]  = "admin/pharmacy";
$route["admin/pharmacylist/(:any)"]  = "admin/pharmacy/$1";
$route["admin/pharmacy/add"]  = "admin/pharmacy_add/";
$route["admin/pharmacySave"]  = "admin/pharmacySave";
$route["admin/pharmacy/edit/(:any)"]  ="admin/pharmacy_Edit/$1";
$route["admin/pharmacyUpdate/(:any)"]  ="admin/pharmacyUpdate/$1";
$route["admin/pharmacyDelete"]  ="admin/pharmacyDelete";
$route["admin/getpharmacyperpage"]  ="admin/getpharmacyperpage";
$route["admin/getpharmacyperpage/(:any)"]  ="admin/getpharmacyperpage/$1";
// pharmacy

$route['admin/prescriptionlist']  = 'admin/prescription';
$route["admin/getprescriptionperpage"]  ="admin/getprescriptionperpage";
$route["admin/getprescriptionperpage/(:any)"]  ="admin/getprescriptionperpage/$1";
$route["admin/prescriptionView"]  ="admin/prescriptionView";
// download
$route["admin/convertimage/(:any)"]  ="admin/convertimage/$1";
// download
// hire
$route['admin/forgot-password'] = "admin/forgotPassword";
$route['admin/forgotcheckemail'] = "admin/forgotcheckemail";
$route['admin/newpassword/(:any)'] = "admin/newpassword/$1";
$route['admin/newpasswordUpdate'] = "admin/newpasswordUpdate";
$route['admin/settingupdate'] = "admin/settingupdate";
$route['admin/Export'] = "admin/Export";
$route['admin/Export/(:any)'] = "admin/Export/$1";
