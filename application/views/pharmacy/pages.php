

    <div class="page-content">
        <div class="container-fluid">
            <div class="refer-btn">
                <a class="btn btn-rounded"  href="<?php echo base_url(); ?>pharmacy1/pages/add">Add Page</a>
            </div>

            <div class="table-responsive table-sec">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 4%">S.No</th>
                            <th style="width: 13%">Title</th>
                             <th style="width: 6%">Status</th>
                            <th style="width: 6%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="pagesdata">
                      <?php
                       if(!empty($result))
                       {
                         $start = $start + 1;
                         foreach($result as $r)
                         {
                      ?>
                        <tr class="data">
                            <td><?php echo $start++; ?></td>
                            <td><?php echo $r->pagesTitle; ?></td>


                            <td>
                          <?php if($r->pagesStatus == 1)
                            {
                              ?>
                              <a data-id="<?php echo $r->pagesId; ?>" data-status="0" class="pagesStatus pagesStatus<?php echo $r->pagesId; ?> btn btn-success">Active</a>

                        <?php }

                              else if($r->pagesStatus == 0)
                              {
                                ?>
                              <a data-id="<?php echo $r->pagesId; ?>" data-status="1" class="pagesStatus pagesStatus<?php echo $r->pagesId; ?> btn btn-primary">Inactive</a>

                              <?php } ?>
                            </td>


                            <td>

                             <a href="<?php echo base_url(); ?>pharmacy1/pages/edit/<?php echo $r->pagesId; ?>"><i class="fa fa-edit"></i></a>
                             <a class="pagesdelete" data-id="<?php echo $r->pagesId; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                             </td>
                        </tr>
                      <?php } }
                           else
                               {
                                 ?>
                            <tr><td colspan="5">No record</td></tr>
                          <?php } ?>
                    </tbody>
                </table>
                <div class="pagespagination">
                  <?php echo $links; ?>

                </div>
            </div>
        </div>
    </div>
