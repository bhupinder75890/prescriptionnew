<!DOCTYPE html>
<html>

<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Pharmacy Login</title>

	<link href="img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.html" rel="icon" type="image/png">
	<link href="img/favicon-2.html" rel="shortcut icon">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.toast.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/loginpage.css">
<style>
 ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
}

li {
  float: left;
}

li a {
  display: block;
  padding: 8px;
}
.pageslink ul {
float: ;
float: right;
}
.pageslink ul .pagesli {
padding: 10px;
}
.pageslink ul .pagesli a {
padding: 0;
}
.pageslink {
	padding-top: 100px;
}
</style>
</head>
<body>
	<div class="loader_panel" style="display: none;">
		<img src="<?php echo base_url(); ?>assets/image/preloader.gif">
	</div>



		<div class="login_panel">
	    	<div class="login_bx">
	        	<div class="login_logo">
							<?php
							
                if(!empty($logo->logo))
								{
							 ?>
							<img src="<?php echo base_url(); ?>assets/profile/<?php echo $logo->logo; ?>" alt="">
						<?php } else
						{
							?>
							<img src="<?php echo base_url(); ?>assets/admin/img/logo.png" alt="">
           <?php } ?>
	            </div>
	        	<h3>Login</h3>

					 <form id="loginform" class="reset"  method="post" action="<?php echo base_url(); ?>pharmacy">
								 <div class="form-group">
										 <input type="text" id="email" name="email" class="form-control" placeholder="E-Mail"/>

								 </div>
									<div class="form-group">
										 <input type="password" id="password" name="password" class="form-control" placeholder="Password"/>

								 </div>
								 <div class="form-group text-right">
									 <a  href="<?php echo base_url(); ?>pharmacy1/forgot-password" class="forgot">Forgot Password?</a>
								 </div>
								 <button type="submit" class="btn btn-primary">
										 Login
								 </button>
								 <div class="pageslink">
									 <?php
									 if(!empty($pages))
									 {
										 ?>
									 <ul class="pagesul nav  navbar-right ">
										 <?php
										 foreach($pages as $p)
										 {
										 ?>
										 <li class="pagesli"><a href="<?php echo base_url(); ?>pharmacy/p/<?php echo str_replace(' ', '-',strtolower($p->pagesTitle.'-'.$p->pagesId));  ?>"><?php echo ucwords($p->pagesTitle); ?></a></li>
									 <?php } ?>
									 </ul>
								 <?php } ?>
								 </div>
					 </form>
	    <div>

	    </div>
	  </div>
	        </div>


<script src="<?php echo base_url(); ?>assets/admin/js/lib/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/popper/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.toast.js"></script>
<script src="<?php echo base_url(); ?>assets/js/doctorcustom.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="<?php echo base_url(); ?>assets/admin/js/app.js"></script>
</body>

</html>
