<!DOCTYPE html>
<html>

<head lang="en">
<title>Pharmacy Login</title>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.toast.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/loginpage.css">

</head>
<style>

 .pagetitle {
font-size: 34px;
text-transform: uppercase;
font-weight: bolder;
margin: 0 0 0;
background: #fff;
padding: 20px;
width: 100%;
box-shadow: 0 0 8px #ccc;
}

.pagedescription {
background: #fff;
padding: 20px;
box-shadow: 0 0 8px #ccc;
margin-bottom: 30px;
}
.pharmacyname {
font-size: 30px;
margin: 20px 0 15px;
width: 100%;
}
.btn.btn-info {
	margin-left: 1074px;
}
</style>
<body>
<div class="contentpage">
  <div class="container">
    <div class="row">
     <div class="pharmacyname">Pharmacy Name: <?php echo $pharmacy->name; ?></div><br>
     <div class="float-right backbutton"><a href="<?php echo base_url(); ?>pharmacy/<?php echo $pharmacy->userId; ?>" class="btn btn-info">Back</a></a></div>
     <div class="pagetitle"><?php echo $content->pagesTitle; ?></div>
     <div class="pagedescription"><?php echo $content->pagesDescription; ?></div>
    </div>
  </div>
</div>
</body>

<script src="<?php echo base_url(); ?>assets/admin/js/lib/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/popper/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.toast.js"></script>
<script src="<?php echo base_url(); ?>assets/js/doctorcustom.js"></script>
</html>
