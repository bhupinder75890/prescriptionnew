<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("form").submit();
});
</script>
<HTML>
 <BODY>
  <form action="https://uat.kiplepay.com/wcgatewayinit.php" method="post">
   <input type="hidden" name="ord_date" value="<?php echo $date ?>">
   <input type="text" name="ord_totalamt" value="<?php echo $result->payableAmount; ?>" />
   <input type="text" name="ord_gstamt" value="0.00" />
   <input type="hidden" name="ord_shipname" value="<?php echo $result->patientName; ?>">
   <input type="hidden" name="ord_shipcountry" value="Malaysia">
   <input type="hidden" name="ord_mercref" value="<?php echo $result->orderNo; ?>">
   <input type="hidden" name="ord_telephone" value="<?php echo $result->phone; ?>">
   <input type="hidden" name="ord_email" value="<?php echo $result->email; ?>">
   <input type="hidden" name="ord_delcharges" value="0.00">
   <input type="hidden" name="ord_svccharges" value="0.00">
   <input type="text" name="ord_mercID" value="<?php echo $merchant->merchant; ?>">
   <input type="text" name="merchant_hashvalue" value="<?php echo $hash; ?>">
   <input type="hidden" name="ord_returnURL" value="<?php echo base_url(); ?>patient/success">
   <input type="submit" name="submit" value="Pay with kiplePay">
  </form>

 </BODY>
</HTML>
