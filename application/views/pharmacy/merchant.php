

    <div class="page-content" >
        <div class="container-fluid">


            <div class="">

              <form  action="<?php echo base_url(); ?>pharmacy1/merchantUpdate/" enctype="multipart/form-data" method="post" id="merchantUpdate">


                      <h4 class="modal-title">Merchant</h4>




                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Merchant Id<span class="error">*</span></label>
                                <input type="text" value="<?php if(!empty($result)){ echo $result->merchant; } ?>" placeholder="Please enter merchant" class="form-control" name="merchant"   id="merchant">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Merchant Secret<span class="error">*</span></label>
                                <input type="text" value="<?php if(!empty($result)){ echo $result->secret; } ?>" placeholder="Please enter merchant secret" class="form-control" name="secret"   id="secret">
                            </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Update</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            <!-- image crop -->
                  <div class="modal fade" id="myModal1" role="dialog">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Image Crop</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="cropArea">
                                      <img-crop image="myImage" result-image="myCroppedImage"></img-crop>
                                  </div>
                                  <div>Cropped Image:</div>
                                  <div><img ng-src="{{myCroppedImage}}" class="img-fluid"/></div>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" ng-click="logoupload()" class="btn btn-success">Submit</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- image crop -->

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
