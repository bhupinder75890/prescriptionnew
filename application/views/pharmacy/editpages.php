

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>pharmacy1/pagesUpdate/<?php echo $result->pagesId; ?>" method="post" class="reset" id="addpages">


                      <h4 class="modal-title">Add Page</h4>




                      <div class="row">
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Title</label>
                                  <input type="text" class="form-control" name="pagesTitle" value="<?php echo $result->pagesTitle; ?>"  id="pagesTitle">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Status</label>
                                  <select class="form-control pagesStatus1" name="pagesStatus"    id="pagesStatus1">
                                    <option value="">Select Status</option>
                                    <option <?php if($result->pagesStatus == 1){ echo "selected"; } ?> value="1">Active</option>
                                    <option <?php if($result->pagesStatus == 0){ echo "selected"; } ?> value="0">InActive</option>

                                  </select>
                                </div>
                          </div>



                              <div class="col-sm-12">
                                  <div class="form-group">
                                    <label>Description</label>
                                      <textarea type="text" class="form-control ckeditor pagesDescription" name="pagesDescription" value=""   id="pagesDescription"><?php echo $result->pagesDescription; ?></textarea>
                                    </div>
                              </div>

                          </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Save</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
