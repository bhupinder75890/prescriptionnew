
<?php
if (!isset($this->session->userdata['doctorloggedin'])) {
    echo "<script> window.location.href='" . base_url() . "/admin/logout'</script>";

}
?>
<!DOCTYPE html>
<html>



<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Doctor Panel</title>

    <link href="img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
    <link href="img/favicon.html" rel="icon" type="image/png">
    <link href="img/favicon-2.html" rel="shortcut icon">


    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/lobipanel/lobipanel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/separate/vendor/lobipanel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/jqueryui/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/separate/pages/widgets.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.toast.css">
    <!-- datepicker -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css">
    <!-- datepicker -->
</head>
<?php
$img = getrow('doctor',array("userId"=>$this->session->userdata['doctorloggedin']['userId']));
?>
<body class="with-side-menu control-panel control-panel-compact">
  <div class="loader_panel" style="display: none;">
    <img src="<?php echo base_url(); ?>assets/image/preloader.gif">
  </div>
  <input type="hidden" value="<?php echo base_url(); ?>" class="base_url">

    <header class="site-header">
        <div class="container-fluid">
            <a href="<?php echo base_url(); ?>admin/dashboard" class="site-logo">
	            <img class="hidden-md-down" src="<?php echo base_url(); ?>assets/admin/img/logo.png" alt="">
	            <!-- <img class="hidden-lg-down" src="<?php echo base_url(); ?>assets/image/user-one.png" alt=""> -->
	        </a>

            <button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
	            <span>toggle menu</span>
	        </button>

            <button class="hamburger hamburger--htla">
	            <span>toggle menu</span>
	        </button>
            <div class="site-header-content">
                <div class="site-header-content-in">
                    <div class="site-header-shown">
                        <div class="dropdown dropdown-notification notif">
                            <a href="#" class="header-alarm dropdown-toggle active" id="dd-notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	                            <i class="font-icon-alarm"></i>
	                        </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-notif" aria-labelledby="dd-notification">
                                <div class="dropdown-menu-notif-header">
                                    Notifications
                                    <span class="label label-pill label-danger">4</span>
                                </div>
                                <div class="dropdown-menu-notif-list">
                                    <div class="dropdown-menu-notif-item">
                                        <div class="photo">
                                            <img src="img/photo-64-1.jpg" alt="">
                                        </div>
                                        <div class="dot"></div>
                                        <a href="#">Morgan</a> was bothering about something
                                        <div class="color-blue-grey-lighter">7 hours ago</div>
                                    </div>
                                    <div class="dropdown-menu-notif-item">
                                        <div class="photo">
                                            <img src="img/photo-64-2.jpg" alt="">
                                        </div>
                                        <div class="dot"></div>
                                        <a href="#">Lioneli</a> had commented on this <a href="#">Super Important Thing</a>
                                        <div class="color-blue-grey-lighter">7 hours ago</div>
                                    </div>
                                    <div class="dropdown-menu-notif-item">
                                        <div class="photo">
                                            <img src="img/photo-64-3.jpg" alt="">
                                        </div>
                                        <div class="dot"></div>
                                        <a href="#">Xavier</a> had commented on the <a href="#">Movie title</a>
                                        <div class="color-blue-grey-lighter">7 hours ago</div>
                                    </div>
                                    <div class="dropdown-menu-notif-item">
                                        <div class="photo">
                                            <img src="img/photo-64-4.jpg" alt="">
                                        </div>
                                        <a href="#">Lionely</a> wants to go to <a href="#">Cinema</a> with you to see <a href="#">This Movie</a>
                                        <div class="color-blue-grey-lighter">7 hours ago</div>
                                    </div>
                                </div>
                                <div class="dropdown-menu-notif-more">
                                    <a href="#">See more</a>
                                </div>
                            </div>
                        </div>



                        <div class="dropdown user-menu">
                            <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <?php if(!empty($img->image))
                                   {
                                     ?>
	                           <img src="<?php echo base_url(); ?>assets/profile/<?php echo $img->image; ?>" alt="">
                           <?php }
                                else
                                 { ?>
                              <img src="<?php echo base_url(); ?>assets/image/user-one.png" alt="">
                            <?php } ?>
	                        </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                <!-- <a class="dropdown-item" href="profile.html"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
                                <a class="dropdown-item" href="#"><span class="font-icon glyphicon glyphicon-cog"></span>Settings</a> -->
                                <a class="dropdown-item" href="<?php echo base_url(); ?>doctor/password"><span class="font-icon glyphicon glyphicon-question-sign"></span>Change Password</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url(); ?>doctor/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
                            </div>
                        </div>

                        <button type="button" class="burger-right">
	                        <i class="font-icon-menu-addl"></i>
	                    </button>
                    </div>
                    <!--.site-header-shown-->

                    <div class="mobile-menu-right-overlay"></div>

                    <!--.site-header-collapsed-->
                </div>
                <!--site-header-content-in-->
            </div>
            <!--.site-header-content-->
        </div>
        <!--.container-fluid-->
    </header>
    <!--.site-header-->
    <?php include('sidebar.php'); ?>
