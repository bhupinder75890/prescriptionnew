

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>doctor/prescriptionUpdate/<?php echo $result->prescriptionId; ?>" method="post" enctype="multipart/form-data" class="reset" id="addprescription">


                      <h4 class="modal-title">Edit Prescription</h4>




                      <div class="row">

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Patient's Name<span class="error">*</span></label>
                                  <input type="text" value="<?php echo $result->patientName; ?>" placeholder="Please enter patient name" class="form-control" name="patientName"   id="patientName">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                  <input type="text" value="<?php echo $result->email; ?>" placeholder="Please enter email address" class="form-control" name="email"   id="email">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                  <input type="text" value="<?php echo $result->phone; ?>" placeholder="Please enter phone number" class="form-control" name="phone"   id="phone">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>IC / Passport Number</label>
                                  <input type="text" value="<?php echo $result->ic; ?>" placeholder="Please enter ic / passport number" class="form-control capital" name="ic"   id="ic">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Pharmacy<span class="error">*</span></label>
                                  <select  class="form-control " name="pharmacyId"   id="pharmacyId">
                                    <option value="">Select Pharmacy</option>
                                    <?php if(!empty($pharmacy))
                                     {
                                       foreach($pharmacy as $p)
                                       {
                                       ?>
                                    <option <?php if($result->pharmacyId == $p->userId){ echo "selected"; } ?> value="<?php echo $p->userId; ?>"><?php echo $p->name; ?> / <?php echo $p->phone; ?></option>
                                  <?php } } ?>
                                  </select>

                              </div>
                          </div>





                          <div class="col-md-12">
                            <a class="addmore"><i class="fa fa-plus" aria-hidden="true"></i>Add More</a>
                            <div class="table-responsive table-sec">
                              <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <td width="3%">S. No</td>
                                        <td width="20%">Drug Name</td>
                                        <td>Strength</td>
                                        <td>Dosage</td>
                                        <td>Frequency</td>
                                        <td>Duration</td>
                                        <td>No</td>
                                        <td>Quantity</td>
                                        <td>Price</td>
                                        <td >Subtotal</td>
                                        <td width="15%">Instruction</td>
                                        <td>Action</td>
                                    </tr>
                                  </thead>
                                  <tbody class="maint">
                                    <?php if(!empty($medicine))
                                          {
                                            $i = 1;
                                            foreach($medicine as $m)
                                            {
                                              ?>
                                    <tr class="main main<?php echo $i; ?>">
                                      <td><?php echo $i; ?></td>
                                      <td>
                                     <input value="<?php if(!empty($m->drugName)){ echo $m->drugName; } ?>" type="text" placeholder="search drug name" data-id="<?php echo $i; ?>" required  class="form-control medicines drug drug<?php echo $i; ?>" name="drug[]"  >
                                     <input type="hidden"  class="drugId1" name="drugId[]" value="<?php if(!empty($m->drugId)){ echo $medicine[0]->drugId; } ?>">
                                     <ul class="resultshow<?php echo $i; ?>" style="display:none;">

                                     </ul>
                                   </td>
                                   <td>
                                     <input value="<?php echo $m->strength; ?>" readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength<?php echo $i; ?>">

                                   </td>
                                      <td>
                                        <select  type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose<?php echo $i; ?>">
                                          <?php if(!empty($m->alldoses))
                                            {
                                              foreach($m->alldoses as $a)
                                              { ?>
                                          <option data-no="<?php echo $i; ?>" <?php if($m->drugDoseId == $a->drugDoseId){ echo "selected"; } ?> value="<?php echo $a->drugDoseId; ?>"><?php echo $a->drugDose; ?></option>
                                        <?php } } ?>
                                        </select>
                                        <input type="hidden" value="<?php $m->drugDose; ?>" class="doses<?php echo $i; ?>">

                                      </td>
                                      <td>
                                        <select data-id="<?php echo $i; ?>"  type="text" name="frequency[]"  class="form-control medicines frequency frequency<?php echo $i; ?>">
                                          <option  value="">Select Frequency</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 1){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="1">Daily</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 2){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="2">Bd</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 3){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="3">Tds</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 4){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="4">qds</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 5){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="5">om</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 6){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="6">on</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 7){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="7">5*/day</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 8){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="8">6*/day</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 9){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="9">Alternative day</option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 10){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="10">Monthly </option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 11){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="11">Weekly </option>
                                          <option <?php if(!empty($m->frequency)){ if($m->frequency == 12){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="12">5 day/week</option>
                                        </select>
                                      </td>
                                      <td>
                                        <select   type="text" name="duration[]"  class="form-control medicines duration duration<?php echo $i; ?>">
                                          <option  value="">Select Duration</option>
                                          <option <?php if(!empty($m->duration)){ if($m->duration == 1){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="1">Day</option>
                                          <option <?php if(!empty($m->duration)){ if($m->duration == 2){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="2">Week</option>
                                          <option <?php if(!empty($m->duration)){ if($m->duration == 3){ echo "selected"; } } ?> data-id="<?php echo $i; ?>" value="3">Month</option>

                                        </select>
                                      </td>
                                      <td><input   value="<?php if(!empty($m->no)){ echo $m->no; } ?>" data-id="<?php echo $i; ?>" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no<?php echo $i; ?>"></td>
                                      <td><input  required value="<?php if(!empty($m->quantity)){ echo $m->quantity; } ?>" data-id="<?php echo $i; ?>" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty<?php echo $i; ?>"></td>
                                      <td><input readonly value="<?php if(!empty($m->price)){ echo $m->price; } ?>" required type="text" name="price[]" placeholder="Price" class="form-control medicines price<?php echo $i; ?>"></td>
                                      <td><input readonly value="<?php if(!empty($m->subtotal)){ echo $m->subtotal; } ?>" required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal<?php echo $i; ?>"></td>
                                      <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"><?php if(!empty($medicine[0]->instruction)){ echo $medicine[0]->instruction; } ?></textarea></td>
                                      <td>

                                        <a class="prescriptionremove" data-id="<?php echo $i; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                      
                                      </td>

                                    </tr>
                                  <?php $i++; } } ?>




                                  </tbody>
                                  <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total Amount (RM)</td>
                                    <td><input readonly name="totalamount" value="<?php echo $result->totalamount; ?>" type="text" class="totalamount"></td>
                                  </tr>
                                </table>
                                </div>
                              <!-- </div> -->
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Amount paid by insurance (RM)</label>
                                  <input type="text" value="<?php if($result->copay >= 1 ){ echo $result->copay; } ?>" placeholder="Please enter copay" class="form-control numberonly copay" name="copay"   id="copay">
                              </div>
                          </div>


                                                    <div class="col-sm-6">

                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                          <label>Pay by patient (RM) :<span class="paybypatient"><?php echo $result->payableAmount; ?></span></label>
                                                        </div>
                                                    </div>

                                                                              <div class="col-sm-6">
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Delivery</label>
                            <select class="form-control" name="delivery">
                              <option value="">Select delivery</option>
                              <option <?php if($result->delivery == 1){ echo "selected"; } ?> value="1">Delivery</option>
                              <option <?php if($result->delivery == 2){ echo "selected"; } ?> value="2">Pick up</option>
                            </select>
                          </div>
                          </div>







                      <div class="col-md-12">
                      <button type="submit" class="btn btn-rounded button-disabled" >Update</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>
                    </div>
                  </div>

            </form>

            </div>
        </div>
    </div>


    <!-- Confirm -->
                   <div class="modal fade" id="confirmprescription" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                     <div class="modal-dialog">
                       <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h4 class="modal-title">Confirm</h4>
                         </div>
                         <div class="modal-body">
                           <p>Are you sure you want to Create this  ?</p>
                         </div>
                         <div class="modal-footer">
                           <button type="button"  class="btn btn-Success" id="confirmed">Confirm</button>

                           <button  type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                         </div>
                       </div>
                     </div>
                   </div>
 <!-- confirm -->
<style>

.table.table.table-bordered td:nth-child(7) {
   width: 70px;
}
table.table.table-bordered td:nth-child(11) {
   width: 240px !important;
}
input.totalamount {
   width: 60px !important;
}
input.form-control.medicines.subtotal {
   width: 60px !important;
}
.form-control.medicines.drug {
    width: 120px;
}
</style>



    <!-- Trigger the add referal modal with a button -->
