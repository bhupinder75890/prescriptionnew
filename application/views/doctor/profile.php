

    <div class="page-content" >
        <div class="container-fluid">


            <div class="">

              <form  action="<?php echo base_url(); ?>doctor/profileUpdate/" enctype="multipart/form-data" method="post" id="profile">


                      <h4 class="modal-title">Profile</h4>




                      <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Name<span class="error">*</span></label>
                                <input type="text" value="<?php echo $result->name; ?>" placeholder="Please enter name" class="form-control" name="name"   id="name">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Email<span class="error">*</span></label>
                                <input readonly="readonly" value="<?php echo $result->userEmail; ?>" type="text" placeholder="Please enter name" class="form-control" name="email"   id="email">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Phone Number<span class="error">*</span></label>
                                <input type="text" value="<?php echo $result->phone; ?>" placeholder="Please enter phone number" class="form-control" name="phone"   id="phone">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>License Number<span class="error">*</span></label>
                                <input type="text" value="<?php echo $result->license; ?>" placeholder="Please enter license number" class="form-control" name="license"   id="license">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Profile Image</label>
                                <input type="file" value=""  class="form-control image" name="image"   id="image">
                                <?php if($result->image)
                                {
                                  ?>
                                  <img  src="<?php echo base_url() ?>assets/profile/<?php echo $result->image; ?>" height="100" width="100" class="showimage" >
                                <?php }
                                else
                                { ?>
                                <img style="display:none" src="" class="showimage" >
                              <?php } ?>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group  col-sm-8">
                              <label>Signature<span class="error">*</span></label>
                              <!-- <input id="sample_input" type="hidden" name="test[image]"> -->
                              <input id="Imagecroped " type="file" value="" <?php if($result->signature ==''){ echo "required"; } ?> class="form-control simage" name="signature" > 
                                <?php if($result->signature)
                                {
                                  ?>
                                  <img  src="<?php echo base_url() ?>assets/signature/<?php echo $result->signature; ?>"  width="100" class="signatureimage" >
                                <?php }
                                else
                                { ?>
                                <img style="display:none" src="" class="signatureimage" >
                              <?php } ?>
                            </div>
                        </div>


                      </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Update</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            <!-- image crop -->
                  <div class="modal fade" id="myModal1" role="dialog">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Image Crop</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="cropArea">
                                      <img-crop image="myImage" result-image="myCroppedImage"></img-crop>
                                  </div>
                                  <div>Cropped Image:</div>
                                  <div><img ng-src="{{myCroppedImage}}" class="img-fluid"/></div>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" ng-click="logoupload()" class="btn btn-success">Submit</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- image crop -->

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
