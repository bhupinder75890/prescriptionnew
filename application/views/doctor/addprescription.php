

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>doctor/prescriptionSave" method="post" enctype="multipart/form-data" class="reset" id="addprescription">


                      <h4 class="modal-title">Add Prescription</h4>




                      <div class="row">

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Patient's Name<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter patient name" class="form-control patientName" name="patientName"   id="patientName">
                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter email address" class="form-control" name="email"   id="email">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter phone number" class="form-control" name="phone"   id="phone">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>IC / Passport Number</label>
                                  <input type="text" placeholder="Please enter ic / passport number" class="form-control capital" name="ic"   id="ic">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Pharmacy<span class="error">*</span></label>
                                  <select  class="form-control pharmacychange" name="pharmacyId"   id="pharmacyId">
                                    <option value="">Select Pharmacy</option>
                                    <?php if(!empty($pharmacy))
                                     {
                                       foreach($pharmacy as $p)
                                       {
                                       ?>
                                    <option value="<?php echo $p->userId; ?>"><?php echo $p->name; ?> / <?php echo $p->phone; ?></option>
                                  <?php } } ?>
                                  </select>

                              </div>
                          </div>

                          <div class="col-md-12">
                            <a class="addmore"><i class="fa fa-plus" aria-hidden="true"></i>Add More</a>
                            <div class="table-responsive table-sec">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <td >S. No</td>
                                        <td >Drug Name</td>
                                        <td >Strength</td>
                                        <td >Dosage</td>
                                        <td >Freq</td>
                                        <td >Duration</td>
                                        <td >No</td>
                                        <td >QTY</td>
                                        <td >Price</td>
                                        <td >Subtotal</td>
                                        <td >Instruction for patient</td>
                                        <td ></td>
                                        </tr>
                                    </thead>
                                    <tbody class="maint">
                                      <!-- 1st -->

                                      <tr class="main main1">
                                        <td>1</td>
                                        <td>
                                       <input type="text" placeholder="search drug name" data-id="1" required  class="form-control medicines drug drug1" name="drug[]"  >
                                       <input type="hidden"  class="drugId1" name="drugId[]" value="">
                                       <ul class="resultshow1" style="display:none;">

                                       </ul>
                                     </td>
                                     <td>
                                       <input readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength1">

                                     </td>
                                        <td>
                                          <select   type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose1">
                                          </select>
                                          <input type="hidden" value="" class="doses1">
                                        </td>

                                        <td>
                                          <select data-id="1"  type="text" name="frequency[]"  class="form-control medicines frequency frequency1">
                                            <option  value="">Select Frequency</option>
                                            <option data-id="1" value="1">Daily</option>
                                            <option data-id="1" value="2">Bd</option>
                                            <option data-id="1" value="3">Tds</option>
                                            <option data-id="1" value="4">qds</option>
                                            <option data-id="1" value="5">om</option>
                                            <option data-id="1" value="6">on</option>
                                            <option data-id="1" value="7">5*/day</option>
                                            <option data-id="1" value="8">6*/day</option>
                                            <option data-id="1" value="9">Alternative day</option>
                                            <option data-id="1" value="10">Monthly </option>
                                            <option data-id="1" value="11">Weekly </option>
                                            <option data-id="1" value="12">5 day/week</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select    type="text" name="duration[]"  class="form-control medicines duration duration1">
                                            <option  value="">Select Duration</option>
                                            <option data-id="1" value="1">Day</option>
                                            <option data-id="1" value="2">Week</option>
                                            <option data-id="1" value="3">Month</option>

                                          </select>
                                        </td>
                                        <td><input   data-id="1" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no1"></td>
                                        <td><input   data-id="1" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty1"></td>
                                        <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price1"></td>
                                        <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal1"></td>
                                        <td><textarea  rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>
                                        <td><a class="prescriptionremove" data-id="1"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                      </tr>
                                      <!-- 1st -->
                                      <!-- 2nd -->
                                      <tr class="main main2">
                                        <td>2</td>
                                        <td>
                                       <input type="text" placeholder="search drug name" data-id="2" required  class="form-control medicines drug drug2" name="drug[]"  >
                                       <input type="hidden"  class="drugId2" name="drugId[]" value="">
                                       <ul class="resultshow2" style="display:none;">

                                       </ul>
                                      </td>
                                      <td>
                                       <input readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength2">

                                      </td>
                                        <td>
                                          <select  type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose2">
                                          </select>
                                          <input type="hidden" value="" class="doses2">

                                        </td>

                                        <td>
                                          <select data-id="2"  type="text" name="frequency[]"  class="form-control medicines frequency frequency2">
                                            <option  value="">Select Frequency</option>
                                            <option data-id="2" value="1">Daily</option>
                                            <option data-id="2" value="2">Bd</option>
                                            <option data-id="2" value="3">Tds</option>
                                            <option data-id="2" value="4">qds</option>
                                            <option data-id="2" value="5">om</option>
                                            <option data-id="2" value="6">on</option>
                                            <option data-id="2" value="7">5*/day</option>
                                            <option data-id="2" value="8">6*/day</option>
                                            <option data-id="2" value="9">Alternative day</option>
                                            <option data-id="2" value="10">Monthly </option>
                                            <option data-id="2" value="11">Weekly </option>
                                            <option data-id="2" value="12">5 day/week</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select   type="text" name="duration[]"  class="form-control medicines duration duration2">
                                            <option  value="">Select Duration</option>
                                            <option data-id="2" value="1">Day</option>
                                            <option data-id="2" value="2">Week</option>
                                            <option data-id="2" value="3">Month</option>

                                          </select>
                                        </td>
                                        <td><input   data-id="2" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no2"></td>
                                        <td><input   data-id="2" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty2"></td>
                                        <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price2"></td>
                                        <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal2"></td>
                                        <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>
                                        <td><a class="prescriptionremove" data-id="2"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                      </tr>
                                      <!-- 2nd -->

                                      <!-- 3rd -->
                                      <tr class="main main3">
                                        <td>3</td>
                                        <td>
                                       <input type="text" placeholder="search drug name" data-id="3" required  class="form-control medicines drug drug3" name="drug[]"  >
                                       <input type="hidden"  class="drugId3" name="drugId[]" value="">
                                       <ul class="resultshow3" style="display:none;">

                                       </ul>
                                      </td>
                                      <td>
                                       <input readonly  required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength3">

                                      </td>
                                        <td>
                                          <select  type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose3">
                                          </select>
                                          <input type="hidden" value="" class="doses3">

                                        </td>

                                        <td>
                                          <select data-id="3"  type="text" name="frequency[]"  class="form-control medicines frequency frequency3">
                                            <option  value="">Select Frequency</option>
                                            <option data-id="3" value="1">Daily</option>
                                            <option data-id="3" value="2">Bd</option>
                                            <option data-id="3" value="3">Tds</option>
                                            <option data-id="3" value="4">qds</option>
                                            <option data-id="3" value="5">om</option>
                                            <option data-id="3" value="6">on</option>
                                            <option data-id="3" value="7">5*/day</option>
                                            <option data-id="3" value="8">6*/day</option>
                                            <option data-id="3" value="9">Alternative day</option>
                                            <option data-id="3" value="10">Monthly </option>
                                            <option data-id="3" value="11">Weekly </option>
                                            <option data-id="3" value="12">5 day/week</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select   type="text" name="duration[]"  class="form-control medicines duration duration3">
                                            <option  value="">Select Duration</option>
                                            <option data-id="3" value="1">Day</option>
                                            <option data-id="3" value="2">Week</option>
                                            <option data-id="3" value="3">Month</option>

                                          </select>
                                        </td>
                                        <td><input   data-id="3" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no3"></td>
                                        <td><input   data-id="3" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty3"></td>
                                        <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price3"></td>
                                        <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal3"></td>
                                        <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>
                                        <td><a class="prescriptionremove" data-id="3"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                      </tr>
                                      <!-- 3rd -->
                                      <!-- 4th -->
                                      <tr class="main main4">
                                        <td>4</td>
                                        <td>
                                       <input type="text" placeholder="search drug name" data-id="4" required  class="form-control medicines drug drug4" name="drug[]"  >
                                       <input type="hidden"  class="drugId4" name="drugId[]" value="">
                                       <ul class="resultshow4" style="display:none;">

                                       </ul>
                                      </td>
                                      <td>
                                       <input readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength4">

                                      </td>
                                        <td>
                                          <select  type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose4">
                                          </select>
                                          <input type="hidden" value="" class="doses4">

                                        </td>

                                        <td>
                                          <select data-id="4"  type="text" name="frequency[]"  class="form-control medicines frequency frequency4">
                                            <option  value="">Select Frequency</option>
                                            <option data-id="4" value="1">Daily</option>
                                            <option data-id="4" value="2">Bd</option>
                                            <option data-id="4" value="3">Tds</option>
                                            <option data-id="4" value="4">qds</option>
                                            <option data-id="4" value="5">om</option>
                                            <option data-id="4" value="6">on</option>
                                            <option data-id="4" value="7">5*/day</option>
                                            <option data-id="4" value="8">6*/day</option>
                                            <option data-id="4" value="9">Alternative day</option>
                                            <option data-id="4" value="10">Monthly </option>
                                            <option data-id="4" value="11">Weekly </option>
                                            <option data-id="4" value="12">5 day/week</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select   type="text" name="duration[]"  class="form-control medicines duration duration4">
                                            <option  value="">Select Duration</option>
                                            <option data-id="4" value="1">Day</option>
                                            <option data-id="4" value="2">Week</option>
                                            <option data-id="4" value="3">Month</option>

                                          </select>
                                        </td>
                                        <td><input   data-id="4" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no4"></td>
                                        <td><input   data-id="4" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty4"></td>
                                        <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price4"></td>
                                        <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal4"></td>
                                        <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>
                                        <td><a class="prescriptionremove" data-id="4"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                      </tr>
                                      <!-- 4th -->
                                      <!-- 5th -->
                                      <tr class="main main5">
                                        <td>5</td>
                                        <td>
                                       <input type="text" placeholder="search drug name" data-id="5" required  class="form-control medicines drug drug5" name="drug[]"  >
                                       <input type="hidden"  class="drugId5" name="drugId[]" value="">
                                       <ul class="resultshow5" style="display:none;">

                                       </ul>
                                      </td>
                                      <td>
                                       <input readonly required type="text" name="strength[]" placeholder="Strength" class="form-control medicines strength strength5">

                                      </td>
                                        <td>
                                          <select  type="text" name="dosage[]" placeholder="Dosage" class="form-control medicines dose dose5">
                                          </select>
                                          <input type="hidden" value="" class="doses5">

                                        </td>

                                        <td>
                                          <select data-id="5"  type="text" name="frequency[]"  class="form-control medicines frequency frequency5">
                                            <option  value="">Select Frequency</option>
                                            <option data-id="5" value="1">Daily</option>
                                            <option data-id="5" value="2">Bd</option>
                                            <option data-id="5" value="3">Tds</option>
                                            <option data-id="5" value="4">qds</option>
                                            <option data-id="5" value="5">om</option>
                                            <option data-id="5" value="6">on</option>
                                            <option data-id="5" value="7">5*/day</option>
                                            <option data-id="5" value="8">6*/day</option>
                                            <option data-id="5" value="9">Alternative day</option>
                                            <option data-id="5" value="10">Monthly </option>
                                            <option data-id="5" value="11">Weekly </option>
                                            <option data-id="5" value="12">5 day/week</option>
                                          </select>
                                        </td>
                                        <td>
                                          <select   type="text" name="duration[]"  class="form-control medicines duration duration5">
                                            <option  value="">Select Duration</option>
                                            <option data-id="5" value="1">Day</option>
                                            <option data-id="5" value="2">Week</option>
                                            <option data-id="5" value="3">Month</option>

                                          </select>
                                        </td>
                                        <td><input   data-id="5" type="text" name="no[]" placeholder="No" class="form-control medicines numberonly no no5"></td>
                                        <td><input   data-id="5" type="text" name="qty[]" placeholder="Qty" class="form-control medicines numberonly qty qty5"></td>
                                        <td><input readonly required type="text" name="price[]" placeholder="Price" class="form-control medicines price5"></td>
                                        <td><input readonly required type="text" name="subtotal[]" placeholder="Sub Total" class="form-control medicines subtotal subtotal5"></td>
                                        <td><textarea rows="1" type="text" name="instruction[]" placeholder="Instruction" class="form-control medicines"></textarea></td>
                                        <td><a class="prescriptionremove" data-id="5"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                      </tr>
                                      <!-- 5th -->



                                    </tbody>
                                    <tr>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td></td>
                                      <td>Total:</td>
                                      <td><input readonly name="totalamount" type="text" class="totalamount"></td>
                                    </tr>
                                  </table>
                                </div>
                              <!-- </div> -->
                          </div>
                          <!-- table -->


                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Amount paid by insurance (RM)</label>
                                  <input type="text" placeholder="Please enter insurance amount" class="form-control copay numberonly" name="copay"   id="copay">
                              </div>
                          </div>
                          <div class="col-sm-6">

                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Pay by patient (RM) :<span class="paybypatient"></span></label>
                              </div>
                          </div>
                          <div class="col-sm-6">
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label>Delivery</label>
                            <select class="form-control" name="delivery">
                              <option value="">Select delivery</option>
                              <option value="1">Delivery</option>
                              <option value="2">Pick up</option>
                            </select>
                          </div>
                          </div>



                      <div class="col-md-12">
                      <button type="submit" class="btn btn-rounded button-disabled" >Save</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>
                    </div>
                  </div>

            </form>

            </div>
        </div>
    </div>

    <!-- Confirm -->
                   <div class="modal fade" id="confirmprescription" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                     <div class="modal-dialog">
                       <div class="modal-content">
                         <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                           <h4 class="modal-title">Confirm</h4>
                         </div>
                         <div class="modal-body">
                           <p>Are you sure you want to Create this  ?</p>
                         </div>
                         <div class="modal-footer">
                           <button type="button"  class="btn btn-Success" id="confirmed">Confirm</button>

                           <button  type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                         </div>
                       </div>
                     </div>
                   </div>
 <!-- confirm -->
 <style>
 .table.table.table-bordered td:nth-child(7) {
    width: 50px;
}
table.table.table-bordered td:nth-child(11) {
    width: 300px !important;
}
input.totalamount {
    width: 60px !important;
}
input.form-control.medicines.subtotal {
    width: 60px !important;
}




.qty {

    width: 40px;
}

.strength {

    width: 50px;
}

 .no {

    width: 30px;
}

 .no1 {

    width: 50px;
}

 .price1 {

    width: 50px;
}

 .price2 {

    width: 50px;
}

 .price3 {

    width: 50px;
}

 .price4 {

    width: 50px;
}

 .price5 {

    width: 50px;
}


 .frequency {

    width: 70px;
}


 .duration {

    width: 70px;
}

</style>

    <!-- Trigger the add referal modal with a button -->
