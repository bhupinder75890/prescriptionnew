

    <div class="page-content">
        <div class="container-fluid">
          <!-- search -->

          <div class="refer-btn">
              <a class="btn btn-rounded export" >Export</a>
          </div>


        <div class="linkedsearch-section">
         <div class="row">
         <div class="col-md-2">
           <div class="form-group">
             <select name="perpage" class="prescriptionperpage form-control">
               <option value="">Select perpage</option>
               <option selected value="10">10</option>
               <option value="20">20</option>
               <option value="60">60</option>
               <option value="100">100</option>
             </select>
           </div>
         </div>

          <div class="col-md-4">
           <div class="form-group">
             <input type="text" name="searchtext" placeholder="search by "  class="searchprescription form-control">
           </div>
         </div>

         <div class="col-md-2">
           <div class="form-group">
             <select name="perpage" class="pharmacyId form-control">
               <option value="">Select Pharmacy</option>
               <?php
               if(!empty($pharmacy))
               {
                 foreach($pharmacy as $p)
                 {
               ?>
               <option  value="<?php echo $p->userId; ?>"><?php echo $p->name; ?></option>
             <?php } } ?>
               </select>
           </div>
         </div>

         <!-- <div class="col-md-2">
           <div class="form-group">
             <input type="button" name="search" value="Search" class="searchdoctor btn btn-rounded">
           </div>
         </div> -->

       </div>
     </div>
          <!-- search -->


            <div class="table-responsive table-sec">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 3%">S.No</th>
                            <th style="width: 10%">Date</th>
                            <th style="width: 5%">Order No</th>
                            <th style="width: 10%">Doctor's Name</th>
                            <th style="width: 10%">Patient's Name</th>
                            <th style="width: 5%">Email</th>
                            <th style="width: 5%">Phone</th>
                            <th style="width: 5%">Pharmacy</th>
                            <th style="width: 5%">Amount paid by patient</th>
                            <th style="width: 5%">Amount paid by insurance</th>
                            <th style="width: 5%">Total Amount</th>
                            <th style="width: 5%">Mode of Order</th>

                            <th style="width: 5%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="prescriptiondata">
                      <?php
                       if(!empty($result))
                       {
                         $start = $start + 1;
                         foreach($result as $r)
                         {
                      ?>
                        <tr class="data">
                            <td><?php echo $start++; ?></td>
                            <td><?php echo $date = date("d-m-Y", strtotime($r->date));  ?></td>

                            <td><?php echo $r->orderNo; ?></td>
                            <td><?php echo $r->doctor; ?></td>
                            <td><?php echo $r->patientName; ?></td>
                            <td><?php echo $r->email; ?></td>
                            <td><?php echo $r->phone; ?></td>
                            <td><?php echo $r->pharmacy; ?></td>
                            <td><?php echo $r->payableAmount; ?></td>
                            <td><?php echo $r->copay; ?></td>
                            <td><?php echo $r->totalamount; ?></td>
                            <td><?php if($r->delivery == 1){ echo "Delivery"; } else if($r->delivery == 2){ echo "Self Pickup"; } ?></td>



                            <td>
                             <a target="_blank" href="<?php echo base_url(); ?>admin/convertimage/<?php echo $r->prescriptionId; ?>"><i class="fa fa-download" aria-hidden="true"></i></a>
                             <a data-id="<?php echo $r->prescriptionId; ?>" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a>
                             </td>
                        </tr>
                      <?php } }
                           else
                               {
                                 ?>
                            <tr><td colspan="5">No record</td></tr>
                          <?php } ?>
                    </tbody>
                </table>
                <div class="prescriptionpagination">
                  <?php echo $links; ?>

                </div>
            </div>
        </div>
    </div>




    <!-- View -->
 <div class="modal fade" id="prescriptionmodal" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Prescription View</h4>
       </div>
       <div class="modal-body">
         <div class="bodydata">
         </div>
         <div class="table-responsive table-sec">
             <table class="table table-bordered">
                 <thead>
                     <tr>
                       <td>S. No</td>
                       <td>Drug Name</td>
                       <td>Strength</td>
                       <td>Dosage</td>
                       <td>Duration</td>
                       <td>Frequency</td>
                       <td>No</td>
                       <td>Qty</td>
                       <td>Price</td>
                       <td>Subtotal</td>
                      <td>Instruction</td>
                     </tr>
                 </thead>
                 <tbody class="tableresrow">
                 </tbody>
               </table>
             </div>


       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
 </div>
 <!-- view -->
