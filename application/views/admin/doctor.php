

    <div class="page-content">
        <div class="container-fluid">
          <!-- search -->


          <div class="refer-btn">
              <a class="btn btn-rounded"  href="<?php echo base_url(); ?>admin/doctor/add">Add Doctor</a>
          </div>

        <div class="linkedsearch-section">
         <div class="row">
         <div class="col-md-2">
           <div class="form-group">
             <select name="perpage" class="doctorperpage form-control">
               <option value="">Select perpage</option>
               <option selected value="10">10</option>
               <option value="20">20</option>
               <option value="60">60</option>
               <option value="100">100</option>
             </select>
           </div>
         </div>

         <div class="col-md-4">
           <div class="form-group">
             <input type="text" name="date" placeholder="search by "  class="searchtext form-control">
           </div>
         </div>

         <!-- <div class="col-md-2">
           <div class="form-group">
             <input type="button" name="search" value="Search" class="searchdoctor btn btn-rounded">
           </div>
         </div> -->

       </div>
     </div>
          <!-- search -->


            <div class="table-responsive table-sec">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 5%">S.No</th>
                            <th style="width: 10%">Name</th>
                            <th style="width: 10%">Email</th>
                            <th style="width: 6%">Phone</th>
                            <th style="width: 6%">License</th>
                            <th style="width: 13%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="doctordata">
                      <?php
                       if(!empty($result))
                       {
                         $start = $start + 1;
                         foreach($result as $r)
                         {
                      ?>
                        <tr class="data">
                            <td><?php echo $start++; ?></td>
                            <td><?php echo $r->name; ?></td>
                            <td><?php echo $r->userEmail; ?></td>
                            <td><?php echo $r->phone; ?></td>
                            <td><?php echo $r->license; ?></td>


                            <td>
                             <a href="<?php echo base_url(); ?>admin/doctor/edit/<?php echo $r->userId; ?>"><i class="fa fa-edit"></i></a>
                             <a class="doctordelete" data-id="<?php echo $r->userId; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                             </td>
                        </tr>
                      <?php } }
                           else
                               {
                                 ?>
                            <tr><td colspan="5">No record</td></tr>
                          <?php } ?>
                    </tbody>
                </table>
                <div class="doctorpagination">
                  <?php echo $links; ?>

                </div>
            </div>
        </div>
    </div>

    <!-- View -->
  <div class="modal fade" id="employeeView" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Employee Detail</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- view -->


  <!-- email -->
 <div class="modal fade " id="employeeEmail" role="dialog">
   <div class="modal-dialog">

     <!-- Modal content-->
     <form id="employeeEmailform" class="reset" method="post" action="<?php echo base_url(); ?>admin/patientEmail">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Email</h4>
       </div>
       <div class="modal-body">
         <input type="hidden" class="empId" name="emplyeeId" value="" >
         <input type="hidden" class="type" name="type" value="employee" >
         <div class="form-group">
           <label>Email</label>
           <input type="text" name="email" class="email form-control" placeholder="Please enter email">
         </div>
         <div class="form-group">
           <label>Subject</label>
           <input type="text" name="subject" class="subject form-control" placeholder="Please enter subject">
         </div>
         <div class="form-group">
           <label>Document</label>
           <select style="height:150px" multiple name="document[]" class="document form-control" >

             <?php if(!empty($documents))
                    {
                      foreach($documents as $d)
                      {
                      ?>
             <option value="<?php echo base_url(); ?>assets/<?php echo $d->folder; ?>/<?php echo $d->document; ?>"><?php echo $d->document; ?></option>
           <?php } } ?>
           </select>
         </div>
         <div class="form-group">
           <label>Message</label>
           <textarea type="text" name="message" class="message form-control" placeholder="Please enter message"></textarea>
         </div>
       </div>
       <div class="modal-footer">
         <button type="submit" class="btn btn-success" >Submit</button>
       </div>
     </div>
   </form>

   </div>
 </div>
  <!-- email -->
