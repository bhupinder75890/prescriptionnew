

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>admin/doctorSave" method="post" enctype="multipart/form-data" class="reset" id="adddoctor">


                      <h4 class="modal-title">Add Doctor</h4>




                      <div class="row">

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Name<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter name" class="form-control variety" name="name"   id="name">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter email address" class="form-control variety" name="userEmail"   id="userEmail">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter phone number" class="form-control" name="phone"   id="phone">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>License Number<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter license number" class="form-control " name="license"   id="license">

                              </div>
                          </div>





                          </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Save</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
