

<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
  <ul class="side-menu-list sidebar-menu">


    <li <?php if($this->uri->segment(2) =="dashboard") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>admin/dashboard">
        <span><i class="fa fa-tachometer"></i><span class="lbl">Dashboard</span></span>
      </a>
   </li>

    <li <?php if($this->uri->segment(2) =="doctorlist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>admin/doctorlist">
        <span><i class="fa fa-list"></i><span class="lbl">Doctor</span></span>
      </a>
   </li>

    <li <?php if($this->uri->segment(2) =="pharmacylist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>admin/pharmacylist">
        <span><i class="fa fa-list"></i><span class="lbl">Pharmacy</span></span>
      </a>
   </li>
    <li <?php if($this->uri->segment(2) =="prescriptionlist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>admin/prescriptionlist">
        <span><i class="fa fa-list"></i><span class="lbl">Prescription</span></span>
      </a>
   </li>









  </ul>

</nav>
