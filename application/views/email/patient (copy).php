<table border="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fff; box-shadow:0 0 10px #ccc;" width="600">
				<tbody>
					<tr>
						<td align="center" height="100" width="100" style="background:#c3c8ca;border-bottom:solid 1px #f5f5f5;" valign="middle">
              <img  src="<?php echo base_url(); ?>assets/admin/img/dt-logo.png" alt="">
            </td>
					</tr>
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td width="15">&nbsp;</td>
									<td width="670">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>
												<td height="15">&nbsp;</td>
											</tr>
											<tr>
												<td width="530">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px;">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody>

																	<tr>
                                    <td>
																		<center><h4>Medication List</h4></center>
																			<br>
																			<br>

																			 Hi <?php echo $name; ?>,
																			 <br/>
																			<p><b>Patient Name :</b><?php echo $result->patientName; ?> </p>
																			<p><b>Email :</b><?php echo $result->email; ?> </p>
																			<p><b>Phone :</b><?php echo $result->phone; ?> </p>
																			<p><b>Ic :</b><?php echo $result->ic; ?> </p>
																			<p><b>Date :</b><?php echo $date = date("d-m-Y", strtotime($result->date));  ?> </p>
                                        <p><b>Doctor :</b><?php echo $result->doctor;  ?> </p>
																				<br>
																				<br>
																				<br>
																				<?php if(!empty($medicines))
																				{
																					?>
																			<table border="1" width="100%" style=" border-collapse: collapse;text-align: center;">
																				<tr>
																				<th>S. NO</th>
																				<th>Drug</th>
																				<th>Dosage</th>
																				<th>Frequency</th>
																				<th>Duration</th>
																				<th>Instruction</th>

																			</tr>
																			<?php
																			$i = 1;
																			foreach($medicines as $m)
																			 {
																				 ?>
																			<tr>
																				<td><?php echo $i; ?></td>
																				<td><?php echo $m->drug; ?></td>
																				<td><?php echo $m->dosage; ?></td>
																				<td><?php echo $m->frequency; ?></td>
																				<td><?php echo $m->duration; ?></td>
																				<td><?php echo $m->instruction; ?></td>
																			</tr>
																		<?php $i++; } ?>
																			</table>
																		<?php } ?>


																			 <br/><b>Pharmacy Name :</b><?php echo $result->pharmacy; ?><br/>
																			 <br/><b>Pharmacy Phone :</b><?php echo $result->phphone; ?><br/>

																			 <p><b>Date :</b><?php echo $date = date("d-m-Y", strtotime($result->date));  ?> </p>
																										 <p><b>Electonically signed by :</b><?php if($result->signature){ ?><img src="<?php echo base_url(); ?>assets/signature/<?php echo $result->signature; ?>" width="100" ><?php } ?> </p>
																			 <p><b>Medical license :</b> <?php echo $result->license; ?> </p>
																			 <p>DRT Global Sd Bhd (1304866-k) </p>
																			 <p>2G-23-1b,Medan Rajowali, Salaria Square ,11900, Bayan Lepas, Penang , Malaysia </p>
                                    </td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<!-- <tr>
												<td height="15" style="padding-bottom:15px;">
												<p>*This email account is not monitored. Please do not reply to this email as we will not be able to read and respond to your messages.</p>
												</td>
											</tr> -->
										</tbody>
									</table>
									</td>
									<td width="15">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding:5px; background: none repeat scroll 0 0 #333;
             border-top: 1px solid #CCCCCC;color:#fff;" valign="top">
						<!-- <p>You have received this message by auto generated e-mail.</p> -->

						<center><?php echo '&copy; '.date('Y'). ' ALL RIGHTS RESERVED'; ?></center>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
