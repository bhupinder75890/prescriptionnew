<table border="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fff; box-shadow:0 0 10px #ccc;" width="600">
				<tbody>
					<tr>
						<td align="center"  width="100" style="background:#fff;border-bottom:solid 1px #f5f5f5;" valign="middle">
              <img  src="<?php echo base_url(); ?>assets/admin/img/doctortouchrx-email.jpg" alt="">
            </td>
					</tr>
					<tr>
						<td>
						<table cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td width="15">&nbsp;</td>
									<td width="670">
									<table cellpadding="0" cellspacing="0" width="100%">
										<tbody>
											<tr>
												<td height="15">&nbsp;</td>
											</tr>
											<tr>
												<td width="530">
												<table cellpadding="0" cellspacing="0" width="100%">
													<tbody>
														<tr>
															<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px;">
															<table border="0" cellpadding="0" cellspacing="0" width="100%">
																<tbody>

																	<tr>
                                    <td>

																			 Hi <?php echo $name; ?>,
																			 <br/>
																			<p>An electronic prescription has been sent by <b><?php echo $result->doctor; ?></b> to <b><?php echo $result->pharmacy; ?> </b>at <?php echo $result->paddress; ?>. A pharmacy representative will contact you shortly. If you have any questions, please feel free to contact the pharmacy at <b><?php echo $result->phphone; ?>.</b></p>
																			<?php if($result->payableAmount > 0)
																			{
																				?>
																			<a href="<?php echo base_url(); ?>patient/payment/<?php echo $Id; ?>">Click Link and Pay</a>
																		<?php } ?>
																		<br/>
																		<br/>
																		<p>Sincerely,</p>
                                    <p>DoctorTouch</p>
                                    </td>
																	</tr>
																</tbody>
															</table>
															</td>
														</tr>
													</tbody>
												</table>
												</td>
											</tr>
											<!-- <tr>
												<td height="15" style="padding-bottom:15px;">
												<p>*This email account is not monitored. Please do not reply to this email as we will not be able to read and respond to your messages.</p>
												</td>
											</tr> -->
										</tbody>
									</table>
									</td>
									<td width="15">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" style="padding:5px; background: none repeat scroll 0 0 #333;
             border-top: 1px solid #CCCCCC;color:#fff;" valign="top">
						<!-- <p>You have received this message by auto generated e-mail.</p> -->

						<center><?php echo '&copy; '.date('Y'). ' ALL RIGHTS RESERVED'; ?></center>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
