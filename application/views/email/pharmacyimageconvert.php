
<!DOCTYPE html>
<html>

<head>
    <title></title>
    <link rel="stylesheet" href=
"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src=
"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
    </script>
    <script src=
"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js">
    </script>
    <script src=
"https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js">
    </script>
    <!-- <style>
        .top {
            margin-top: 20px;
        }

        h1 {
            color: green;
        }

        input {
            background-color: transparent;
            border: 0px solid;
            width: 300;
        }

        body {
            text-align: center;
        } -->
    <!-- </style> -->
</head>

<body>


    <table id="createImg" border="0" width="100%">
    	<tbody>
    		<tr>
    			<td>
    			<table align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fff; box-shadow:0 0 10px #ccc;" width="1080">
    				<tbody>
    					<tr>
    						<td align="center" height="100" width="100" style="background:white;border-bottom:solid 1px #f5f5f5;" valign="middle">
                <img  src="<?php echo base_url(); ?>assets/admin/img/doctortouchrx-email.jpg" alt="">
                </td>
    					</tr>
    					<tr style="background-image: url('<?php echo base_url(); ?>assets/admin/img/logo-transparent-new3.png');">
    						<td>
    						<table cellpadding="0" cellspacing="0" width="100%">
    							<tbody>
    								<tr>
    									<td width="15">&nbsp;</td>
    									<td width="670">
    									<table cellpadding="0" cellspacing="0" width="100%">
    										<tbody>
    											<tr>
    												<td height="15">&nbsp;</td>
    											</tr>
    											<tr>
    												<td width="530">
    												<table cellpadding="0" cellspacing="0" width="100%">
    													<tbody>
    														<tr>
    															<td style="font-family:Arial, Helvetica, sans-serif; padding:10px; font-size:13px;">
    															<table border="0" cellpadding="0" cellspacing="0" width="100%">
    																<tbody>

    																	<tr>
                                        <td>
    																		<center><h4>Medication List</h4></center>
    																			<br>
    																			<br>
                                          <p><b>Date :</b><?php echo $date = date("d-m-Y", strtotime($result->date));  ?> </p>
                                             <p><b>Order No :</b><?php echo $result->orderNo;  ?> </p>
    																			<p><b>Patient's Name :</b><?php echo $result->patientName; ?> </p>
    																			<p><b>Email :</b><?php echo $result->email; ?> </p>
    																			<p><b>Phone :</b><?php echo $result->phone; ?> </p>
    																			<p><b>IC/Passport Number :</b><?php echo $result->ic; ?> </p>
                                            <p><b>Doctor's Name :</b><?php echo $result->doctor;  ?> </p>
    																				<br>
    																				<br>
    																				<br>
                                            <?php if(!empty($medicines))
    																				{
    																					?>
    																			<table border="1" width="100%" style="border-collapse: collapse; padding-left:10px";>
                                            <tr>
                                              <th width="50">S. NO</th>
                                              <th style="padding-left:10px;">Drug</th>
                                              <th style="padding-left:10px;">Strength</th>
                                              <th style="padding-left:10px;">Dosage</th>
                                              <th style="padding-left:10px;">Frequency</th>
                                              <th style="padding-left:10px;">Duration</th>
                                              <th style="padding-left:10px;">No</th>
                                              <th style="padding-left:10px;">Quantity</th>
                                              <th style="padding-left:10px;">Price</th>
                                              <th style="padding-left:10px;">Subtotal</th>
                                              <th style="padding-left:10px;">Instruction</th>

                                          </tr>
                                          <?php
                                          $i = 1;
                                          foreach($medicines as $m)
                                           {
                                             ?>
                                             <tr>
                                               <td style="padding-left:10px;"><?php echo $i; ?></td>
                                                 <td style="padding-lfft:10px;"> <?php echo $m->drugName; ?> </td>
                                                 <td style="padding-left:10px;"><?php echo $m->strength; ?></td>
                                                 <td style="padding-left:10px;"><?php echo $m->drugDose; ?></td>
                                                 <td style="padding-left:10px;"><?php
                                                 if($m->frequency == 1)
                                                 {
                                                   echo "Daily";
                                                 }
                                                 else if($m->frequency == 2)
                                                 {
                                                     echo "Bd";
                                                 }
                                                 else if($m->frequency == 3)
                                                 {
                                                     echo "Tds";
                                                 }
                                                 else if($m->frequency == 4)
                                                 {
                                                   echo "Qds";
                                                 }
                                                 else if($m->frequency == 5)
                                                 {
                                                   echo "Om";
                                                 }
                                                 else if($m->frequency == 6)
                                                 {
                                                   echo "On";
                                                 }
                                                 else if($m->frequency == 7)
                                                 {
                                                   echo "5 day";
                                                 }
                                                 else if($m->frequency == 8)
                                                 {
                                                   echo "6 day";
                                                 }
                                                 else if($m->frequency == 9)
                                                 {
                                                   echo "Alternative";
                                                 }
                                                 else if($m->frequency == 10)
                                                 {
                                                     echo "Monthly";
                                                 }
                                                 else if($m->frequency == 10)
                                                 {
                                                     echo "<td>Weekly";
                                                 }
                                                 else if($m->frequency == 10)
                                                 {
                                                   echo "5 day/week";
                                                 }
                                                 ?></td>
                                                 <td style="padding-left:10px;"><?php if($m->duration == 1){ echo "Day";} else if($m->duration == 2){ echo "Week"; } else if($m->duration == 3){ "Month"; } ?></td>
                                                  <td style="padding-left:10px;"><?php echo $m->no; ?></td>
                                                  <td style="padding-left:10px;"><?php echo $m->quantity; ?></td>
                                                 <td style="padding-left:10px;"><?php echo $m->price; ?></td>
                                                 <td style="padding-left:10px;"><?php echo $m->subtotal; ?></td>
                                                 <td style="padding-left:10px;"><?php echo $m->instruction; ?></td>
                                             </tr>
    																		<?php $i++; } ?>
    																			</table>
    																		<?php } ?>


    																			 <br/><b>Total Prescription Amount :</b> <?php echo $result->totalamount; ?><br/>
    																			 <br/><b>Amount paid by insurance :</b> <?php echo $result->copay; ?><br/>
                                           <?php if($result->deliveryCharges != 0)
                                           {?>
                                          <br/><b>Delivery Charges :</b>RM <?php echo $result->deliveryCharges; ?><br/>
                                         <?php } ?>
    																			 <br/><b>Amount paid by patient :</b><?php echo $result->payableAmount; ?><br/>
    																			 <br/><b>Pharmacy Name :</b><?php echo $result->pharmacy; ?><br/>

                                           <br/><b>Pharmacy Phone :</b><?php echo $result->phphone; ?><br/>

                                           <br/><b>Pharmacy Address :</b><?php echo $result->paddress; ?><br/><br/>

                                          <p><b>Electonically Signed By :</b><?php if($result->signature){ ?><img src="<?php echo base_url(); ?>assets/signature/<?php echo $result->signature; ?>" width="100" ><?php } ?> </p>

                                         <br/>  <p><b>Medical License :</b> <?php echo $result->license; ?> </p>
    																			 <p>DRT Global Sd Bhd (1304866-k) </p>
    																			 <p>2G-23-1b,Medan Rajowali, Salaria Square ,11900, Bayan Lepas, Penang , Malaysia </p>
                                        </td>
    																	</tr>
    																</tbody>
    															</table>
    															</td>
    														</tr>
    													</tbody>
    												</table>
    												</td>
    											</tr>
    											<!-- <tr>
    												<td height="15" style="padding-bottom:15px;">
    												<p>*This email account is not monitored. Please do not reply to this email as we will not be able to read and respond to your messages.</p>
    												</td>
    											</tr> -->
    										</tbody>
    									</table>
    									</td>
    									<td width="15">&nbsp;</td>
    								</tr>
    							</tbody>
    						</table>
    						</td>
    					</tr>
    					<tr>
    						<td align="center" style="padding:5px; background: none repeat scroll 0 0 #333;
                 border-top: 1px solid #CCCCCC;color:#fff;" valign="top">
    						<!-- <p>You have received this message by auto generated e-mail.</p> -->

    						<center><?php echo '&copy; '.date('Y'). ' ALL RIGHTS RESERVED'; ?></center>
    						</td>
    					</tr>
    				</tbody>
    			</table>
    			</td>
    		</tr>
    	</tbody>
    </table>


    <script>
        $(function() {
            // $("#geeks").click(function() {
                html2canvas($("#createImg"), {
                    onrendered: function(canvas) {
                        var imgsrc = canvas.toDataURL("image/png");
                        //console.log(imgsrc);
                        $("#newimg").attr('src', imgsrc);
                        $("#img").show();
                        var dataURL = canvas.toDataURL();
                        $.ajax({
                            type: "POST",
                            url: '<?php echo base_url(); ?>pharmacy1/imageupload1',
                            data: {
                                imgBase64: dataURL,id:<?php echo $id; ?>
                            }
                        }).done(function(o)
                        {
                          //console.log('saved');
                         //  var link = document.createElement('a');
                         //  link.href = '<?php echo base_url(); ?>'+o;
                         // link.download = 'Download.png';
                         // document.body.appendChild(link);
                         // link.click();
                         // document.body.removeChild(link);
     setTimeout(function() { window.location.href= '<?php echo base_url(); ?>pharmacy1/prescriptionlist';},500);


                        });
                    }
                });
            // });
        });

    </script>
</body>

</html>
