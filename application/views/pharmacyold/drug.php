

    <div class="page-content">
        <div class="container-fluid">
          <!-- search -->


          <div class="refer-btn">
              <a class="btn btn-rounded"  href="<?php echo base_url(); ?>pharmacy/drug/add">Add Drug</a>
          </div>

        <div class="linkedsearch-section">
         <div class="row">
         <div class="col-md-2">
           <div class="form-group">
             <select name="perpage" class="drugperpage form-control">
               <option value="">Select perpage</option>
               <option selected value="10">10</option>
               <option value="20">20</option>
               <option value="60">60</option>
               <option value="100">100</option>
             </select>
           </div>
         </div>

          <div class="col-md-4">
           <div class="form-group">
             <input type="text" name="searchtext" placeholder="search by "  class="searchdrug form-control">
           </div>
         </div>

         <!-- <div class="col-md-2">
           <div class="form-group">
             <input type="button" name="search" value="Search" class="searchdoctor btn btn-rounded">
           </div>
         </div> -->

       </div>
     </div>
          <!-- search -->


            <div class="table-responsive table-sec">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 3%">S.No</th>
                            <th style="width: 10%">Drug Name</th>
                            <th style="width: 8%">Dose form</th>
                            <th style="width: 10%">Strength</th>
                            <th style="width: 10%">Dose</th>
                            <th style="width: 5%">Selling Price</th>
                            <th style="width: 5%">Buying Price</th>
                            <th style="width: 10%">Expiry Date</th>
                            <th style="width: 5%">Action</th>
                        </tr>
                    </thead>
                    <tbody class="drugdata">
                      <?php
                       if(!empty($result))
                       {
                         $start = $start + 1;
                         foreach($result as $r)
                         {
                      ?>
                        <tr class="data">
                            <td><?php echo $start++; ?></td>
                            <td><?php echo $r->drugName; ?></td>
                            <td><?php
                              if($r->drugType == 1)
                              {
                                echo "Unit";
                               }
                              else if($r->drugType == 2)
                              {
                                echo "Tablet";
                              }
                              else if($r->drugType == 3)
                              {
                                echo "Bottle";
                              }
                              else if($r->drugType == 3)
                              {
                                echo "Bottle";
                              }
                              else if($r->drugType == 4)
                              {
                                echo "Injection";
                              }
                              else if($r->drugType == 5)
                              {
                                echo "Syrup";
                              }
                               ?></td>
                               <td><?php echo $r->drugStrength; ?></td>

                            <td><?php if(!empty($r->dose))
                            {
                              foreach($r->dose as $key=>$d)
                              {
                              echo $d->drugDose;
                              if($key!==count($r->dose)-1){echo ',';}
                              }
                            } ?>
                            </td>

                            <td><?php echo $r->drugSalePrice; ?></td>
                            <td><?php echo $r->drugBuyPrice; ?></td>

                            <td><?php if($r->drugExpiryDate){ echo $date = date("d-m-Y", strtotime($r->drugExpiryDate)); }  ?></td>




                            <td>
                             <a href="<?php echo base_url(); ?>pharmacy/drug/edit/<?php echo $r->drugId; ?>"><i class="fa fa-edit"></i></a>
                             <a class="drugdelete" data-id="<?php echo $r->drugId; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                             </td>
                        </tr>
                      <?php } }
                           else
                               {
                                 ?>
                            <tr><td colspan="8">No record</td></tr>
                          <?php } ?>
                    </tbody>
                </table>
                <div class="drugpagination">
                  <?php echo $links; ?>

                </div>
            </div>
        </div>
    </div>
