

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>pharmacy1/drugUpdate/<?php echo $result->drugId; ?>" method="post" enctype="multipart/form-data" class="reset" id="adddrug">


                      <h4 class="modal-title">Edit Inventroy</h4>




                      <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                              <label>Drug Name<span class="error">*</span></label>
                                <input type="text" value="<?php echo $result->drugName; ?>" placeholder="Please enter drug name" class="form-control drugname" name="drugName"   id="drugName">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label>Strength<span class="error">*</span></label>
                                <input value="<?php echo $result->drugStrength; ?>" type="text" placeholder="Please enter Strength" class="form-control drugStrength" name="drugStrength"   id="drugStrength">
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="adddose">
                            <?php if(!empty($dose))
                             {
                               $i = 1;
                               foreach($dose as $d)
                               {
                               ?>
                            <div class="row maindose maindose<?php echo $i; ?>">
                        <div class="col-sm-6">
                            <div class="form-group">
                              <label>Dose<span class="error">*</span></label>
                                <input value="<?php echo $d->drugDose; ?>" type="text" placeholder="Please enter dose" class="form-control drugDose" name="drugDose[]"   id="drugDose">

                            </div>

                          </div>
                          <div class="col-sm-2">
                            <?php if($i == 1)
                            {
                              ?>
                          <a class="addmore"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <?php }
                          else
                          {
                            ?>
                            <a data-id="<?php echo $i; ?>" class="removedose"><i class="fa fa-trash" aria-hidden="true"></i></a>
                          <?php } ?>
                        </div>
                        </div>
                      <?php $i++; } } ?>
                        </div>


                      </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Dose form<span class="error">*</span></label>
                                  <select type="text"  class="form-control" name="drugType"   id="drugType">
                                    <option value="">Select Item Type</option>
                                    <option <?php if($result->drugType == 1){ echo "selected"; } ?> value="1">Unit</option>
                                    <option <?php if($result->drugType == 2){ echo "selected"; } ?> value="2">Tablet</option>
                                    <option <?php if($result->drugType == 3){ echo "selected"; } ?> value="3">Bottle</option>
                                    <option <?php if($result->drugType == 4){ echo "selected"; } ?> value="4">Injection</option>
                                    <option <?php if($result->drugType == 5){ echo "selected"; } ?> value="5">Syrup</option>
                                  </select>

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Selling Price<span class="error">*</span></label>
                                  <input value="<?php echo $result->drugSalePrice; ?>" type="text" placeholder="Please enter sale price" class="form-control numberdecimalonly" name="drugSalePrice"   id="drugSalePrice">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Buying Price</label>
                                  <input value="<?php echo $result->drugBuyPrice; ?>" type="text" placeholder="Please enter buy price" class="form-control numberdecimalonly" name="drugBuyPrice"   id="drugBuyPrice">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Expiry Date</label>
                                  <input value="<?php echo $result->drugExpiryDate; ?>" readonly type="text"  placeholder="Please select expiry date" class="form-control datepicker" name="drugExpiryDate"   id="drugExpiryDate">

                              </div>
                          </div>





                          </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Update</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
