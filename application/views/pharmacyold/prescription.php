

    <div class="page-content">
        <div class="container-fluid">
          <!-- search -->

<!--
          <div class="refer-btn">
              <a class="btn btn-rounded"  href="<?php echo base_url(); ?>doctor/prescription/add">Add Prescription</a>
          </div> -->

        <div class="linkedsearch-section">
         <div class="row">
         <div class="col-md-2">
           <div class="form-group">
             <select name="perpage" class="prescriptionperpage form-control">
               <option value="">Select perpage</option>
               <option selected value="10">10</option>
               <option value="20">20</option>
               <option value="60">60</option>
               <option value="100">100</option>
             </select>
           </div>
         </div>

          <div class="col-md-4">
           <div class="form-group">
             <input type="text" name="searchtext" placeholder="search by "  class="searchprescription form-control">
           </div>
         </div>

         <!-- <div class="col-md-2">
           <div class="form-group">
             <input type="button" name="search" value="Search" class="searchdoctor btn btn-rounded">
           </div>
         </div> -->

       </div>
     </div>
          <!-- search -->


            <div class="table-responsive table-sec">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 3%">S.No</th>
                            <th style="width: 8%">Date</th>
                            <th style="width: 8%">Order No</th>
                            <th style="width: 8%">Doctor's Name</th>
                            <th style="width: 8%">Patient's Name</th>
                            <th style="width: 5%">Phone</th>
                            <th style="width: 5%">Amount paid by patient</th>
                            <th style="width: 5%">Amount paid by insurance</th>
                            <th style="width: 5%">Total Amount</th>
                            <th style="width: 3%">View</th>
                            <th style="width: 10%">Status</th>
                            <th style="width: 10%">Delivery</th>
                        </tr>
                    </thead>
                    <tbody class="prescriptiondata">
                      <?php
                       if(!empty($result))
                       {
                         $start = $start + 1;
                         foreach($result as $r)
                         {
                      ?>
                        <tr class="data">
                            <td><?php echo $start++; ?></td>
                            <td><?php echo $date = date("d-m-Y", strtotime($r->date));  ?></td>
                            <td><?php echo $r->orderNo;  ?></td>
                            <td><?php echo $r->name; ?></td>
                            <td><?php echo $r->patientName; ?></td>
                            <!-- <td><?php //echo $r->email; ?></td> -->
                            <td><?php echo $r->phone; ?></td>
                            <td><?php echo $r->payableAmount; ?></td>
                            <td><?php echo $r->copay; ?></td>
                            <td><?php echo $r->totalamount; ?></td>
                            <td><a data-id="<?php echo $r->prescriptionId; ?>" class="viewprescription"><i class="fa fa-eye" aria-hidden="true"></i></a></td>

                            <td>
                              <select class="prescriptionStatus" name="status">
                                <option  value="">Select Status</option>
                                <option <?php if($r->status == 1){ echo "selected"; } ?> data-id="<?php echo $r->prescriptionId; ?>" value="1">Accept Order</option>
                                <option <?php if($r->status == 2){ echo "selected"; } ?> data-id="<?php echo $r->prescriptionId; ?>" value="2">Cancel</option>
                              </select>

                             </td>
                             <td><?php if($r->delivery == 1){ echo "Delivery"; } else if($r->delivery == 2){ echo "Self Pickup"; } ?>
                               <?php if($r->delivery == 1)
                               {
                                 ?>
                                 <select  class="deliveryCost"  >
                                   <option value="">Select Cost</option>
                                   <option data-id="<?php echo $r->prescriptionId; ?>" <?php if($r->deliveryCharges == 10){ echo "selected"; } ?> value="10">RM10</option>
                                   <option data-id="<?php echo $r->prescriptionId; ?>" <?php if($r->deliveryCharges == 20){ echo "selected"; } ?> value="20">RM20</option>
                                   <option data-id="<?php echo $r->prescriptionId; ?>" <?php if($r->deliveryCharges == 25){ echo "selected"; } ?> value="25">RM25</option>
                                   <option data-id="<?php echo $r->prescriptionId; ?>" <?php if($r->deliveryCharges == 30){ echo "selected"; } ?> value="30">RM30 </option>
                                   <option data-id="<?php echo $r->prescriptionId; ?>" <?php if($r->deliveryCharges == 40){ echo "selected"; } ?> value="40">RM40 </option>
                                 </select>
                                 <?php
                               } ?>
                             </td>
                        </tr>
                      <?php } }
                           else
                               {
                                 ?>
                            <tr><td colspan="8">No record</td></tr>
                          <?php } ?>
                    </tbody>
                </table>
                <div class="prescriptionpagination">
                  <?php echo $links; ?>

                </div>
            </div>
        </div>
    </div>




    <!-- View -->
 <div class="modal fade" id="prescriptionmodal" role="dialog">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title">Prescription View</h4>
       </div>
       <div class="modal-body">
         <div class="bodydata">
         </div>
         <div class="table-responsive table-sec">
             <table class="table table-bordered">
                 <thead>
                     <tr>
                       <td>S. No</td>
                       <td>Drug Name</td>
                       <td>Strength</td>
                       <td>Dosage</td>
                       <td>Duration</td>
                       <td>Frequency</td>
                       <td>No</td>
                       <td>Qty</td>
                       <td>Price</td>
                       <td>Subtotal</td>
                      <td>Instruction</td>
                     </tr>
                 </thead>
                 <tbody class="tableresrow">
                 </tbody>
               </table>
             </div>


       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       </div>
     </div>

   </div>
 </div>
 <!-- view -->

 <!-- Confirm -->
                <div class="modal fade" id="confirmprescription" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Confirm</h4>
                      </div>
                      <div class="modal-body">
                        <p style="display:none" class="approved1">Are you sure you want to Proceed with Payment  ?</p>
                        <p style="display:none" class="approved2">Are you sure you want to Cancel the order  ?</p>
                        <input type="hidden" value="" name="id" class="confirmprescriptionId">
                        <input type="hidden" value="" name="status" class="confirmprescriptionStatus">
                      </div>
                      <div class="modal-footer">
                        <button type="button"  class="btn btn-Success" id="confirmed">Confirm</button>

                        <button  type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                      </div>
                    </div>
                  </div>
                </div>
<!-- confirm -->
