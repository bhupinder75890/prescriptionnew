

<div class="mobile-menu-left-overlay"></div>
<nav class="side-menu">
  <ul class="side-menu-list sidebar-menu">


    <li <?php if($this->uri->segment(2) =="dashboard") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/dashboard/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-tachometer"></i><span class="lbl">Dashboard</span></span>
      </a>
   </li>

    <li <?php if($this->uri->segment(2) =="profile") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/profile/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-list"></i><span class="lbl">Profile</span></span>
      </a>
   </li>
    <li <?php if($this->uri->segment(2) =="druglist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/druglist/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-list"></i><span class="lbl">Inventory</span></span>
      </a>
   </li>

    <li <?php if($this->uri->segment(2) =="prescriptionlist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/prescriptionlist/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-list"></i><span class="lbl">Prescription Order</span></span>
      </a>
   </li>

    <li <?php if($this->uri->segment(2) =="reportlist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/reportlist/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-list"></i><span class="lbl">Report</span></span>
      </a>
   </li>
    <li <?php if($this->uri->segment(2) =="pageslist") { ?> class="active" <?php } ?>>
      <a href="<?php echo base_url(); ?>pharmacy/pageslist/<?php echo $this->session->userdata['doctorloggedin']['url']; ?>">
        <span><i class="fa fa-list"></i><span class="lbl">Pages</span></span>
      </a>
   </li>
  </ul>

</nav>
