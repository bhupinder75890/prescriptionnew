

    <div class="page-content">
        <div class="container-fluid">


            <div class="">

              <form action="<?php echo base_url(); ?>pharmacy1/drugSave" method="post" enctype="multipart/form-data" class="reset" id="adddrug">


                      <h4 class="modal-title">Add Inventory</h4>




                      <div class="row">

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Drug Name<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter drug name" class="form-control drugname" name="drugName"   id="drugName">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Strength<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter Strength" class="form-control drugStrength" name="drugStrength"   id="drugStrength">
                              </div>
                          </div>
                          <div class="col-md-12">
                            <div class="adddose">
                              <div class="row maindose">
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Dose<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter dose" class="form-control drugDose" name="drugDose[]"   id="drugDose">

                              </div>

                            </div>
                            <div class="col-sm-2">
                            <a class="addmore"><i class="fa fa-plus" aria-hidden="true"></i></a>
                          </div>
                          </div>
                          </div>


                        </div>


                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Dose form<span class="error">*</span></label>
                                  <select type="text"  class="form-control" name="drugType"   id="drugType">
                                    <option value="">Select Item Type</option>
                                    <option value="1">Unit</option>
                                    <option value="2">Tablet</option>
                                    <option value="3">Bottle</option>
                                    <option value="4">Injection</option>
                                    <option value="5">Syrup</option>
                                  </select>

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Selling Price<span class="error">*</span></label>
                                  <input type="text" placeholder="Please enter selling price" class="form-control numberdecimalonly" name="drugSalePrice"   id="drugSalePrice">

                              </div>
                          </div>
                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Buying Price</label>
                                  <input type="text" placeholder="Please enter buying price" class="form-control numberdecimalonly" name="drugBuyPrice"   id="drugBuyPrice">

                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <label>Expiry Date</label>
                                  <input readonly type="text"  placeholder="Please select expiry date" class="form-control datepicker" name="drugExpiryDate"   id="drugExpiryDate">

                              </div>
                          </div>





                          </div>
                      <button type="submit" class="btn btn-rounded button-disabled" >Save</button>
                      <button type="button" class="cancel btn btn-rounded btn-default" >Cancel</button>

            </form>

            </div>
        </div>
    </div>


    <!-- Trigger the add referal modal with a button -->
