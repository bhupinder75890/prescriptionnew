<!DOCTYPE html>
<html>

<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Admin Login</title>

	<link href="img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
	<link href="img/favicon.html" rel="icon" type="image/png">
	<link href="img/favicon-2.html" rel="shortcut icon">


<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/separate/pages/login.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.toast.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/loginpage.css">

</head>
<body>
	<div class="loader_panel" style="display: none;">
		<img src="<?php echo base_url(); ?>assets/image/preloader.gif">
	</div>




				<div class="login_panel">
			    	<div class="login_bx">
			        	<div class="login_logo">
									<img src="<?php echo base_url(); ?>assets/image/logo.png" alt="">

			            </div>
			        	<h3>New Password</h3>

							 <form id="loginform" class="reset"  method="post" action="<?php echo base_url(); ?>admin">
								 <div class="form-group">
										 <input type="password" id="password" name="password" class="form-control" placeholder="Please Enter New Password"/>
										 <input type="hidden" value="<?php echo $email; ?>" id="email" name="email"  />
								 </div>
										 <div class="form-group text-right">
											<a href="<?php echo base_url(); ?>admin">Sign in ?</a>
										 </div>
										 <button type="submit" class="btn btn-primary">
												 Login
										 </button>
							 </form>
			    <div>

			    </div>
			  </div>
			        </div>


<script src="<?php echo base_url(); ?>assets/admin/js/lib/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/popper/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/tether/tether.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/lib/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/js/plugins.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.toast.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/lib/match-height/jquery.matchHeight.min.js"></script>
    <script>
        $(function() {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function(){
                setTimeout(function(){
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                },100);
            });
        });
    </script>
<script src="<?php echo base_url(); ?>assets/admin/js/app.js"></script>
</body>

</html>
