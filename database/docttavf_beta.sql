-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 07, 2021 at 02:05 PM
-- Server version: 10.3.28-MariaDB-log-cll-lve
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `docttavf_beta`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `roleId` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`, `pass`, `name`, `roleId`, `status`) VALUES
(1, 'email2mkashif@gmail.com', 'e6e061838856bf47e1de730719fb2609', 'admin@123', 'admin', 0, 1),
(2, 'chaneujin8@gmail.com', 'e6e061838856bf47e1de730719fb2609', 'admin@123', 'admin', 0, 1),
(3, 'tklclee@yahoo.com', 'e6e061838856bf47e1de730719fb2609', 'admin@123', 'admin', 0, 1),
(4, 'drpetcha@gmail.com', 'e6e061838856bf47e1de730719fb2609', 'admin@123', 'admin', 0, 1),
(5, 'bhupindersingh75890@gmail.com', 'e6e061838856bf47e1de730719fb2609', 'admin@123', 'admin', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctorId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `license` varchar(200) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctorId`, `userId`, `name`, `phone`, `license`, `image`, `signature`) VALUES
(1, 1, 'Dr. Ammar', 182044350, 'GH434545454', NULL, NULL),
(2, 3, 'Dr Demo', 111111111, 'MN4345454422', NULL, NULL),
(3, 5, 'liching', 125035773, '48463', NULL, NULL),
(4, 6, 'Peter Chang', 97813547, '14548C', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `drug`
--

CREATE TABLE `drug` (
  `drugId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `drugName` varchar(200) NOT NULL,
  `drugStrength` varchar(200) NOT NULL,
  `drugType` int(11) NOT NULL,
  `drugSalePrice` decimal(10,2) NOT NULL,
  `drugBuyPrice` decimal(10,2) DEFAULT NULL,
  `drugExpiryDate` date DEFAULT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drug`
--

INSERT INTO `drug` (`drugId`, `userId`, `drugName`, `drugStrength`, `drugType`, `drugSalePrice`, `drugBuyPrice`, `drugExpiryDate`, `date`) VALUES
(1, 2, 'Panadol Extra', '500mg', 2, 2.00, 1.00, '2024-07-23', '2020-07-16'),
(2, 2, 'Panadol Cold', '1000mg', 2, 4.00, 2.00, '2024-07-16', '2020-07-16'),
(3, 2, 'Hydrocodone', '500mg', 2, 2.00, 1.00, NULL, '2020-07-16'),
(4, 2, 'Oxycodone', '200mg', 2, 4.00, 3.00, NULL, '2020-07-16'),
(5, 2, 'rrr', 'rr', 1, 2.00, 22.00, '2020-07-20', '2020-07-20'),
(6, 2, 'plavix', '75mg', 2, 9.30, 0.00, NULL, '2020-07-22'),
(7, 2, 'Lipitor ', '40mg', 2, 8.20, 0.00, NULL, '2020-07-22'),
(8, 2, 'Atorvastatin ( rotaqor)', '10mg', 2, 1.10, 0.00, NULL, '2020-07-22'),
(9, 2, 'Atorvastatin (rotaqor) 20mg', '20mg', 2, 1.40, 0.00, NULL, '2020-07-22'),
(10, 2, 'Klacid 14', '1tab', 2, 590.00, 0.00, NULL, '2020-07-22'),
(11, 2, 'Caduet', '5/10mg', 2, 7.10, 0.00, NULL, '2020-07-22'),
(12, 2, 'Actrapid', '100u/ml', 4, 33.00, 0.00, NULL, '2020-07-22'),
(13, 2, 'concor', '5mg', 2, 2.30, 0.00, NULL, '2020-07-22'),
(14, 2, 'NeoDeca eye drop', '1unit', 1, 8.10, 0.00, NULL, '2020-07-22'),
(15, 2, 'clotrimazole, beclomethasone Dipropionate 0.025%', '1unit', 1, 22.20, 0.00, NULL, '2020-07-22'),
(20, 2, 'Oxycodone', '100ml', 1, 25.00, 0.00, NULL, '2020-07-28'),
(21, 2, 'cardiprin', '100mg', 2, 0.60, 0.00, NULL, '2020-07-29'),
(28, 2, 'asd cream', '1unit', 1, 5.12, 0.00, NULL, '2020-11-30');

-- --------------------------------------------------------

--
-- Table structure for table `drugDose`
--

CREATE TABLE `drugDose` (
  `drugDoseId` int(11) NOT NULL,
  `drugId` int(11) NOT NULL,
  `drugDose` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drugDose`
--

INSERT INTO `drugDose` (`drugDoseId`, `drugId`, `drugDose`) VALUES
(4, 2, '250mg'),
(5, 2, '500mg'),
(6, 2, '1000mg'),
(7, 3, '250mg'),
(8, 3, '500mg'),
(11, 4, '100mg'),
(12, 4, '200mg'),
(13, 5, '12'),
(14, 6, '75mg'),
(15, 7, '10mg'),
(16, 7, '20mg'),
(17, 7, '40mg'),
(18, 8, '5mg'),
(19, 8, '10mg'),
(22, 10, '1tab'),
(23, 11, '1tab'),
(25, 12, '1vial'),
(33, 9, '10mg'),
(34, 9, '20mg'),
(39, 1, '250mg'),
(40, 1, '500mg'),
(41, 1, '1000mg'),
(42, 13, '1.25mg'),
(43, 13, '2.5mg'),
(44, 13, '5mg'),
(45, 21, '100mg'),
(46, 29, '20mg'),
(47, 30, '20mg'),
(48, 31, '20mg');

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `medicineId` int(11) NOT NULL,
  `prescriptionId` int(11) NOT NULL,
  `drugId` int(11) DEFAULT NULL,
  `drugDoseId` int(11) DEFAULT NULL,
  `strength` varchar(200) NOT NULL,
  `no` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `frequency` varchar(200) DEFAULT NULL,
  `instruction` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `subtotal` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine`
--

INSERT INTO `medicine` (`medicineId`, `prescriptionId`, `drugId`, `drugDoseId`, `strength`, `no`, `duration`, `frequency`, `instruction`, `quantity`, `price`, `subtotal`) VALUES
(1, 1, 2, 4, '1000mg', 2, 2, '1', '', 4, 4.00, 14.00),
(2, 1, 2, 5, '1000mg', 4, 2, '1', '', 14, 4.00, 56.00),
(3, 2, 4, 9, '200mg', 4, 2, '2', 'take on time', 28, 4.00, 112.00),
(4, 2, 3, 8, '500mg', 5, 2, '4', '', 140, 2.00, 280.00),
(5, 3, 1, 2, '500mg', 5, 1, '2', 'this is tets\r\n', 10, 2.00, 20.00),
(6, 3, 4, 10, '200mg', 4, 2, '3', '', 84, 4.00, 336.00),
(7, 4, 1, 2, '500mg', 4, 2, '1', 'Take after meal', 28, 2.00, 56.00),
(8, 4, 4, 10, '200mg', 7, 2, '3', 'Dont drive after taking tablet', 147, 4.00, 588.00),
(9, 5, 1, 3, '500mg', 2, 2, '3', 'Take after meal', 84, 2.00, 168.00),
(12, 6, 8, 19, '10mg', 4, 1, '6', '', 4, 1.10, 4.40),
(13, 6, 8, 25, '100u/ml', 0, 0, '', '', 0, 33.00, 0.00),
(14, 7, 8, 19, '10mg', 4, 1, '6', '', 4, 1.10, 4.40),
(15, 7, 8, 18, '100u/ml', 0, 1, '', '', 1, 33.00, 33.00),
(16, 7, 14, 30, '1unit', 0, 2, '2', '', 1, 7.80, 7.80),
(17, 7, 15, 32, '1unit', 0, 0, '3', '', 1, 21.40, 21.40),
(18, 8, 8, 19, '10mg', 4, 1, '6', '', 4, 1.10, 4.40),
(19, 8, 8, 18, '100u/ml', 0, 0, '', '', 0, 33.00, 0.00),
(20, 9, 8, 19, '10mg', 4, 1, '6', '', 4, 1.10, 4.40),
(21, 9, 8, 18, '100u/ml', 0, 1, '', '', 10, 33.00, 330.00),
(22, 9, 8, 30, '1unit', 0, 2, '2', '', 1, 7.80, 7.80),
(23, 10, 8, 19, '10mg', 4, 1, '6', '', 4, 1.10, 4.40),
(24, 10, 8, 18, '100u/ml', 0, 1, '', '', 1, 33.00, 33.00),
(25, 10, 8, 30, '1unit', 0, 2, '2', '2 drop right eye', 1, 7.80, 7.80),
(26, 10, 8, 32, '1unit', 0, 0, '3', '', 1, 21.40, 21.40),
(27, 10, 6, 14, '75mg', 2, 1, '3', '', 6, 9.30, 55.80),
(28, 11, 8, 19, '10mg', 4, 1, '2', '', 0, 1.10, 0.00),
(29, 11, 8, 18, '100u/ml', 0, 1, '', '', 10, 33.00, 330.00),
(30, 11, 8, 18, '1unit', 0, 2, '2', '', 1, 7.80, 7.80),
(31, 12, 1, 1, '500mg', 7, 2, '1', '', 25, 2.00, 49.00),
(32, 12, 4, 11, '200mg', 5, 2, '3', '', 53, 4.00, 210.00),
(33, 13, 2, 4, '1000mg', 5, 1, '10', '', 38, 4.00, 150.00),
(34, 14, 11, 23, '5/10mg', 1, 1, '2', '', 0, 7.10, 2.84),
(35, 14, 21, 38, '100mg', 1, 1, '1', '', 1, 0.50, 0.50),
(36, 14, 9, 33, '20mg', 1, 2, '1', '', 4, 1.40, 4.90),
(37, 15, 1, 1, '500mg', 16, 2, '3', '', 158, 2.00, 315.00),
(38, 15, 4, 12, '200mg', 1, 1, '4', '', 4, 4.00, 16.00),
(39, 16, 2, 6, '1000mg', 5, 2, '1', '', 35, 4.00, 140.00),
(40, 16, 4, 11, '200mg', 4, 1, '10', '', 60, 4.00, 240.00),
(41, 17, 1, 1, '500mg', 3, 2, '1', '', 11, 2.00, 21.00),
(42, 17, 20, NULL, '100ml', 2, 3, '1', '', 5, 25.00, 125.00),
(43, 18, 1, 1, '500mg', 3, 2, '1', '', 11, 2.00, 21.00),
(44, 18, 20, NULL, '100ml', 2, 3, '1', '', 5, 25.00, 125.00),
(45, 19, 1, 1, '500mg', 3, 2, '1', '', 11, 2.00, 21.00),
(46, 19, 20, NULL, '100ml', 2, 3, '1', '', 5, 25.00, 125.00),
(47, 20, 1, 1, '500mg', 3, 2, '1', 'in night', 11, 2.00, 21.00),
(48, 20, 20, NULL, '100ml', 2, 3, '1', 'after meal', 5, 25.00, 125.00),
(49, 21, 1, 1, '500mg', 3, 2, '1', 'in night', 11, 2.00, 21.00),
(50, 21, 20, NULL, '100ml', 1, 3, '2', 'after meal', 1, 25.00, 25.00),
(53, 23, 4, 11, '200mg', 1, 1, '1', 'dsad', 3, 4.00, 12.00),
(58, 22, 20, 1, '100ml', 0, 2, '2', '', 2, 25.00, 50.00),
(59, 22, 20, 1, '500mg', 2, 2, '3', '', 21, 2.00, 42.00),
(60, 24, 2, 6, '1000mg', 2, 2, '3', 'after meal', 42, 4.00, 168.00),
(61, 24, 20, 6, '100ml', 1, 3, '1', '', 1, 25.00, 25.00),
(62, 25, 2, 6, '1000mg', 2, 2, '3', 'after meal', 42, 4.00, 168.00),
(63, 25, 1, 3, '500mg', 5, 2, '7', '', 350, 2.00, 700.00),
(64, 26, 1, 2, '500mg', 4, 2, '2', 'dfdfgd', 56, 2.00, 112.00),
(65, 26, 2, 6, '1000mg', 4, 2, '1', 'dfd', 28, 4.00, 112.00),
(66, 27, 1, 2, '500mg', 4, 2, '2', 'dfdfgd', 56, 2.00, 112.00),
(67, 27, 1, 6, '1000mg', 4, 2, '1', 'dfdfgd', 28, 4.00, 112.00),
(68, 28, 1, 1, '500mg', 3, 2, '2', 'after meal', 21, 2.00, 42.00),
(69, 28, 2, 6, '1000mg', 5, 2, '1', 'after lunch', 35, 4.00, 140.00),
(70, 29, 1, 35, '1unit', 5, 2, '1', 'after meal', 175, 7.80, 7.80),
(71, 29, 14, 35, '1unit', 1, 1, '7', '', 1, 7.80, 7.80),
(72, 30, 1, 1, '1unit', 5, 2, '1', 'after meal', 175, 7.80, 7.80),
(73, 30, 1, 35, '1unit', 1, 1, '7', 'after meal', 1, 7.80, 7.80),
(74, 31, 1, 1, '1unit', 5, 2, '1', 'after meal', 175, 7.80, 7.80),
(75, 31, 14, 35, '1unit', 1, 2, '2', '', 14, 7.80, 109.20),
(76, 32, 1, 39, '500mg', 3, 1, '1', '', 2, 2.00, 3.00),
(77, 33, 1, 39, '500mg', 3, 1, '1', '', 2, 2.00, 3.00),
(78, 34, 1, 39, '500mg', 1, 1, '1', 'Test item', 1, 2.00, 1.00),
(79, 35, 1, 39, '500mg', 2, 1, '1', 'Test item', 1, 2.00, 2.00),
(80, 36, 1, 39, '500mg', 2, 1, '1', 'Test item', 1, 2.00, 2.00),
(81, 37, 1, 39, '500mg', 2, 1, '1', 'Test item', 1, 2.00, 2.00),
(82, 38, 1, 39, '500mg', 2, 1, '1', '', 1, 2.00, 2.00),
(83, 39, 2, 4, '1000mg', 2, 1, '1', '', 1, 4.00, 2.00),
(84, 40, 2, 4, '1000mg', 7, 1, '2', '', 4, 4.00, 14.00),
(85, 41, 2, 4, '1000mg', 0, 1, '1', '', 1, 4.00, 0.00),
(86, 42, 1, 39, '500mg', 0, 0, '', '', 1, 2.00, 2.00),
(87, 43, 1, 39, '500mg', 2, 1, '1', '', 1, 2.00, 2.00),
(88, 44, 1, 39, '500mg', 2, 1, '1', '', 1, 2.00, 2.00),
(89, 45, 1, 40, '500mg', 1, 1, '1', '', 1, 2.00, 2.00),
(90, 46, 6, 14, '75mg', 100, 1, '5', 'dssm take after med', 100, 9.30, 930.00),
(91, 46, 15, 14, '1unit', 0, 1, '6', '', 1, 21.40, 21.40),
(92, 47, 1, 39, '500mg', 0, 1, '1', 'Eat when neccessary', 1, 2.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE `merchant` (
  `merchantId` int(11) NOT NULL,
  `secret` varchar(200) NOT NULL,
  `merchant` varchar(200) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merchant`
--

INSERT INTO `merchant` (`merchantId`, `secret`, `merchant`, `userId`) VALUES
(2, '123456', '80000155', 3),
(4, 'qcAdKI8uDX6Cgha5', '80008678', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `pagesId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `pagesTitle` varchar(200) NOT NULL,
  `pagesDescription` text NOT NULL,
  `pagesStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`pagesId`, `userId`, `pagesTitle`, `pagesDescription`, `pagesStatus`) VALUES
(1, 2, 'About Us', '<p><strong>A unique approach to your health. </strong><br />\r\nEvery person is unique and so is our approach to your care.<br />\r\n<br />\r\nBLISS Pharmacy Pharmacists are empowered to unlock their potential beyond the dispensary &ndash; they engage with their local communities and collaborate with their wider health care network to drive health change to make a positive difference in their patients&rsquo; lives.<br />\r\n<br />\r\nWe see each patient as an individual and personalise our recommendations so they are meaningful to you.<br />\r\n<br />\r\nWe take your health care to heart. We&#39;re always looking for better ways to take care of you, including:<br />\r\n1- getting to know you better so we can personalise our care and advice to you<br />\r\n2- providing patient care services and professional support for a variety of health concerns<br />\r\n3- ensuring your prescribed therapy is working for you by reviewing your medicines<br />\r\n4- making your life easier by packing your medicines<br />\r\n5- giving your accessibility to customised medicines via our compounding network<br />\r\n6- being there for you when you cannot visit the pharmacy - following up by phone, providing support at home and delivering to you</p>\r\n', 1),
(3, 2, 'Terms & Conditions', '<h2><strong>Terms and Conditions</strong></h2>\r\n\r\n<p>Welcome to DoctorTouch RX!</p>\r\n\r\n<p>These terms and conditions outline the rules and regulations for the use of BLISS Pharmacy&#39;s Website, located at https://doctortouchrx.com.</p>\r\n\r\n<p>By accessing this website we assume you accept these terms and conditions. Do not continue to use DoctorTouch RX if you do not agree to take all of the terms and conditions stated on this page. Our Terms and Conditions were created with the help of the Terms And Conditions Generator and the Free Terms &amp; Conditions Generator.</p>\r\n\r\n<p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: &quot;Client&quot;, &quot;You&quot; and &quot;Your&quot; refers to you, the person log on this website and compliant to the Company&rsquo;s terms and conditions. &quot;The Company&quot;, &quot;Ourselves&quot;, &quot;We&quot;, &quot;Our&quot; and &quot;Us&quot;, refers to our Company. &quot;Party&quot;, &quot;Parties&quot;, or &quot;Us&quot;, refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client&rsquo;s needs in respect of provision of the Company&rsquo;s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>\r\n\r\n<h3><strong>Cookies</strong></h3>\r\n\r\n<p>We employ the use of cookies. By accessing DoctorTouch RX, you agreed to use cookies in agreement with the BLISS Pharmacy&#39;s Privacy Policy.</p>\r\n\r\n<p>Most interactive websites use cookies to let us retrieve the user&rsquo;s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p>\r\n\r\n<h3><strong>License</strong></h3>\r\n\r\n<p>Unless otherwise stated, BLISS Pharmacy and/or its licensors own the intellectual property rights for all material on DoctorTouch RX. All intellectual property rights are reserved. You may access this from DoctorTouch RX for your own personal use subjected to restrictions set in these terms and conditions.</p>\r\n\r\n<p>You must not:</p>\r\n\r\n<ul>\r\n	<li>Republish material from DoctorTouch RX</li>\r\n	<li>Sell, rent or sub-license material from DoctorTouch RX</li>\r\n	<li>Reproduce, duplicate or copy material from DoctorTouch RX</li>\r\n	<li>Redistribute content from DoctorTouch RX</li>\r\n</ul>\r\n\r\n<p>This Agreement shall begin on the date hereof.</p>\r\n\r\n<p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. BLISS Pharmacy does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of BLISS Pharmacy,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, BLISS Pharmacy shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p>\r\n\r\n<p>BLISS Pharmacy reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p>\r\n\r\n<p>You warrant and represent that:</p>\r\n\r\n<ul>\r\n	<li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li>\r\n	<li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li>\r\n	<li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li>\r\n	<li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li>\r\n</ul>\r\n\r\n<p>You hereby grant BLISS Pharmacy a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p>\r\n\r\n<h3><strong>Hyperlinking to our Content</strong></h3>\r\n\r\n<p>The following organizations may link to our Website without prior written approval:</p>\r\n\r\n<ul>\r\n	<li>Government agencies;</li>\r\n	<li>Search engines;</li>\r\n	<li>News organizations;</li>\r\n	<li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li>\r\n	<li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li>\r\n</ul>\r\n\r\n<p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party&rsquo;s site.</p>\r\n\r\n<p>We may consider and approve other link requests from the following types of organizations:</p>\r\n\r\n<ul>\r\n	<li>commonly-known consumer and/or business information sources;</li>\r\n	<li>dot.com community sites;</li>\r\n	<li>associations or other groups representing charities;</li>\r\n	<li>online directory distributors;</li>\r\n	<li>internet portals;</li>\r\n	<li>accounting, law and consulting firms; and</li>\r\n	<li>educational institutions and trade associations.</li>\r\n</ul>\r\n\r\n<p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of BLISS Pharmacy; and (d) the link is in the context of general resource information.</p>\r\n\r\n<p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party&rsquo;s site.</p>\r\n\r\n<p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to BLISS Pharmacy. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p>\r\n\r\n<p>Approved organizations may hyperlink to our Website as follows:</p>\r\n\r\n<ul>\r\n	<li>By use of our corporate name; or</li>\r\n	<li>By use of the uniform resource locator being linked to; or</li>\r\n	<li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party&rsquo;s site.</li>\r\n</ul>\r\n\r\n<p>No use of BLISS Pharmacy&#39;s logo or other artwork will be allowed for linking absent a trademark license agreement.</p>\r\n\r\n<h3><strong>iFrames</strong></h3>\r\n\r\n<p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p>\r\n\r\n<h3><strong>Content Liability</strong></h3>\r\n\r\n<p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>\r\n\r\n<h3><strong>Your Privacy</strong></h3>\r\n\r\n<p>Please read Privacy Policy</p>\r\n\r\n<h3><strong>Reservation of Rights</strong></h3>\r\n\r\n<p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it&rsquo;s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p>\r\n\r\n<h3><strong>Removal of links from our website</strong></h3>\r\n\r\n<p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p>\r\n\r\n<p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p>\r\n\r\n<h3><strong>Disclaimer</strong></h3>\r\n\r\n<p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p>\r\n\r\n<ul>\r\n	<li>limit or exclude our or your liability for death or personal injury;</li>\r\n	<li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li>\r\n	<li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li>\r\n	<li>exclude any of our or your liabilities that may not be excluded under applicable law.</li>\r\n</ul>\r\n\r\n<p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>\r\n\r\n<p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>\r\n', 1),
(4, 2, 'Return Policy', '<ol>\r\n	<li>Customer must present the good(s) physically to exchange at the outlet they purchased from.</li>\r\n	<li>Exchange of good(s) can only be made within 7 days of purchase.</li>\r\n	<li>Exchange / Returned good(s) must be:<br />\r\n	&ndash; Accompanied with the original receipt.<br />\r\n	&ndash; In original unopened packaging, unmarked and properly packed.</li>\r\n	<li>No Return / Exchange is allowed for the following good(s):<br />\r\n	&ndash; Medicines<br />\r\n	&ndash; Cold chain&nbsp; items</li>\r\n</ol>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&ndash; Compression stockings</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &ndash; Orthopaedic supports</p>\r\n\r\n<ol>\r\n	<li>Any form of promotion / product voucher / discount voucher used on purchase is not returnable.</li>\r\n	<li>Good(s) sold are not refundable.</li>\r\n	<li>In case of discrepancy upon receiving of good(s), Customer must notify the respective outlet immediately.<br />\r\n	&nbsp;</li>\r\n</ol>\r\n', 1),
(5, 2, 'Contact Us', '<p><strong>Shop Address:</strong><br />\r\n25, persiaran mutiara 7, pusat komersial bandar tasek mutiara, 14120 Simpang Ampat, Penang<br />\r\n<br />\r\n&nbsp;</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `paymentId` int(11) NOT NULL,
  `orderNo` int(11) NOT NULL,
  `returnCode` int(11) DEFAULT NULL,
  `patientEmail` varchar(200) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `paymentrId` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`paymentId`, `orderNo`, `returnCode`, `patientEmail`, `amount`, `paymentrId`) VALUES
(1, 80720, 100, 'email2mkashif@gmail.com', 336.00, NULL),
(2, 7526, 100, 'bhupindersingh75890@gmail.com', 312.00, NULL),
(3, 47987, 100, 'email2mkashif@gmail.com', 202.00, NULL),
(4, 50365, 100, 'email2mkashif@gmail.com', 92.00, NULL),
(5, 96386, 100, 'leeliching@yahoo.com', 144.00, NULL),
(6, 47141, 100, 'leeliching@yahoo.com', 41.00, NULL),
(7, 88464, 0, 'email2mkashif@gmail.com', 12.00, NULL),
(8, 17698, 0, 'email2mkashif@gmail.com', 1.00, NULL),
(9, 50711, -1, 'email2mkashif@gmail.com', 2.00, NULL),
(10, 50711, 100, 'email2mkashif@gmail.com', 2.00, NULL),
(11, 313, 100, 'leeliching@yahoo.com', 2.00, 'cf663e0d16706c3bf2be03727968cb2779e86868'),
(12, 69040, 100, 'chaneujin8@gmail.com', 2.00, NULL),
(13, 97063, 100, 'eujinc@doctortouch.com', 2.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pharmacy`
--

CREATE TABLE `pharmacy` (
  `pharmacyId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `logo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pharmacy`
--

INSERT INTO `pharmacy` (`pharmacyId`, `userId`, `name`, `address`, `phone`, `logo`) VALUES
(1, 2, 'Bliss Pharmacy', 'A-13-15, Pangsapuri Berembang Indah\r\nJalan Nipah Off Jalan Ampang', 182044350, '1597557434bliss-pharmacy-logo.webp'),
(2, 4, 'Demo Pharmacy', 'A-13-05, Setapak, Kuala Lumpur, Malaysia', 56565656565, '');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `prescriptionId` int(11) NOT NULL,
  `orderNo` varchar(200) NOT NULL,
  `userId` int(11) NOT NULL,
  `patientName` varchar(200) NOT NULL,
  `ic` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `phone` bigint(20) NOT NULL,
  `pharmacyId` int(11) NOT NULL,
  `totalamount` decimal(10,2) NOT NULL,
  `copay` decimal(10,2) DEFAULT NULL,
  `payableAmount` decimal(10,2) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `delivery` int(11) NOT NULL,
  `deliveryCharges` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `paymentStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`prescriptionId`, `orderNo`, `userId`, `patientName`, `ic`, `email`, `phone`, `pharmacyId`, `totalamount`, `copay`, `payableAmount`, `image`, `date`, `delivery`, `deliveryCharges`, `status`, `paymentStatus`) VALUES
(1, '12668', 1, 'Maria', 'DD2323232K', 'email2mkashif@gmail.com', 182044350, 2, 70.00, 10.00, 80.00, 'assets/generate/5f102522d998a.png', '2020-07-16', 1, 20, 1, 0),
(2, '24465', 1, 'Kash', 'AS2323232K', 'email2mkashif@gmail.com', 182044350, 2, 392.00, 300.00, 92.00, 'assets/generate/5f1024ab5f8b1.png', '2020-07-16', 2, NULL, 0, 0),
(3, '80720', 1, 'Maria', 'SDF34343', 'email2mkashif@gmail.com', 34343, 2, 356.00, 50.00, 336.00, 'assets/generate/5f110b9a1e509.png', '2020-07-17', 1, 30, 0, 0),
(4, '16526', 3, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 644.00, 500.00, 154.00, 'assets/generate/5f1e32cf6d2a0.png', '2020-07-19', 1, 10, 1, 0),
(5, '27908', 1, 'Sam', 'GFFGFG3434', 'email2mkashif@gmail.com', 182044350, 2, 168.00, 100.00, 68.00, 'assets/generate/5f16b5e43b52a.png', '2020-07-21', 1, NULL, 0, 0),
(6, '88049', 1, 'tklc', '', 'tklclee@yahoo.com', 0, 2, 4.00, 0.00, 4.00, 'assets/generate/5f17cd79de695.png', '2020-07-22', 2, NULL, 0, 0),
(7, '67430', 1, 'tklc', '', 'tklclee@yahoo.com', 0, 2, 65.00, 50.00, 15.00, 'assets/generate/5f17cf9d9b5e0.png', '2020-07-22', 1, NULL, 0, 0),
(9, '7526', 1, 'tklc', '', 'bhupindersingh75890@gmail.com', 0, 2, 362.00, 50.00, 312.00, 'assets/generate/5f218e50c2523.png', '2020-07-23', 1, NULL, 1, 0),
(10, '45683', 1, 'tklc', '', 'tklclee@yahoo.com', 0, 2, 120.00, 100.00, 20.00, 'assets/generate/5f1e40d13aadd.png', '2020-07-27', 2, NULL, 1, 0),
(11, '28288', 2, 'tklc', '', 'tklclee@yahoo.com', 0, 2, 0.00, 50.00, -50.00, 'assets/generate/5f29a90f47690.png', '2020-07-27', 1, NULL, 1, 0),
(12, '59319', 1, 'kash', 'AD4343', 'email2mkashif@gmail.com', 909090909, 2, 259.00, 200.00, 69.00, 'assets/generate/5f215f328d730.png', '2020-07-29', 1, 10, 1, 0),
(13, '93751', 1, 'Mohd', 'AD34343', 'mkash.designs@gmail.com', 45454, 2, 150.00, 100.00, 60.00, 'assets/generate/5f2162f3300c6.png', '2020-07-29', 1, 10, 1, 0),
(14, '41259', 1, 'tk', '', 'tklclee@yahoo.com', 4039005, 2, 6.00, 200.00, -184.00, 'assets/generate/5f217140ce2c9.png', '2020-07-29', 1, 10, 1, 0),
(15, '90017', 1, 'Jhone', 'GH3434343', 'email2mkashif@gmail.com', 33333, 2, 331.00, 100.00, 241.00, 'assets/generate/5f226056d8322.png', '2020-07-30', 1, 10, 1, 0),
(16, '47987', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 392.00, 200.00, 202.00, 'assets/generate/5f24ff79b7bd2.png', '2020-08-01', 1, 10, 1, 0),
(17, '88784', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 146.00, 200.00, -54.00, NULL, '2020-08-01', 1, NULL, 0, 0),
(18, '61734', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 146.00, 200.00, -54.00, NULL, '2020-08-01', 1, NULL, 0, 0),
(19, '6042', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 146.00, 200.00, -54.00, NULL, '2020-08-01', 1, NULL, 0, 0),
(20, '29944', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 146.00, 200.00, -54.00, NULL, '2020-08-01', 1, NULL, 0, 0),
(21, '39467', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 46.00, 200.00, -154.00, 'assets/generate/5f2557b068f23.png', '2020-08-01', 1, NULL, 1, 0),
(22, '68361', 1, 'Sam', 'DD2323232K', 'email2mkashif@gmail.com', 182044350, 2, 92.00, 100.00, 2.00, NULL, '2020-08-01', 1, 10, 1, 0),
(23, '76055', 1, 'cczxczxczccczc', 'EWQE', 'doctor@yopmail.com', 2222222222, 2, 12.00, 0.00, 12.00, 'assets/generate/5f29a8c694174.png', '2020-08-01', 1, NULL, 1, 0),
(24, '63282', 1, 'Huzzi', 'DD2323232K', 'bhupindersingh75890@gmail.com', 182044350, 2, 193.00, 100.00, 93.00, 'assets/generate/5f297c00e4cf0.png', '2020-08-01', 1, NULL, 1, 0),
(25, '97234', 1, 'Huzzi', 'DD2323232K', 'email2mkashif@gmail.com', 182044350, 2, 868.00, 100.00, 788.00, 'assets/generate/5f256154329a9.png', '2020-08-01', 1, 20, 1, 0),
(26, '8526', 1, 'kash', 'DF343', 'leeliching@yahoo.com', 34343, 2, 224.00, 200.00, 34.00, 'assets/generate/5f28e4ce94e7e.png', '2020-08-04', 1, 10, 1, 0),
(27, '96386', 1, 'kash', 'DF343', 'leeliching@yahoo.com', 34343, 2, 224.00, 100.00, 144.00, 'assets/generate/5f2acd69f293c.png', '2020-08-05', 1, 20, 1, 0),
(28, '50365', 1, 'Maria', 'CFF3434343', 'email2mkashif@gmail.com', 182044350, 2, 182.00, 100.00, 92.00, 'assets/generate/5f2acca0bcf63.png', '2020-08-05', 1, 10, 1, 0),
(29, '52847', 1, 'Maria', 'CFF3434343', 'leeliching@yahoo.com', 12345678, 2, 14.00, 100.00, -86.00, 'assets/generate/5f2c0fa627164.png', '2020-08-06', 1, NULL, 0, 0),
(30, '96625', 1, 'Maria', 'CFF3434343', 'leeliching@yahoo.com', 12345678, 2, 14.00, 100.00, -76.00, 'assets/generate/5f2c12ffaf561.png', '2020-08-06', 1, 10, 1, 0),
(31, '47141', 1, 'Maria', 'CFF3434343', 'leeliching@yahoo.com', 12345678, 2, 116.00, 100.00, 41.00, 'assets/generate/5f2c143180ef4.png', '2020-08-06', 1, 25, 1, 0),
(32, '46013', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 3.00, 1.00, 12.00, 'assets/generate/5f2d60b28d108.png', '2020-08-07', 1, 10, 1, 0),
(33, '88464', 1, 'Maria', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 3.00, 1.00, 12.00, 'assets/generate/5f2d6c163fba6.png', '2020-08-07', 1, 10, 1, 0),
(34, '17698', 1, 'Sam', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 1.00, 0.00, 1.00, 'assets/generate/5f3162ed3c095.png', '2020-08-10', 1, NULL, 1, 0),
(35, '50711', 1, 'Sam', 'AS2323232', 'email2mkashif@gmail.com', 182044350, 2, 2.00, 0.00, 2.00, 'assets/generate/5f3164bb22f1d.png', '2020-08-10', 1, NULL, 1, 0),
(36, '313', 1, 'Lee', 'AS2323232', 'leeliching@yahoo.com', 182044350, 2, 2.00, 0.00, 2.00, 'assets/generate/5f32221656de6.png', '2020-08-11', 1, NULL, 1, 0),
(37, '145', 1, 'Lee', 'AS2323232', 'leeliching@yahoo.com', 182044350, 2, 2.00, 20.00, -18.00, 'assets/generate/5f322b13ce064.png', '2020-08-11', 2, NULL, 1, 0),
(38, '75110', 1, 'Yu juin', 'AS2323232K', 'chaneujin8@gmail.com', 182044350, 2, 2.00, 0.00, 2.00, 'assets/generate/5f38e0f70f8ca.png', '2020-08-16', 1, NULL, 1, 0),
(39, '69040', 1, 'Yu juin', 'AS2323232K', 'chaneujin8@gmail.com', 182044350, 2, 2.00, 0.00, 2.00, 'assets/generate/5f38e2d3e143a.png', '2020-08-16', 1, NULL, 1, 0),
(40, '88435', 6, 'Daniel Chang', '', 'drpetcha@gmail.com', 97813547, 2, 14.00, 5.00, 9.00, 'assets/generate/5f39981e36e70.png', '2020-08-16', 2, NULL, 1, 0),
(42, '97063', 1, 'Eu Jin ', '123456789', 'eujinc@doctortouch.com', 123456789, 2, 2.00, 0.00, 2.00, 'assets/generate/5f3a2fa3a15f3.png', '2020-08-17', 2, NULL, 1, 0),
(43, '26951', 1, 'Eu Jin ', '12345678', 'eujinc@generousapp.com', 12345678, 2, 2.00, 0.00, 2.00, 'assets/generate/5f3a3172d1602.png', '2020-08-17', 2, NULL, 1, 0),
(44, '97973', 1, 'Eu Jin ', '12345678', 'email2mkashif@gmail.com', 12345678, 2, 2.00, 2.00, 0.00, 'assets/generate/5f421a053ff4f.png', '2020-08-23', 1, NULL, 1, 0),
(45, '59313', 1, 'Kash', 'AQ234434', 'email2mkashif@gmail.com', 23232232, 2, 2.00, 0.00, 2.00, 'assets/generate/5fa3f757e12fa.png', '2020-09-11', 1, NULL, 1, 2),
(46, '71778', 5, 'Maria', '', 'thomasliching@yahoo.com', 60125035773, 2, 951.00, 200.00, 751.00, 'assets/generate/5fa5f87f778ad.png', '2020-11-07', 2, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userPassword` varchar(200) NOT NULL,
  `userPass` varchar(200) NOT NULL,
  `userType` int(11) NOT NULL,
  `userStatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `userEmail`, `userPassword`, `userPass`, `userType`, `userStatus`) VALUES
(1, 'kashif.mh2u@gmail.com', 'b3666d14ca079417ba6c2a99f079b2ac', 'doctor123', 1, 0),
(2, 'click2tube@gmail.com', '2bfa6dce52ee8cf7a9fd149c999ca1db', 'pharma123', 2, 0),
(3, 'email2mkashif@gmail.com', '62cc2d8b4bf2d8728120d052163a77df', 'demo123', 1, 0),
(4, 'unisolutionz@yahoo.com', '62cc2d8b4bf2d8728120d052163a77df', 'demo123', 2, 0),
(5, 'leeliching@yahoo.com', '29aa7acafb73866d6571e1a72f46c146', 'Apple123', 1, 0),
(6, 'drpetcha@gmail.com', '163d740861470429e43eaaabea7fce86', 'D@Z4wZfb', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctorId`);

--
-- Indexes for table `drug`
--
ALTER TABLE `drug`
  ADD PRIMARY KEY (`drugId`);

--
-- Indexes for table `drugDose`
--
ALTER TABLE `drugDose`
  ADD PRIMARY KEY (`drugDoseId`);

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD PRIMARY KEY (`medicineId`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD PRIMARY KEY (`merchantId`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`pagesId`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `pharmacy`
--
ALTER TABLE `pharmacy`
  ADD PRIMARY KEY (`pharmacyId`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`prescriptionId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctorId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `drug`
--
ALTER TABLE `drug`
  MODIFY `drugId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `drugDose`
--
ALTER TABLE `drugDose`
  MODIFY `drugDoseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `medicine`
--
ALTER TABLE `medicine`
  MODIFY `medicineId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `merchant`
--
ALTER TABLE `merchant`
  MODIFY `merchantId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `pagesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pharmacy`
--
ALTER TABLE `pharmacy`
  MODIFY `pharmacyId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `prescriptionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
